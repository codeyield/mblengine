<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * Custom config file
 */

// Название сайта
$config['sitetitle'] = 'MBL Engine ' . VERSION;

// Параметры нового тиража
$config['drawparams'] = [
	'title_minlen'	=> 0,
	'title_maxlen'	=> 200,
	'code_minlen'	=> 3,
	'code_maxlen'	=> 12,
	'coin_minlen'	=> 3,
	'coin_maxlen'	=> 10,
	'min_bet'		=> 1,
	'max_bet'		=> 1,
	'min_tickets'	=> 2,
	'max_tickets'	=> 10000000,
	'min_prize'		=> 1,
	'max_prize'		=> 10000000,	// Не более чем 10 ** PRECISION
	'min_winners'	=> 1,
	'max_winners'	=> 1000000,
	'min_cashback'	=> 0,
	'max_cashback'	=> 1000,
];

// Параметры текстовых шаблонов для постов
$config['textparams'] = [
	'tpl_minlen'	=> 3,
	'tpl_maxlen'	=> 50,
	'minlen'		=> 0,
	'maxlen'		=> 2500,
	'url_minlen'	=> 0,
	'url_maxlen'	=> 100,
];

// Параметры чата/канала (пока только настройки авто-запуска тиражей)
$config['chatparams'] = [
	'apresets_minlen'	=> 4,
	'apresets_maxlen'	=> 500,
];

// Сообщения на странице шаблонов
$config['tplmessages'] = [
	'tplsaved'    => 'Тексты шаблонов успешно сохранены.',
	'tplfail'     => 'Не удалось сохранить тексты шаблонов :(',
	'newtpladded' => 'Новый шаблон был добавлен. Выберите его из выпадающего списка, если необходимо.',
	'newtplfail'  => 'Не удалось добавить новый шаблон :(',
];

// Типы розыгрышей
$config['drawmodes'] = [
	[
		'type'  => DRAWMODE_MANUAL,
		'descr' => 'РУЧНОЙ: розыгрыш и выплаты вручную',
		'short' => 'РУЧНОЙ',
	]
];

// Типы статусов тиража
$config['drawstatuses'] = [
	DRAWSTATUS_WAIT  => DRAWSTATUS_WAIT,
	DRAWSTATUS_RUN   => DRAWSTATUS_RUN,
	DRAWSTATUS_DONE  => DRAWSTATUS_DONE,
	DRAWSTATUS_BREAK => DRAWSTATUS_BREAK,
];

// Действия, тексты кнопок и тексты сообщений на странице управления тиражами
$config['actions'] = [
	ACTION_SHOWRUN	=> [
		'value'  => ACTION_SHOWRUN,
		'event'  => EVENT_PREVIEW,
		'btn'    => EMOJI_FLAGRU . ' Пост запуска',
		'msgerr' => 'Ошибка! Не удалось сформировать текст поста для публикации.',
		'msgempty' => 'ПРЕДУПРЕЖДЕНИЕ: Соответствующий шаблон пуст.',
	],
	ACTION_SHOWRUN2	=> [
		'value'  => ACTION_SHOWRUN2,
		'event'  => EVENT_PREVIEW,
		'btn'    => EMOJI_FLAGUK . ' Пост запуска',
		'msgerr' => 'Ошибка! Не удалось сформировать текст поста для публикации.',
		'msgempty' => 'ПРЕДУПРЕЖДЕНИЕ: Соответствующий шаблон пуст.',
	],
	ACTION_SHOWPLAY	=> [
		'value'  => ACTION_SHOWPLAY,
		'event'  => EVENT_PREVIEW,
		'btn'    => EMOJI_FLAGRU . ' Пост розыгрыша',
		'msgerr' => 'Ошибка! Не удалось сформировать текст поста для публикации.',
		'msgempty' => 'ПРЕДУПРЕЖДЕНИЕ: Соответствующий шаблон пуст.',
	],
	ACTION_SHOWPLAY2	=> [
		'value'  => ACTION_SHOWPLAY2,
		'event'  => EVENT_PREVIEW,
		'btn'    => EMOJI_FLAGUK . ' Пост розыгрыша',
		'msgerr' => 'Ошибка! Не удалось сформировать текст поста для публикации.',
		'msgempty' => 'ПРЕДУПРЕЖДЕНИЕ: Соответствующий шаблон пуст.',
	],
	ACTION_SHOWINFO	=> [
		'value'  => ACTION_SHOWINFO,
		'event'  => EVENT_PREVIEW,
		'btn'    => EMOJI_FLAGRU . ' Инфо пост',
		'msgerr' => 'Ошибка! Не удалось сформировать текст поста для публикации.',
		'msgempty' => 'ПРЕДУПРЕЖДЕНИЕ: Соответствующий шаблон пуст.',
	],
	ACTION_SHOWINFO2	=> [
		'value'  => ACTION_SHOWINFO2,
		'event'  => EVENT_PREVIEW,
		'btn'    => EMOJI_FLAGUK . ' Инфо пост',
		'msgerr' => 'Ошибка! Не удалось сформировать текст поста для публикации.',
		'msgempty' => 'ПРЕДУПРЕЖДЕНИЕ: Соответствующий шаблон пуст.',
	],
	ACTION_RUNDRAW	=> [
		'value'  => ACTION_RUNDRAW,
		'event'  => EVENT_ACTION,
		'btn'    => EMOJI_ROCKET . ' Запустить и опубликовать',
		'msgok'  => 'Тираж был запущен. Проверьте публикацию поста в выбранном чате/канале.',
		'msgerr' => 'Ошибка! Не удалось запустить тираж и/или опубликовать пост.',
		'msgnopost' => 'Тираж был успешно запущен, однако не удалось опубликовать пост.',
	],
	ACTION_PLAYOUT	=> [
		'value'  => ACTION_PLAYOUT,
		'event'  => EVENT_ACTION,
		'btn'    => 'Проверить и разыграть',
		'msgok'  => 'Транзакции тиража были обработаны. Проверьте публикацию поста в выбранном чате/канале.',
		'msgerr' => 'Ошибка! Не удалось обработать транзакции тиража.',
		//'msgnopost' => 'Тираж был успешно разыгран, однако не удалось опубликовать пост.',
	],
	ACTION_SYNC	=> [
		'value'  => ACTION_SYNC,
		'event'  => EVENT_ACTION,
		'btn'    => 'Синхронизировать',
		'msgok'  => 'Транзакции тиража были обработаны. Проверьте публикацию поста в выбранном чате/канале.',
		'msgerr' => 'Ошибка! Не удалось обработать транзакции тиража.',
		//'msgnopost' => 'Тираж был успешно разыгран, однако не удалось опубликовать пост.',
	],
	ACTION_RUNPOST	=> [
		'value'  => ACTION_RUNPOST,
		'event'  => EVENT_ACTION,
		'btn'    => 'Выпустить пост(ы) запуска',
		'msgok'  => 'Готово. Проверьте публикацию поста в выбранном чате/канале.',
		'msgerr' => 'Ошибка! Не удалось опубликовать пост.',
	],
	ACTION_PLAYPOST	=> [
		'value'  => ACTION_PLAYPOST,
		'event'  => EVENT_ACTION,
		'btn'    => 'Выпустить пост(ы) розыгрыша',
		'msgok'  => 'Готово. Проверьте публикацию поста в выбранном чате/канале.',
		'msgerr' => 'Ошибка! Не удалось опубликовать пост.',
	],
	ACTION_INFOPOST	=> [
		'value'  => ACTION_INFOPOST,
		'event'  => EVENT_ACTION,
		'btn'    => 'Выпустить инфо пост(ы)',
		'msgok'  => 'Готово. Проверьте публикацию поста в выбранном чате/канале.',
		'msgerr' => 'Ошибка! Не удалось опубликовать пост.',
	],
	ACTION_BREAKDRAW	=> [
		'value'  => ACTION_BREAKDRAW,
		'event'  => EVENT_ACTION,
		'btn'    => 'Отменить без розыгрыша',
		'msgok'  => 'Тираж был отменен без розыгрыша. Сообщите пользователям о причинах и о судьбе купленных билетов.',
		'msgerr' => 'Ошибка! Не удалось отменить тираж.',
	],
	ACTION_DELDRAW		=> [
		'value'  => ACTION_DELDRAW,
		'event'  => EVENT_ACTION,
		'btn'    => 'Удалить безвозвратно',
		'msgok'  => 'Тираж был удален.',
		'msgerr' => 'Ошибка! Не удалось удалить тираж.',
	],
];

// Список нод Минтер для парсера
$config['minter_nodes'] = [
//	'mscandev'	  => 'https://api.mscan.dev/'.MSCAN_APIKEY.'/node/',
	'minterone'	  => 'https://api.minter.one/',
];

// Список нод Минтер для работы с API
$config['minter_nodes_api'] = [
//	'mscan' => [
//		'base_uri'        => 'https://api.mscan.dev/'.MSCAN_APIKEY.'/node/',
//		'verify'          => false,
//		'connect_timeout' => MINTERAPI_CONNTIMEOUT,
//		'timeout'         => MINTERAPI_TIMEOUT,
//	],
//	'minterone' => [
//		'base_uri'        => 'https://api.minter.one/',
//		'verify'          => false,
//		'connect_timeout' => MINTERAPI_CONNTIMEOUT,
//		'timeout'         => MINTERAPI_TIMEOUT,
//	],
//	'mtools' => [		// Ex. Mscan.dev
//		'base_uri'        => 'https://api.mtools.network/',
//		'verify'          => false,
//		'connect_timeout' => MINTERAPI_CONNTIMEOUT,
//		'timeout'         => MINTERAPI_TIMEOUT,
//	],
	'funfasy' => [
		'base_uri'        => 'https://mnt.funfasy.dev/',
		'verify'          => false,
		'connect_timeout' => MINTERAPI_CONNTIMEOUT,
		'timeout'         => MINTERAPI_TIMEOUT,
		'headers'         => [
			'Content-Type'     => 'application/json',
			'X-Project-Id'     => FUNFASY_ID,
			'X-Project-Secret' => FUNFASY_SECRET,
		]
	],
];

// Режим парсинга постов в Телеграм
$config['tg_parse_mode'] = TGPARSEMODE_MARKDOWN;

// Прокси для Телеграм
$config['telegram_proxy'] = [
//    'type'   => CURLPROXY_SOCKS5,         // Валидная константа CURL: CURLPROXY_HTTP|CURLPROXY_SOCKS4|CURLPROXY_SOCKS5
//    'proxy'  => '',
//    'usrpwd' => ''
];

// Параметры шаблонизатора
$config['twigconfig'] = [
    'paths' => [
		VIEWPATH, 
		VIEWPATH.'js'.DIRECTORY_SEPARATOR,
		VIEWPATH.'authtwig'.DIRECTORY_SEPARATOR,
	]
];
