<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
 *--------------------------------------------------------------------------
 * Пользовательские константы
 *--------------------------------------------------------------------------
 */

// Режим технического обслуживания и версия движка
defined('MAINTENANCE') OR define('MAINTENANCE', false);
defined('VERSION') OR define('VERSION', '1.27');

// Email'ы для оповещений
defined('EMAIL_ALERT') OR define('EMAIL_ALERT', 'sysinform@validpost.com');

// Телеграм бот для публикации постов тиражей в чаты/каналы
defined('TELEGRAM_BOT_TOKEN') OR define('TELEGRAM_BOT_TOKEN', '1235761037:AAF4aXtIbGvIaUMpaOabbnUIg8BwOXbbTLw');
defined('TELEGRAM_BOT_TOKEN') OR define('TELEGRAM_BOT_TOKEN2', '');

// Разрешение на выплатные транзакции и постинг в Телеграм
// Постинг в Телеграм отключать в особых случаях, иначе транзы будут проверены, а соотв. посты НЕ будут опубликованы
defined('PAYMENT_ON')   OR define('PAYMENT_ON', true);
defined('TGPOSTING_ON') OR define('TGPOSTING_ON', true);

// Приблизительное число запросов до переподключения соединения с БД. Юзается в Mhworker, Mhdata, Trx
// Служит для предотвращения переполнения памяти в движке БД CI при длительной работе консольных скриптов
defined('QUERIES_TO_RECONNECT')	OR define('QUERIES_TO_RECONNECT', 100);

// Максимум постов в телеграм за один раз (на один вызов Checkout)
defined('TG_MAX_POSTS_ONCE')	OR define('TG_MAX_POSTS_ONCE', 8);

// Макс. неудачных попыток перевода средств на ОДНУ операцию, прежде чем отбой
defined('MINTERPAY_MAXTRIES')	OR define('MINTERPAY_MAXTRIES', 5);

// Время до автозапуска следующего тиража в чате после завершения текущего, секунд
defined('AUTORUN_INTERVAL')		OR define('AUTORUN_INTERVAL', 30);

// Режим обработки текста в Телеграм постах
defined('TGPARSEMODE_MARKDOWN')	OR define('TGPARSEMODE_MARKDOWN', 'Markdown');
defined('TGPARSEMODE_HTML')		OR define('TGPARSEMODE_HTML',     'HTML');

// Режим проведения тиража. Значения должны совпадать со значениями enum-поля `mode` в таблице тиражей
defined('DRAWMODE_MANUAL')	OR define('DRAWMODE_MANUAL',	'manual');

// Статус тиража. Значения должны совпадать со значениями enum-поля `status` в таблице тиражей
defined('DRAWSTATUS_WAIT')	OR define('DRAWSTATUS_WAIT',  'wait');
defined('DRAWSTATUS_RUN')	OR define('DRAWSTATUS_RUN',   'run');
defined('DRAWSTATUS_DONE')	OR define('DRAWSTATUS_DONE',  'done');
defined('DRAWSTATUS_BREAK')	OR define('DRAWSTATUS_BREAK', 'break');

// Выплатные транзакции после розыгрыша тиража. Значения должны соответствовать полям в таблице тиражей
defined('DRAWTX_PAY')		OR define('DRAWTX_PAY',		 'txpay');
defined('DRAWTX_CASHBACK')	OR define('DRAWTX_CASHBACK', 'txcashback');
defined('DRAWTX_PROFIT')	OR define('DRAWTX_PROFIT',	 'txprofit');

// Статус транзакции. Значения должны совпадать со значениями enum-поля `status` в таблице транзакций
defined('TXSTATUS_EVEN')	OR define('TXSTATUS_EVEN',		'even');		// Ровное число оплаченных билетов
defined('TXSTATUS_OVERPAY')	OR define('TXSTATUS_OVERPAY',	'overpay');		// Переплатили лишнего
defined('TXSTATUS_EXCESS')	OR define('TXSTATUS_EXCESS',	'excess');		// Лишние билеты у крайнего участника тиража
defined('TXSTATUS_BADCOIN')	OR define('TXSTATUS_BADCOIN',	'badcoin');		// Оплачена неверная монета
defined('TXSTATUS_LATE')	OR define('TXSTATUS_LATE',		'late');		// Опоздал, тираж уже разыгран

// Тексты пейлоадов для возврата ошибочных оплат
defined('PAYLOAD_BADCOIN')	OR define('PAYLOAD_BADCOIN',	'Wrong coin, pay only in ');				// Не та монета, нужно
defined('PAYLOAD_LATE')		OR define('PAYLOAD_LATE',		'This draw is already completed');			// Этот тираж уже завершен
defined('PAYLOAD_WINNERS')	OR define('PAYLOAD_WINNERS',	'Congrats to the lottery winners ');		// Поздравляем победителей лотереи
defined('PAYLOAD_CASHBACK')	OR define('PAYLOAD_CASHBACK',	'Cashback to the lottery participants ');	// Кэшбек участникам лотереи
defined('PAYLOAD_PROFIT')	OR define('PAYLOAD_PROFIT',		'');

// Тексты для кнопок с транзакцими выплат
defined('BTN_TXWINNERS')	OR define('BTN_TXWINNERS',		'Payment transaction');
defined('BTN_TXCASHBACK')	OR define('BTN_TXCASHBACK',		'Cashback transaction');

// Коды действий на странице управления тиражами
defined('ACTION_SHOWRUN')	OR define('ACTION_SHOWRUN', 	'showrun');
defined('ACTION_SHOWRUN2')	OR define('ACTION_SHOWRUN2', 	'showrun2');
defined('ACTION_SHOWPLAY')	OR define('ACTION_SHOWPLAY', 	'showplay');
defined('ACTION_SHOWPLAY2')	OR define('ACTION_SHOWPLAY2', 	'showplay2');
defined('ACTION_SHOWINFO')	OR define('ACTION_SHOWINFO', 	'showinfo');
defined('ACTION_SHOWINFO2')	OR define('ACTION_SHOWINFO2', 	'showinfo2');
defined('ACTION_RUNDRAW')	OR define('ACTION_RUNDRAW', 	'rundraw');
defined('ACTION_RUNPOST')	OR define('ACTION_RUNPOST', 	'runpost');
defined('ACTION_PLAYOUT')	OR define('ACTION_PLAYOUT',		'playout');
defined('ACTION_SYNC')		OR define('ACTION_SYNC',		'sync');
defined('ACTION_PLAYPOST')	OR define('ACTION_PLAYPOST',	'playpost');
defined('ACTION_INFOPOST')	OR define('ACTION_INFOPOST',	'infopost');
defined('ACTION_BREAKDRAW')	OR define('ACTION_BREAKDRAW',	'break');
defined('ACTION_DELDRAW')	OR define('ACTION_DELDRAW',		'delete');

// Типы событий для форм на странице управления тиражами для вызова соотв. js-обработчика
defined('EVENT_ACTION')		OR define('EVENT_ACTION',		'event-action');
defined('EVENT_PREVIEW')	OR define('EVENT_PREVIEW',		'event-preview');

// Типы кнопок для страницы управления тиражами, они же Bootstrap-стили
defined('BTN_DEFAULT')	OR define('BTN_DEFAULT', 'btn-default');
defined('BTN_PRIMARY')	OR define('BTN_PRIMARY', 'btn-primary');
defined('BTN_SUCCESS')	OR define('BTN_SUCCESS', 'btn-success');
defined('BTN_INFO')		OR define('BTN_INFO',    'btn-info');
defined('BTN_WARNING')	OR define('BTN_WARNING', 'btn-warning');
defined('BTN_DANGER')	OR define('BTN_DANGER',  'btn-danger');

// Коды постов в выбранном комплекте шаблонов
defined('TEXTCODE_RUNMANUAL')	OR define('TEXTCODE_RUNMANUAL',		'runmnl');
defined('TEXTCODE_RUNMANUAL2')	OR define('TEXTCODE_RUNMANUAL2',	'runmnl2');
defined('TEXTCODE_INFO')		OR define('TEXTCODE_INFO',			'info');
defined('TEXTCODE_INFO2')		OR define('TEXTCODE_INFO2',			'info2');
defined('TEXTCODE_PLAYMANUAL')	OR define('TEXTCODE_PLAYMANUAL',	'playmnl');
defined('TEXTCODE_PLAYMANUAL2')	OR define('TEXTCODE_PLAYMANUAL2',	'playmnl2');

// Имена полей, в которых хранятся ID опубликованных постов
defined('POSTID_RUN')	OR define('POSTID_RUN',   'runpostid');
defined('POSTID_RUN2')	OR define('POSTID_RUN2',  'runpostid2');
defined('POSTID_INFO')	OR define('POSTID_INFO',  'infopostid');
defined('POSTID_INFO2')	OR define('POSTID_INFO2', 'infopostid2');
defined('POSTID_END')	OR define('POSTID_END',   'endpostid');
defined('POSTID_END2')	OR define('POSTID_END2',  'endpostid2');

// Имена кук и срок их жизни
defined('COOKIE_LOTTERYTITLE')	OR define('COOKIE_LOTTERYTITLE', 	'lotterytitle');
defined('COOKIE_MODESELECTED')	OR define('COOKIE_MODESELECTED',	'mode');
defined('COOKIE_WALLETSELECTED')OR define('COOKIE_WALLETSELECTED',	'wallet');
defined('COOKIE_COINSELECTED')	OR define('COOKIE_COINSELECTED',	'coin');
defined('COOKIE_CHATSELECTED')	OR define('COOKIE_CHATSELECTED', 	'chatselected');
defined('COOKIE_TPLSELECTED')	OR define('COOKIE_TPLSELECTED',		'tplselected');
defined('COOKIEEXPIRY_1YEAR')   OR define('COOKIEEXPIRY_1YEAR', 86400 * 356);

// Избранные эможди для кнопок и прочего (д.б. строго в двойных кавычках!)
defined('EMOJI_EYES')	OR define('EMOJI_EYES',   "\xF0\x9F\x91\x80");					// 👀
defined('EMOJI_ROCKET')	OR define('EMOJI_ROCKET', "\xF0\x9F\x9A\x80");					// 🚀
defined('EMOJI_FLAGRU')	OR define('EMOJI_FLAGRU', "\xF0\x9F\x87\xB7\xF0\x9F\x87\xBA");	// 🇷🇺
defined('EMOJI_FLAGUK')	OR define('EMOJI_FLAGUK', "\xF0\x9F\x87\xAC\xF0\x9F\x87\xA7");	// 🇬🇧
defined('EMOJI_USDFLY')	OR define('EMOJI_USDFLY', "\xF0\x9F\x92\xB8");					// 💸

// Время отображения завершенных/прерванных тиражей в панели управления, часов
defined('CLOSED_DRAW_POSTVIEW')	OR define('CLOSED_DRAW_POSTVIEW', 6);

// Время проверки завершенных тиражей, в течение которого будут сделаны возвраты ошибочных оплат
defined('CLOSED_DRAW_POSTCHECK')OR define('CLOSED_DRAW_POSTCHECK', 2);

// Параметры для статистики
defined('STAT_MAX_TRXS_VIEW')	OR define('STAT_MAX_TRXS_VIEW',   200);		// Макс. последних отображаемых транз
defined('STAT_MIN_WALLET_TRXS')	OR define('STAT_MIN_WALLET_TRXS', 1);		// Мин. покупок кошелька для показа в стате
defined('STAT_MAX_BUYERS_VIEW')	OR define('STAT_MAX_BUYERS_VIEW', 200);		// Макс. кошельков покупателей показываем в стате
defined('STAT_MIN_POWER_PAY')	OR define('STAT_MIN_POWER_PAY',   500);		// Всегда показываем кошельки, оплатившие суммы от

// Minterscan префикс для транзакций
defined('MSCAN_TXPREFIX')   OR define('MSCAN_TXPREFIX', 'https://minterscan.net/tx/');

// Ссылка Bip.dev API для загрузки курса + Интервал обновления курса BIP, мин.
defined('BIPDEV_APIURL_GETPRICE') or define('BIPDEV_APIURL_GETPRICE', 'https://api.bip.dev/api/price');
defined('BIPDEV_UPDATE_INTERVAL') or define('BIPDEV_UPDATE_INTERVAL', 180);


/*--------------------------------------------------------------------------
 * Параметры нод
 *--------------------------------------------------------------------------
 */
// Ключи доступа к API
defined('MSCAN_APIKEY')			OR define('MSCAN_APIKEY',   '6d19f27c-6248-5dfd-af4a-dfedc39ed0e1');
defined('FUNFASY_ID')			OR define('FUNFASY_ID',     'a5084f8d-427b-4043-810d-9234c4aaef3c');
defined('FUNFASY_SECRET')		OR define('FUNFASY_SECRET', '1555bb7baacba4833e8538273776e255');

// Параметры для парсера
defined('PARSER_QUERY_PREFIX')	OR define('PARSER_QUERY_PREFIX', 'transactions?query=');
defined('PARSER_CURLTIMEOUT')	OR define('PARSER_CURLTIMEOUT',     15);	// CURL таймаут в секундах
defined('PARSER_MAXFAILS_4NODE') OR define('PARSER_MAXFAILS_4NODE', 3);		// Макс. неудачных попыток парсинга на ноду
defined('PARSER_MAXFAILS_TOTAL') OR define('PARSER_MAXFAILS_TOTAL', 10);	// Макс. неудачных попыток парсинга всего

// Параметры для запросов к API
defined('MINTERAPI_CONNTIMEOUT') OR define('MINTERAPI_CONNTIMEOUT', 10);
defined('MINTERAPI_TIMEOUT')     OR define('MINTERAPI_TIMEOUT',     30);

// Коды транзакций Минтер
defined('TXTYPE_SEND')     		OR define('TXTYPE_SEND',     0x01);
defined('TXTYPE_DELEGATE')		OR define('TXTYPE_DELEGATE', 0x07);


/*--------------------------------------------------------------------------
 * Параметры бэкапов БД
 *--------------------------------------------------------------------------
 */
defined('DBBACKUP_PATH')     OR define('DBBACKUP_PATH', APPPATH . 'dbbackups' . DIRECTORY_SEPARATOR);
defined('DBBACKUP_ARCHTYPE') OR define('DBBACKUP_ARCHTYPE', 'zip');
defined('DBBACKUP_MAXFILES') OR define('DBBACKUP_MAXFILES', 24);
defined('DBBACKUP_MAXDAYS')  OR define('DBBACKUP_MAXDAYS',  2);


/*--------------------------------------------------------------------------
 * Служебные константы - НЕ ТРОГАТЬ!!!
 *--------------------------------------------------------------------------
 */

// Таблицы в БД
defined('DBTBL_PERMITS')	OR define('DBTBL_PERMITS',	'le_permits');
defined('DBTBL_CHATS')		OR define('DBTBL_CHATS',	'le_chats');
defined('DBTBL_DRAWS')		OR define('DBTBL_DRAWS',	'le_draws');
defined('DBTBL_PRESETS')	OR define('DBTBL_PRESETS',	'le_presets');
defined('DBTBL_TEMPLATES')	OR define('DBTBL_TEMPLATES','le_templates');
defined('DBTBL_TRXS')		OR define('DBTBL_TRXS',		'le_trxs');
defined('DBTBL_BCPAGES')	OR define('DBTBL_BCPAGES',	'le_bcpages');
defined('DBTBL_MHDATA')		OR define('DBTBL_MHDATA',	'le_mhdata');
defined('DBTBL_BIPDEV')		OR define('DBTBL_BIPDEV',	'le_bipdev');

// BIP & PIPS
defined('PIPS') OR define('PIPS', 1000000000000000000);		// 1e18
defined('PRECISION') OR define('PRECISION', 9);				// Точность округления

// Универсальный формат даты и времени
defined('YMDHIS') OR define('YMDHIS', 'Y-m-d H:i:s');

/*
|--------------------------------------------------------------------------
| Display Debug backtrace
|--------------------------------------------------------------------------
|
| If set to TRUE, a backtrace will be displayed along with php errors. If
| error_reporting is disabled, the backtrace will not display, regardless
| of this setting
|
*/
defined('SHOW_DEBUG_BACKTRACE') OR define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
defined('FILE_READ_MODE')  OR define('FILE_READ_MODE', 0644);
defined('FILE_WRITE_MODE') OR define('FILE_WRITE_MODE', 0666);
defined('DIR_READ_MODE')   OR define('DIR_READ_MODE', 0755);
defined('DIR_WRITE_MODE')  OR define('DIR_WRITE_MODE', 0755);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/
defined('FOPEN_READ')                           OR define('FOPEN_READ', 'rb');
defined('FOPEN_READ_WRITE')                     OR define('FOPEN_READ_WRITE', 'r+b');
defined('FOPEN_WRITE_CREATE_DESTRUCTIVE')       OR define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
defined('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE')  OR define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
defined('FOPEN_WRITE_CREATE')                   OR define('FOPEN_WRITE_CREATE', 'ab');
defined('FOPEN_READ_WRITE_CREATE')              OR define('FOPEN_READ_WRITE_CREATE', 'a+b');
defined('FOPEN_WRITE_CREATE_STRICT')            OR define('FOPEN_WRITE_CREATE_STRICT', 'xb');
defined('FOPEN_READ_WRITE_CREATE_STRICT')       OR define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
|--------------------------------------------------------------------------
| Exit Status Codes
|--------------------------------------------------------------------------
|
| Used to indicate the conditions under which the script is exit()ing.
| While there is no universal standard for error codes, there are some
| broad conventions.  Three such conventions are mentioned below, for
| those who wish to make use of them.  The CodeIgniter defaults were
| chosen for the least overlap with these conventions, while still
| leaving room for others to be defined in future versions and user
| applications.
|
| The three main conventions used for determining exit status codes
| are as follows:
|
|    Standard C/C++ Library (stdlibc):
|       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
|       (This link also contains other GNU-specific conventions)
|    BSD sysexits.h:
|       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
|    Bash scripting:
|       http://tldp.org/LDP/abs/html/exitcodes.html
|
*/
defined('EXIT_SUCCESS')        OR define('EXIT_SUCCESS', 0); // no errors
defined('EXIT_ERROR')          OR define('EXIT_ERROR', 1); // generic error
defined('EXIT_CONFIG')         OR define('EXIT_CONFIG', 3); // configuration error
defined('EXIT_UNKNOWN_FILE')   OR define('EXIT_UNKNOWN_FILE', 4); // file not found
defined('EXIT_UNKNOWN_CLASS')  OR define('EXIT_UNKNOWN_CLASS', 5); // unknown class
defined('EXIT_UNKNOWN_METHOD') OR define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
defined('EXIT_USER_INPUT')     OR define('EXIT_USER_INPUT', 7); // invalid user input
defined('EXIT_DATABASE')       OR define('EXIT_DATABASE', 8); // database error
defined('EXIT__AUTO_MIN')      OR define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
defined('EXIT__AUTO_MAX')      OR define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code
