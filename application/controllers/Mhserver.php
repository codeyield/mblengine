<?php
declare(ticks = 1);
defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * Консольный сервер прослушивания транзакций в блокчейне Minter
 * 
 * СТАРТУЕМ С СОХРАНЁННОГО БЛОКА И ДОГОНЯЕМ ТЕКУЩИЙ (без параметров):
 * php index.php mhserver
 * php /var/www/lotto/data/www/vps20103.xxvps.net/index.php mhserver > /var/www/lotto/data/www/lotto.system/application/logs/mhserver.log
 * 
 * СТАРТУЕМ С ТЕКУЩЕГО БЛОКА БЛОКЧЕЙНА:
 * php index.php mhserver 0
 * php /var/www/lotto/data/www/vps20103.xxvps.net/index.php mhserver 0
 * 
 * СТАРТУЕМ С УКАЗАННОГО БЛОКА И ДОГОНЯЕМ ТЕКУЩИЙ:
 * php index.php mhserver 3298897
 * php /var/www/lotto/data/www/vps20103.xxvps.net/index.php mhserver 3298897
 */

define('SCRIPTNAME', 'MHserver');

const BLOCKDELAY    = 5.0;				// Интервал проверки между блоками, секунд (float)
const SOMEDELAY     = 1.5;				// Сколько дополнительно подождать, если обогнали появление новых блоков, секунд (float)
const MICRODELAY    = 100;				// Принудительная задержка между итерациями цикла, милисекунд (int)

const WALLETS_RELOAD_DELAY  = 10;		// Интервал переполучения списка кошельков из БД, секунд
const NODEFAILS_RESET_DELAY = 3600;		// Интервал сброса/обнуления счетчика ошибок нод, секунд

const TP_WHILE_NEXTSTEP  = 'nxt';		// Условное имя тайм-поинта для одного шага прохода цикла
const TP_WALLETS_RELOAD  = 'wtp';		// Условное имя тайм-поинта для переполучения списка кошельков
const TP_NODEFAILS_RESET = 'nfr';		// Условное имя тайм-поинта для сброса/обнуления счетчика ошибок нод

const MAXBLOCKLIMIT = null;				// Блок останова скрипта перед апдейтом блокчейна или null, если не требуется


use Minter\MinterAPI;
use GuzzleHttp\Exception\RequestException;


class Mhserver extends CI_Controller {

	public function index($fromBlock = null) {
		
		set_time_limit(0);

		if(!is_cli()) {
			echo "This script may be running in console mode only\n";
			exit(1);
		}
		
		// Обработчик завершения скрипта для консольного режима
		function sig_handler($signo) {
			
			switch ($signo) {
				case SIGINT:	$msg = 'Terminated by user';			break;
				case SIGTERM:	$msg = 'Process killed';				break;
				case SIGHUP:	$msg = 'Restart signal was received';	break;
			}
			
			log_message('error', SCRIPTNAME . ': ' . $msg);
			@mail(EMAIL_ALERT, SCRIPTNAME . ' ALERT', $msg);
			// $this->db->close();
			
			exit(1);
		}

		// Устанавливаем обработчики сигналов, если скрипт запущен под NIX*
		if(strtoupper(substr(PHP_OS, 0, 3)) !== 'WIN') {
			pcntl_signal(SIGINT,  'sig_handler');	// Ctrl+C
			pcntl_signal(SIGTERM, 'sig_handler');	// killall myscript / kill <PID>
			pcntl_signal(SIGHUP,  'sig_handler');	// обрыв связи
		}
		
		
		$this->load->model(['runtime', 'failer', 'mhdata']);
		
		// Получаем список API нод и инициализируем счетчики ошибок
		$nodes = $this->config->item('minter_nodes_api');
		$this->failer->init(array_keys($nodes));
		
		$apis = [];
		// Формируем массив MinterAPI объектов для каждой рабочей ноды
		foreach($nodes as $key => $node) {
			$apis[$key] = new MinterAPI(new \GuzzleHttp\Client($node));
		}
		
		
		// Первично получаем из БД список кошельков тиражей и устанавливаем тайм-поинт их переполучения
		$wallets = $this->mhdata->getCheckoutWallets();
		$this->runtime->start(TP_WALLETS_RELOAD);
		
		// Устанавливаем тайм-поинт обнуления счетчиков ошибок нод
		$this->runtime->start(TP_NODEFAILS_RESET);
		
		
		// Получаем сохранённый в БД № блока
		$savedBlock = $this->mhdata->getBlockNumber();
		
		// Без входного параметра - стартуем со следующего блока после сохраненного в БД (будем догонять)
		// Входной параметр = 0 - стартуем с текущего блока блокчейна
		$fromBlock = is_null($fromBlock) ? $savedBlock + 1 : (int) $fromBlock;
		$block = $fromBlock <= 1 ? 0 : $fromBlock;
		
		// Входной параметр = № блока - сохраняем в БД предыдущий блок и стартуем с указанного
		if(($block > 0) and ($block <= $savedBlock)) {
			$this->mhdata->putBlockNumber($savedBlock = $block - 1);
		}
		
		// Текущий блок блокчейна будет сразу же получен внутри цикла
		$nowBlock = 0;
		
		
		// ПОЕХАЛИ!
		while(true) {
		
			// Завершение скрипта при обновлении блокчейна на блоке MAXBLOCKLIMIT
			if((MAXBLOCKLIMIT !== null) and ($block >= MAXBLOCKLIMIT)) {
				log_message('error', SCRIPTNAME . ': Aborted while blockhain will updated');
				$this->db->close();
				exit(1);
			}
			
			usleep(MICRODELAY * 1000);
			$this->runtime->start(TP_WHILE_NEXTSTEP);		// Тайм-поинт времени прохода цикла
			
			
			// Сбрасываем/обнуляем счетчики ошибок нод, если истёк интервал
			if($this->runtime->isExpiry(TP_NODEFAILS_RESET, NODEFAILS_RESET_DELAY)) {
				$this->failer->reset();
				$this->runtime->start(TP_NODEFAILS_RESET);
			}
			
			
			// Переполучаем список кошельков из БД, если истёк интервал
			if($this->runtime->isExpiry(TP_WALLETS_RELOAD, WALLETS_RELOAD_DELAY)) {
				$wallets = $this->mhdata->getCheckoutWallets();
				$this->runtime->start(TP_WALLETS_RELOAD);
			}
			
			// Не насилуем API, если нет кошельков для мониторинга. Как только они появятся, работа возобновится с текущего блока блокчейна
			if(count($wallets) == 0) {
				$block = $nowBlock = 0;
				continue;
			}
			
			
			// Получаем № текущего блока блокчейна впервые или каждый раз, когда обрабатываемый блок догнал текущий
			if(($nowBlock == 0) || ($block >= $nowBlock)) {
				
				try {
					// Выбираем ключ наиболее стабильной ноды
					$nodekey = $this->failer->getOptKey();
					
					$response = $apis[$nodekey]->getStatus();

					if(isset($response->result->latest_block_height)) {
						$nowBlock = (int) $response->result->latest_block_height;
					}
					else {
						continue;			// № блока не получен - повторяем попытку в новой итерации
					}

				} catch(RequestException $e) {
					log_message('error', SCRIPTNAME . ': ' . $e->getMessage());
					$this->failer->countErr($nodekey);	// Инкрементируем счетчик ошибок ноды
					continue;							// № блока не получен - повторяем попытку в новой итерации
				}
			}
			
			// Если обогнали формирование блоков в блокчейне, то подождем еще немного и заново запросим № текущего блока
			if($block > $nowBlock) {
				usleep((int) (SOMEDELAY * 1000000));
				continue;
			}
			
			// Если входной параметр был равен нулю, то стартуем с текущего блока блокчейна
			$block = ($block == 0) ? $nowBlock : $block;
			
			
			// Запрашиваем получение транзакций в блоке только если № блока еще не был сохранён в БД как обработанный
			if($block > $savedBlock) {
				
				try {
					// Выбираем ключ наиболее стабильной ноды
					$nodekey = $this->failer->getOptKey();
					
					$response = $apis[$nodekey]->getBlock($block);

					// При наличии транзакций в блоке выполняем проверку на совпадение
					if(isset($response->result->transactions)) {

						foreach($response->result->transactions as $trx) {

							// Перечень действий для обнаруженных подходящих транзакций
							switch ($trx->type) {
								
								case TXTYPE_SEND:
									
									if($this->mhdata->isWalletFound($trx->data->to, $wallets)) {
										echo "Send trx matched in {$block}: {$trx->hash}\n";
										$result = $this->mhdata->addTrxTypeSend($trx, $response->result->height, $response->result->time);
									}
									break;
								
								case TXTYPE_DELEGATE:
									
									if($this->mhdata->isWalletFound($trx->data->from, $wallets)) {
										echo "Delegate trx matched in {$block}: {$trx->hash}\n";
										$result = $this->mhdata->addTrxTypeDelegate($trx, $response->result->height, $response->result->time);
									}
									break;
							}
						}
					}
					
					// Блок считается проверенным независимо от наличия в нём транзакций и тем более совпадений кошельков!
					if(isset($response->result->height)) {
						
						// Сохраняем проверенный № блока в БД
						$this->mhdata->putBlockNumber($savedBlock = $block);

						// Выводим проверенный № блока в браузере или в консоли
						echo $savedBlock < $nowBlock ? "{$savedBlock} -> {$nowBlock}\r" : "{$savedBlock}            \r";
			
						// Задержка на ОСТАТОК времени до истечения интервала BLOCKDELAY. Если догоняем блоки, то задержку не применяем
						// Это единственное место, где инкрементируется $block внутри цикла - редактируем осторожно!
						if($block++ >= $nowBlock) {
							usleep((int) ($this->runtime->getTimeLeft(TP_WHILE_NEXTSTEP, BLOCKDELAY) * 1000000));
						}
					
					}
				} catch(RequestException $e) {
					log_message('error', SCRIPTNAME . ': ' . $e->getMessage());
					$this->failer->countErr($nodekey);	// Инкрементируем счетчик ошибок ноды
				}
			}
		
		}	// end while(true)
		
	}
	
}