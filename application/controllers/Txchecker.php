<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * Парсит блокчейн по указанному кошельку и выполняет сверку транзакций тиража в БД с блокчейном
 */

const SCRIPTNAME = 'Txchecker';

class Txchecker extends CI_Controller {
	
    public function __construct() {
        parent::__construct();
		
		$this->load->model(['draw', 'failer', 'minter', 'trx']);
//		$this->load->model(['text', 'playout', 'tgposter', 'chat', 'checkout', 'mhdata']);
		$this->load->library('curl');
	}
	
	/* 
	 * Парсит блокчейн по указанному кошельку и выполняет сверку транзакций тиража в БД с блокчейном
	 */
	public function index() {
		
		// Получаем полный список транз из БД
		$dbData = $this->trx->getDrawStat('MBL1');
		$dbTrxsIds = array_column($dbData, 'trx');
		
		
		// Таймаут побольше, т.к. за ≈7 сек. ноды не успевают отвечать
		$this->curl->timeout = PARSER_CURLTIMEOUT;
		
		// Получаем список API URL всех имеющихся нод и создаём массив со счетчиками ошибок нод
		$nodes = $this->config->item('minter_nodes');
		
		$nodeId = 'minterone';
		$wallet = 'Mxd1267678bc5a37e7ab5ff6375ba5ebd391de2f36';
		$page     = 31;
		$pageStop = 40;
		
		$trxs = [];
		$totalFails = 0;

		echo '<pre>';

		/* ===========================================================================
		 * Парсим нужное число страниц страниц транзакций по кошельку
		 */
		do {
			/* ===========================================================================
			 * Получаем данные из блокчейна по API. Пример запроса:
			 * https://api.minter.one/transactions?query=%22tags.tx.to%3D%27ff88a0a2c080f7318b870512c3f0d382ac512fa4%27+AND+tags.tx.type%3D%2701%27%22&page=1
			 */
			$query   = urlencode("\"tags.tx.to='". substr($wallet, 2). "' AND tags.tx.type='01'\"") . "&page={$page}";

			$apiurl  = $nodes[$nodeId] . PARSER_QUERY_PREFIX . $query;
			$rawdata = $this->curl->get($apiurl);

			if($rawdata == false) {
				log_message('error', SCRIPTNAME . ": Page downloading fail by URL:\r\n" . $apiurl);
				log_message('error', SCRIPTNAME . ": CURL error #" . $this->curl->error['code']);
				log_message('error', SCRIPTNAME . ": CURL message: " . $this->curl->error['msg']);

				$totalFails++;
				continue;				// Перезапускаем цикл заново на той же странице
			}

			$trxsPage = json_decode($rawdata);

			// Для безопасной проверки в while, если json вернёт ошибку
			$countTrxs = 999;

			// Невалидный JSON
			if(empty($trxsPage)) {
				log_message('error', SCRIPTNAME . ": Json decoding fail by URL:\r\n" . $apiurl);
				$totalFails++;
				continue;
			}
			// Вернулась ошибка
			elseif(isset($trxsPage->error)) {
				log_message('error', SCRIPTNAME . ": Json error:\r\n" . print_r($trxsPage, 1));
				$totalFails++;
				continue;
			}
			// Распарсили, все ОК
			else {
				// Транзакций на странице
				$countTrxs = count($trxsPage->result);
				
				$trxs = (array) $trxsPage->result;
				
				foreach($trxs as $trx) {
					
					$trxId = strtolower($trx->hash);
					if(!in_array($trxId, $dbTrxsIds)) {
						
						echo "Tx NOT FOUND: {$trxId}\r\n";
						print_r($trx);
					}
				}
				
				// Выводим статистику парсинга в браузер
				echo "Parsed page: {$page} / Txs loaded: ".count($trxsPage->result)."\r\n";

				$page++;
			}

		} while(    ($countTrxs >= 100) 					// Еще НЕ последняя страница c данными по кошельку
				and ($page <= $pageStop)					// Парсим до страницы останова включ. (но не более, чем существующих страниц)
				and ($totalFails < PARSER_MAXFAILS_TOTAL)	// Максимум ошибок еще не достигнут
			   );
		
		echo 'Done!';
    }
	
}