<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Скачивает список всех транзакций + список размноженных кошельков в zip-архиве
 * 
 * https://vps20103.xxvps.net/get_txs_and_wallets
 */
class Get_txs_and_wallets extends CI_Controller {
	
    public function __construct() {
        parent::__construct();
		
		$this->load->library(['ion_auth']);
		$this->load->helper(['url', 'language']);
		
		if ( !($this->is_loggedin = $this->ion_auth->logged_in()) ) {
			redirect('auth/login', 'refresh');
		}
		
		/* 
		 * Доступы юзера:
		 * @param bool   $this->permits->is_granted   Права юзера подтверждены
		 * @param bool   $this->permits->is_demo      Демо-режим (кнопки неактивны)
		 * @param array  $this->permits->chats        Список доступных юзеру телеграм чатов/каналов
		 * @param array  $this->permits->tplcodes     Список имен шаблонов для постов для выбранного чата/канала
		 * @param string $this->permits->chatselected Текущий выбранный юзером чат/канал
		 * @param string $this->permits->tplselected  Текущее выбранное юзером имя шаблона для постов
		 * @param string $this->permits->useremail    Авторизационный email юзера
		 */
		$this->load->model('permits');
		$this->permits->load();
		
		$this->load->model(['draw', 'trx']);
	}

	/**
	 * [[Description]]
	 */
	public function index() {

		if($this->is_loggedin and $this->permits->is_granted) {
		
			$drawcode = 'MBL1';

			$result = $this->db
				->select('blocktime,block,trx,wallet,bets')
				->where('drawcode', $drawcode)
				->where('bets > 0', null, false)	// Чтобы не считать левые монеты, т.к. для них bets = 0
				->where_in('status', [TXSTATUS_EVEN, TXSTATUS_OVERPAY, TXSTATUS_EXCESS])
				->order_by('block', 'ASC')
				->get(DBTBL_TRXS);

			$txs = $result->result_array();

			// Собираем полный список транз
			$csvTxs = '';
			foreach($txs as $tx) {
				$tx['trx'] = 'Mt' . $tx['trx'];
				$csvTxs .= implode(",", $tx) . "\n";
			}

			// Размножаем кошельки пропорционально числу купленных билетов
			$wallets = [];
			foreach($txs as $tx) {
				$wallets = array_merge($wallets, array_fill(0, $tx['bets'], $tx['wallet']));
			}

			// Сохраняем файлы в zip-архив в папке кэша и отдаем его в браузер на скачивание
			$this->load->library('zip');

			$this->zip->add_data('transactions.txt', $csvTxs);
			$this->zip->add_data('wallets.txt', implode("\n", $wallets));

			$this->zip->download('data.zip');

		}
    
    }

}