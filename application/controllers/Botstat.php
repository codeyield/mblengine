<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Botstat extends CI_Controller {

// Залогинен ли юзер
private $is_loggedin = false;


    public function __construct() {
        parent::__construct();
		
		$this->load->library(['ion_auth', 'form_validation']);
		$this->load->helper(['url', 'language']);
		
		if ( !($this->is_loggedin = $this->ion_auth->logged_in()) ) {
			redirect('auth/login', 'refresh');
		}
		
		/* 
		 * Доступы юзера:
		 * @param bool   $this->permits->is_granted   Права юзера подтверждены
		 * @param bool   $this->permits->is_demo      Демо-режим (кнопки неактивны)
		 * @param array  $this->permits->chats        Список доступных юзеру телеграм чатов/каналов
		 * @param array  $this->permits->tplcodes     Список имен шаблонов для постов для выбранного чата/канала
		 * @param string $this->permits->chatselected Текущий выбранный юзером чат/канал
		 * @param string $this->permits->tplselected  Текущее выбранное юзером имя шаблона для постов
		 * @param string $this->permits->useremail    Авторизационный email юзера
		 */
		$this->load->model('permits');
		$this->permits->load();
		
		$this->load->model(['draw', 'failer', 'minter', 'trx']);
		$this->load->model(['botdata']);
	}
	
	/**
	 * Отображение страницы статистики тиражей
	 */
	public function index() {
        
		if (MAINTENANCE) {
			$this->load->view('maintenance.html');
		}
		elseif ($this->is_loggedin) {

			// Загружаем шаблонизатор
			$this->load->library('twig', $this->config->item('twigconfig'));
			$this->twig->addGlobal('sitetitle', $this->config->item('sitetitle'));
			$this->twig->addGlobal('environment', ENVIRONMENT);
			$this->twig->addGlobal('base_url', base_url());
			$this->twig->addGlobal('uri_string', uri_string());
			
			if ($this->permits->is_granted) {
				
				$stat = [];
				$users = $this->botdata->getUsers();
				
				// Сумма выплат всем юзерам бота
				$topay = array_sum(array_column($users, 'to_be_payed'));
				
				// Страница статистики тиражей
				$this->twig->display('botstat', [
					'csrf'		=> ['name' => $this->security->get_csrf_token_name(), 'hash' => $this->security->get_csrf_hash()],
					'is_demo'	=> $this->permits->is_demo,
					'chats'		=> $this->permits->chats,
					'users'		=> $users,
					'topay'		=> $topay,
					'selected'	=> [
						'chat'	=> $this->permits->chatselected,
					],
				]);
				
			}
			else {
				$this->twig->display('nopermits', []);
			}
        
		}
		else {
			redirect('auth/login', 'refresh');
		}
	
	}

}