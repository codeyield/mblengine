<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * Консольный воркер для действий с запущенными тиражами в реальном времени
 * 
 * php index.php dbbackup
 * php /var/www/apps/data/www/lotto.thinkapps.ru/index.php dbbackup
 */
class Dbbackup extends CI_Controller {
	
	public function index() {
		
		if(!is_cli()) {
			echo "This script may be running in console mode only\n";
			exit(1);
		}
		
		$this->load->dbutil();
		
		$dbFileName = 'db_'. date("Y-m-d_H-i-s");
		
		$prefs = [
//			'ignore'   => ['users'],			// Таблицу с пользователями исключаем для безопасности?
			'format'   => DBBACKUP_ARCHTYPE,             
			'filename' => $dbFileName . '.sql',
		];
		
		// Сохраняем бэкап
		$result = file_put_contents(
			DBBACKUP_PATH . $dbFileName . '.' . DBBACKUP_ARCHTYPE, 
			$this->dbutil->backup($prefs)
		);
		
		echo ($result !== false) ? $dbFileName . '.' . DBBACKUP_ARCHTYPE . " backup saved.\n" : "Backup error!\n";
		
		
		// Удаляем старые бэкапы
		$files = glob(DBBACKUP_PATH . '*.' . DBBACKUP_ARCHTYPE);
		
		$k = 0;
		if(count($files) > DBBACKUP_MAXFILES) {
			
			foreach($files as $file) {
				
				if((time() - filemtime($file)) > DBBACKUP_MAXDAYS * 24 * 60 * 60) {
					
					++$k;
					unlink($file);
				}
			}
		}
		
		echo ($k > 0) ? "{$k} old backups was deleted.\n" : '';
	}
	
}