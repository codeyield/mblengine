<?php
defined('BASEPATH') OR exit('No direct script access allowed');

const SCRIPTNAME = 'Control';


class Control extends CI_Controller {
	
    public function __construct() {
        parent::__construct();
		
		$this->load->library(['ion_auth', 'form_validation']);
		$this->load->helper(['url', 'language']);
		
		if ( !($this->is_loggedin = $this->ion_auth->logged_in()) ) {
			redirect('auth/login', 'refresh');
		}
		
		/* 
		 * Доступы юзера:
		 * @param bool   $this->permits->is_granted   Права юзера подтверждены
		 * @param bool   $this->permits->is_demo      Демо-режим (кнопки неактивны)
		 * @param array  $this->permits->chats        Список доступных юзеру телеграм чатов/каналов
		 * @param array  $this->permits->tplcodes     Список имен шаблонов для постов для выбранного чата/канала
		 * @param string $this->permits->chatselected Текущий выбранный юзером чат/канал
		 * @param string $this->permits->tplselected  Текущее выбранное юзером имя шаблона для постов
		 * @param string $this->permits->useremail    Авторизационный email юзера
		 */
		$this->load->model('permits');
		$this->permits->load();
		
		$this->load->model(['draw', 'failer', 'minter', 'trx']);
		$this->load->model(['text', 'playout', 'tgposter', 'chat', 'checkout']);
	}

	/**
	 * Отображение страницы управления тиражами
	 */
	public function index() {
		
		if (MAINTENANCE) {
			$this->load->view('maintenance.html');
		}
		elseif ($this->is_loggedin) {

			// Загружаем шаблонизатор
			$this->load->library('twig', $this->config->item('twigconfig'));
			$this->twig->addGlobal('sitetitle', $this->config->item('sitetitle'));
			$this->twig->addGlobal('environment', ENVIRONMENT);
			$this->twig->addGlobal('base_url', base_url());
			$this->twig->addGlobal('uri_string', uri_string());
			
			if ($this->permits->is_granted) {

				$acts = $this->config->item('actions');
				$drawmodes    = $this->config->item('drawmodes');
				$drawstatuses = $this->config->item('drawstatuses');

				// Перекомпоновка массива с типами розыгрышей в вид ['type' => 'description', ... ]
				$drawmodes = array_combine(array_column($drawmodes, 'type'), array_column($drawmodes, 'short'));
				
				// Основной массив с данными
				$draws = $this->draw->getActiveDraws($this->permits->chatselected);

				// Дополняем массив данных кнопками с логикой, зависимой от текущих параметров тиража
				foreach($draws as $key => $draw) {
					
					$buttons = [];

					if($draw['status'] == DRAWSTATUS_WAIT) {
						$buttons = [
							['type' => BTN_INFO]    + $acts[ACTION_SHOWRUN],
							['type' => BTN_INFO]    + $acts[ACTION_SHOWRUN2],
							['type' => BTN_SUCCESS] + $acts[ACTION_RUNDRAW],
							['type' => BTN_DANGER]  + $acts[ACTION_DELDRAW],
						];
					}
					
					// Тираж запущен, но ни одного билета еще не куплено
					if(($draw['status'] == DRAWSTATUS_RUN) and ($draw['nowbets'] == 0)) {
						$buttons = [
							['type' => BTN_INFO]    + $acts[ACTION_SHOWRUN],
							['type' => BTN_INFO]    + $acts[ACTION_SHOWRUN2],
							['type' => BTN_SUCCESS] + $acts[ACTION_RUNPOST],
							['type' => BTN_DANGER]  + $acts[ACTION_BREAKDRAW],
							['type' => BTN_DANGER]  + $acts[ACTION_DELDRAW],
						];
					}
					
					// Тираж запущен и как минимум один билет уже куплен
					if(($draw['status'] == DRAWSTATUS_RUN) and ($draw['nowbets'] > 0)) {
						$buttons = [
							['type' => BTN_INFO]    + $acts[ACTION_SHOWINFO],
							['type' => BTN_INFO]    + $acts[ACTION_SHOWINFO2],
							['type' => BTN_SUCCESS] + $acts[ACTION_INFOPOST],
							['type' => BTN_INFO]    + $acts[ACTION_SHOWPLAY],
							['type' => BTN_INFO]    + $acts[ACTION_SHOWPLAY2],
							['type' => BTN_WARNING] + $acts[ACTION_PLAYOUT],
//							['type' => BTN_DANGER]  + $acts[ACTION_BREAKDRAW],
						];
					}

					if($draw['status'] == DRAWSTATUS_DONE) {
						$buttons = [
							['type' => BTN_INFO]    + $acts[ACTION_SHOWPLAY],
							['type' => BTN_PRIMARY] + $acts[ACTION_PLAYPOST],
						];
					}
					
					// ($draw['status'] == DRAWSTATUS_BREAK) - Для прерванного тиража нет активных кнопок
					
					$draws[$key]['buttons'] = array_filter($buttons);
				
				}
				
				// Определяем наличие запущенных тиражей или тиражей в ожидании
				$is_active = in_array(DRAWSTATUS_RUN, $tmparr = array_column($draws, 'status')) || in_array(DRAWSTATUS_WAIT, $tmparr);

				// Страница управления
				$this->twig->display('control', [
					'csrf'		  => ['name' => $this->security->get_csrf_token_name(), 'hash' => $this->security->get_csrf_hash()],
					'is_demo'	  => $this->permits->is_demo,
					'chats'		  => $this->permits->chats,
					'draws'		  => $draws,
					'modes'		  => $drawmodes,
					'statuses'	  => $this->config->item('drawstatuses'),
					'chatparams'  => $this->config->item('chatparams'),
					'is_active'   => $is_active,
					'postview'	  => CLOSED_DRAW_POSTVIEW,
					'selected'	  => [
						'chat' => $this->permits->chatselected,
					],
				]);

			}
			else {
				$this->twig->display('nopermits', []);
			}
        
		}
		else {
			redirect('auth/login', 'refresh');
		}
	
	}

	/**
	 * Ajax контроллер обработки действий кнопок на странице Управления
	 */
	public function action() {
        
        if (MAINTENANCE or !$this->input->is_ajax_request() or !$this->is_loggedin or !$this->permits->is_granted) 
            die();

		$this->load->is_loaded('form_validation') OR $this->load->library('form_validation');
		
		// Настраиваем правила проверки формы
		$this->form_validation->set_rules([
			[
				'field' => 'drawcode',
				'rules' => "required|alpha_dash",
			], [
				'field' => 'action',
				'rules' => 'required|in_list[' .implode(',', array_keys($this->config->item('actions'))). ']'
			], 
		]);

		// Проверка отправленной формы
		if($this->form_validation->run()) {
			
			// Режим парсинга постов Телеграм
			$this->text->setParseMode($this->config->item('tg_parse_mode'));
			
			$formdata = $this->input->post();
			$drawcode = $formdata['drawcode'];
			
			$acts = $this->config->item('actions');
			
			$draw      = $this->draw->getDraw($drawcode);
			$mode      = $draw['mode'];
			$chat      = $draw['chat'];
			$chat2     = $draw['chat2'];
			$maxprizes = $draw['maxprizes'];

			/* ======================================================================
			 * Обрабатываем нажатия функциональных кнопок в форме
			 */
			switch ($formdata['action']) {
				
				/* ======================================================================
				 * Выводим превью поста запуска тиража
				 */
				case ACTION_SHOWRUN:
					
					$text = $this->text->renderPreview($draw, TEXTCODE_RUNMANUAL);
					
					$resp = is_string($text)
						? ($text !== ''
							? ['response' => true, 'body' => $text]
							: ['response' => false, 'body' => $acts[ACTION_SHOWRUN]['msgempty']]
						  )
						: ['response' => false, 'body' => $acts[ACTION_SHOWRUN]['msgerr']];
					
					break;
				
				/* ======================================================================
				 * Выводим превью поста запуска тиража
				 */
				case ACTION_SHOWRUN2:
					
					$text = $this->text->renderPreview($draw, TEXTCODE_RUNMANUAL2);
					
					$resp = is_string($text)
						? ($text !== ''
							? ['response' => true, 'body' => $text]
							: ['response' => false, 'body' => $acts[ACTION_SHOWRUN2]['msgempty']]
						  )
						: ['response' => false, 'body' => $acts[ACTION_SHOWRUN2]['msgerr']];
					
					break;
				
				/* ======================================================================
				 * Выводим превью поста розыгрыша/завершения тиража
				 */
				case ACTION_SHOWPLAY:
					
					$text = $this->text->renderPreview($draw, TEXTCODE_PLAYMANUAL);
					
					$resp = is_string($text)
						? ($text !== ''
							? ['response' => true, 'body' => $text]
							: ['response' => false, 'body' => $acts[ACTION_SHOWPLAY]['msgempty']]
						  )
						: ['response' => false, 'body' => $acts[ACTION_SHOWPLAY]['msgerr']];
					
					break;
				
				/* ======================================================================
				 * Выводим превью поста розыгрыша/завершения тиража
				 */
				case ACTION_SHOWPLAY2:
					
					$text = $this->text->renderPreview($draw, TEXTCODE_PLAYMANUAL2);
					
					$resp = is_string($text)
						? ($text !== ''
							? ['response' => true, 'body' => $text]
							: ['response' => false, 'body' => $acts[ACTION_SHOWPLAY2]['msgempty']]
						  )
						: ['response' => false, 'body' => $acts[ACTION_SHOWPLAY2]['msgerr']];
					
					break;
				
				/* ======================================================================
				 * Выводим превью инфо поста о состоянии тиража
				 */
				case ACTION_SHOWINFO:
					
					$text = $this->text->renderPreview($draw, TEXTCODE_INFO);

					$resp = is_string($text)
						? ($text !== ''
							? ['response' => true, 'body' => $text]
							: ['response' => false, 'body' => $acts[ACTION_SHOWINFO]['msgempty']]
						  )
						: ['response' => false, 'body' => $acts[ACTION_SHOWINFO]['msgerr']];
					
					break;
				
				/* ======================================================================
				 * Выводим превью инфо поста о состоянии тиража
				 */
				case ACTION_SHOWINFO2:
					
					$text = $this->text->renderPreview($draw, TEXTCODE_INFO2);

					$resp = is_string($text)
						? ($text !== ''
							? ['response' => true, 'body' => $text]
							: ['response' => false, 'body' => $acts[ACTION_SHOWINFO2]['msgempty']]
						  )
						: ['response' => false, 'body' => $acts[ACTION_SHOWINFO2]['msgerr']];
					
					break;
				
				/* ======================================================================
				 * Запускаем тираж и публикуем пост(ы)
				 */
				case ACTION_RUNDRAW:
					
					// Запускаем тираж
					$result = $this->draw->setRun($drawcode);
			
					// Публикуем пост в чате/канале тиража
					$runPostId = $this->tgposter->publish($chat, $this->text->render($draw, TEXTCODE_RUNMANUAL));
					
					if($runPostId > 0) {
						$this->draw->setPostIds($drawcode, [POSTID_RUN => $runPostId]);
					}
					
					$runPostId2 = $this->tgposter->publish($chat2, $this->text->render($draw, TEXTCODE_RUNMANUAL2));
					
					if($runPostId2 > 0){
						$this->draw->setPostIds($drawcode, [POSTID_RUN2 => $runPostId2]);
					}
					
					$resp = $result
						? ((($runPostId > 0) && ($runPostId2 > 0))
							? ['response' => true, 'body' => $acts[ACTION_RUNDRAW]['msgok']]
							: ['response' => false, 'body' => $acts[ACTION_RUNDRAW]['msgnopost']]
						  )
						: ['response' => false, 'body' => $acts[ACTION_RUNDRAW]['msgerr']];
					
					break;
				
				/* ======================================================================
				 * Публикуем пост запуска повторно (без самого запуска)
				 */
				case ACTION_RUNPOST:
			
					$runPostId = $this->tgposter->publish($chat, $this->text->render($draw, TEXTCODE_RUNMANUAL));
					
					if($runPostId > 0) {
						$this->draw->setPostIds($drawcode, [POSTID_RUN => $runPostId]);
					}
					
					$runPostId2 = $this->tgposter->publish($chat2, $this->text->render($draw, TEXTCODE_RUNMANUAL2));
					
					if($runPostId2 > 0) {
						$this->draw->setPostIds($drawcode, [POSTID_RUN2 => $runPostId2]);
					}
					
					$resp = (($runPostId > 0) && ($runPostId2 > 0))
						? ['response' => true, 'body' => $acts[ACTION_RUNPOST]['msgok']]
						: ['response' => false, 'body' => $acts[ACTION_RUNPOST]['msgerr']];
					
					break;
				
				/* ======================================================================
				 * Разыгрываем тираж - публикация внутри Checkout если тираж завершился
				 */ 
				case ACTION_PLAYOUT:
					
					$this->checkout->make(SCRIPTNAME, $drawcode);
					
					// Checkout проверяет сразу пачку транз, поэтому пока непонятно, что возвращать
					$resp = ['response' => true, 'body' => $acts[ACTION_PLAYOUT]['msgok']];
					
					break;
				
				/* ======================================================================
				 * Публикуем пост розыгрыша повторно (без самого розыгрыша)
				 */
				case ACTION_PLAYPOST:
			
					$endPostId = $this->tgposter->publish($chat, $this->text->render($draw, TEXTCODE_PLAYMANUAL));
				
					if($endPostId > 0) {
						$this->tgposter->delete($chat, $draw['infopostid']);
						$this->tgposter->delete($chat, $draw['endPostId']);
						
						$this->draw->setPostIds($drawcode, [POSTID_INFO => 0, POSTID_END => $endPostId]);
					}
					
					$endPostId2 = $this->tgposter->publish($chat2, $this->text->render($draw, TEXTCODE_PLAYMANUAL2));
				
					if($endPostId2 > 0) {
						$this->tgposter->delete($chat2, $draw['infopostid2']);
						$this->tgposter->delete($chat2, $draw['endPostId2']);
						
						$this->draw->setPostIds($drawcode, [POSTID_INFO2 => 0, POSTID_END2 => $endPostId2]);
					}
					
					$resp = (($endPostId > 0) && ($endPostId2 > 0))
						? ['response' => true, 'body' => $acts[ACTION_PLAYPOST]['msgok']]
						: ['response' => false, 'body' => $acts[ACTION_PLAYPOST]['msgerr']];
					
					break;
				
				/* ======================================================================
				 * Публикуем пост о состоянии тиража
				 */
				case ACTION_INFOPOST:
					
					$infoPostId = $this->tgposter->publish($chat, $this->text->render($draw, TEXTCODE_INFO));
					
					if($infoPostId > 0) {
						$this->tgposter->delete($chat, $draw['runpostid']);
						$this->tgposter->delete($chat, $draw['infopostid']);
						
						$this->draw->setPostIds($drawcode, [POSTID_RUN => 0, POSTID_INFO => $infoPostId]);
					}
					
					$infoPostId2 = $this->tgposter->publish($chat2, $this->text->render($draw, TEXTCODE_INFO2));
					
					if($infoPostId2 > 0) {
						$this->tgposter->delete($chat2, $draw['runpostid2']);
						$this->tgposter->delete($chat2, $draw['infopostid2']);
						
						$this->draw->setPostIds($drawcode, [POSTID_RUN2 => 0, POSTID_INFO2 => $infoPostId2]);
					}
					
					$resp = (($infoPostId > 0) && ($infoPostId2 > 0))
						? ['response' => true, 'body' => $acts[ACTION_INFOPOST]['msgok']]
						: ['response' => false, 'body' => $acts[ACTION_INFOPOST]['msgerr']];
					
					break;
				
				/* ======================================================================
				 * Прерываем тираж без розыгрыша
				 */
				case ACTION_BREAKDRAW:
					
					$resp = $this->draw->break($drawcode)
						? ['response' => true, 'body' => $acts[ACTION_BREAKDRAW]['msgok']]
						: ['response' => false, 'body' => $acts[ACTION_BREAKDRAW]['msgerr']];
					
					break;
				
				/* ======================================================================
				 * Удаляем тираж (доступно для незапущенных тиражей без купленных билетов)
				 */
				case ACTION_DELDRAW:
					
					$this->tgposter->delete($chat, $draw['runpostid']);
					
					$resp = $this->draw->delete($drawcode)
						? ['response' => true, 'body' => $acts[ACTION_DELDRAW]['msgok']]
						: ['response' => false, 'body' => $acts[ACTION_DELDRAW]['msgerr']];
					
					break;
			}

        }
        else {
            $resp = ['response' => false];
		}
        
        // Отправляем ответ в ajax-скрипт
        echo json_encode($resp);
	}
	
	
    /**
	 * Ajax-контроллер проверки валидности списка кодов тиражей в поле apresets
	 * Юзается ajax-механизм проверки поля от Bootstrap Validator
	 */
	public function check_drawcodes_valid() {
        
        if (MAINTENANCE or !$this->input->is_ajax_request() or !$this->is_loggedin or !$this->permits->is_granted) 
            die();
        
        $apresets = $this->input->get('apresets');
        
		// Список кодов тиражей валиден, т.е. каждый из них найден в БД
		if(isset($apresets) and $this->draw->isDrawcodesValid($apresets)) {
        
			echo "OK";
        }
        // Неверные входные данные или левый код тиража в списке (не найден в БД)
        else {
            header("HTTP/1.1 404 Not Found");
        }
        
    }

}
