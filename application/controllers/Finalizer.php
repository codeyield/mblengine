<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * Финализатор Millions BIP Lottery
 * 
 * php index.php finalizer
 * /opt/php71/bin/php /var/www/lotto/data/www/vps20103.xxvps.net/index.php finalizer
 */

const DRAWCODE = 'MBL1';

class Finalizer extends CI_Controller {
	
	public function index() {

		$this->load->model(['draw', 'failer', 'minter', 'trx']);
		$this->load->model(['text', 'playout', 'tgposter']);
		
		if(!is_cli()) {
			echo "This script may be running in console mode only\n";
			exit(1);
		}
		
		$this->draw->setPlayout(DRAWCODE);
		
		$d = $this->draw->getDraw(DRAWCODE);

//		$d['chat'] = 'crypt0winners';
//		$d['chat2'] = 'crypt0winners';
		
		
		// Формируем текст по соотв. шаблону (последующий ручной розыгрыш) и публикуем пост #1
		$endPostId = $this->tgposter->publish($d['chat'], $this->text->render($d, TEXTCODE_PLAYMANUAL));

		if($endPostId > 0) {

			// Удаляем последний инфопост тиража
			$this->tgposter->delete($d['chat'], $d['infopostid']);
			$this->draw->setPostIds($d['drawcode'], [POSTID_INFO => 0, POSTID_END => $endPostId]);
		}

		// Формируем текст по соотв. шаблону (последующий ручной розыгрыш) и публикуем пост #2
		if(!empty($d['chat2'])) {

			$endPostId2 = $this->tgposter->publish($d['chat2'], $this->text->render($d, TEXTCODE_PLAYMANUAL2));

			if($endPostId2 > 0) {

				// Удаляем последний инфопост тиража
				$this->tgposter->delete($d['chat2'], $d['infopostid2']);
				$this->draw->setPostIds($d['drawcode'], [POSTID_INFO2 => 0, POSTID_END2 => $endPostId2]);
			}
		}
		
	}

}