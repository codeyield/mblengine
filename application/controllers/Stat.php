<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Stat extends CI_Controller {

// Залогинен ли юзер
private $is_loggedin = false;


    public function __construct() {
        parent::__construct();
		
		$this->load->library(['ion_auth', 'form_validation']);
		$this->load->helper(['url', 'language']);
		
		if ( !($this->is_loggedin = $this->ion_auth->logged_in()) ) {
			redirect('auth/login', 'refresh');
		}
		
		/* 
		 * Доступы юзера:
		 * @param bool   $this->permits->is_granted   Права юзера подтверждены
		 * @param bool   $this->permits->is_demo      Демо-режим (кнопки неактивны)
		 * @param array  $this->permits->chats        Список доступных юзеру телеграм чатов/каналов
		 * @param array  $this->permits->tplcodes     Список имен шаблонов для постов для выбранного чата/канала
		 * @param string $this->permits->chatselected Текущий выбранный юзером чат/канал
		 * @param string $this->permits->tplselected  Текущее выбранное юзером имя шаблона для постов
		 * @param string $this->permits->useremail    Авторизационный email юзера
		 */
		$this->load->model('permits');
		$this->permits->load();
		
		$this->load->model(['draw', 'failer', 'minter', 'trx']);
	}
	
	/**
	 * Отображение страницы статистики тиражей
	 */
	public function index() {
        
		if (MAINTENANCE) {
			$this->load->view('maintenance.html');
		}
		elseif ($this->is_loggedin) {

			// Загружаем шаблонизатор
			$this->load->library('twig', $this->config->item('twigconfig'));
			$this->twig->addGlobal('sitetitle', $this->config->item('sitetitle'));
			$this->twig->addGlobal('environment', ENVIRONMENT);
			$this->twig->addGlobal('base_url', base_url());
			$this->twig->addGlobal('uri_string', uri_string());

			if ($this->permits->is_granted) {

				$drawmodes    = $this->config->item('drawmodes');
				$drawstatuses = $this->config->item('drawstatuses');

				// Перекомпоновка массива с типами розыгрышей в вид ['type' => 'description', ... ]
				$drawmodes = array_combine(array_column($drawmodes, 'type'), array_column($drawmodes, 'short'));
				
				// Основной массив с данными
				$draws = $this->draw->getStatisticalDraws($this->permits->chatselected);
				
				
				// Собираем и рассчитываем всю статистику по лотерее
				foreach($draws as $key => $draw) {
					
					$data = $this->trx->getDrawStat($draw['drawcode']);
					
					
					// Считаем стату по продажам билетов
					$bets = array_column($data, 'bets');
					
					$tickets = array_filter(
						array_count_values($bets), 
						function($v, $k) {
							// Единичные покупки на случайные суммы не учитываем, кроме билетов ценой больше чем
							return ($k >= STAT_MIN_POWER_PAY) || ($v != 1); 
						},
						ARRAY_FILTER_USE_BOTH
					);
					
					// ksort($tickets);		// BIP по возрастанию
					arsort($tickets);		// Кол-во билетов по убыванию
					
					
					// Собираем стату по кошелькам
					$buyers = [];
					foreach($data as $item) {
						$buyers[$k]['wallet'] = $k = $item['wallet'];
						$buyers[$k]['bets'] = isset($buyers[$k]['bets']) ? $buyers[$k]['bets'] + $item['bets'] : $item['bets'];
						$buyers[$k]['qty']  = isset($buyers[$k]['qty'])  ? $buyers[$k]['qty']  + 1 : 1;
					}

					$buyers = array_filter(
						array_values($buyers),
						function($item) {
							return ($item['qty'] > STAT_MIN_WALLET_TRXS) || ($item['bets'] >= STAT_MIN_POWER_PAY);
						}
					);

					// Кошельки по убыванию кол-ва купленных билетов
					usort($buyers, function($a, $b) { return $a['bets'] < $b['bets'] ? 1 : -1; });

					$buyers = array_slice($buyers, 0, STAT_MAX_BUYERS_VIEW);
					
					
					// Считаем выручку по дням
					$revenue = [];
					foreach($data as $item) {
						$k = substr($item['blocktime'], 0, 10);
						$revenue[$k] = isset($revenue[$k]) ? $revenue[$k] + $item['bets'] : $item['bets'];
					}
					
					
					// Добавляем данные в тираж
					$draws[$key]['totaltrxs']  = count($data);
					$draws[$key]['average']    = array_sum($bets) / count($bets);
					$draws[$key]['uniqbuyers'] = count(array_unique(array_column($data, 'wallet')));
					
					$draws[$key]['stat']    = array_slice($data, 0, STAT_MAX_TRXS_VIEW);
					$draws[$key]['tickets'] = $tickets;
					$draws[$key]['buyers']  = $buyers;
					$draws[$key]['revenue'] = $revenue;
				}
				
				
				// Страница статистики тиражей
				$this->twig->display('stat', [
					'csrf'		=> ['name' => $this->security->get_csrf_token_name(), 'hash' => $this->security->get_csrf_hash()],
					'is_demo'	=> $this->permits->is_demo,
					'chats'		=> $this->permits->chats,
					'draws'		=> $draws,
					'modes'		=> $drawmodes,
					'statuses'	=> $drawstatuses,
					'maxtrxs'	=> STAT_MAX_TRXS_VIEW,
					'selected'	=> [
						'chat'	=> $this->permits->chatselected,
					],
				]);
				
			}
			else {
				$this->twig->display('nopermits', []);
			}
        
		}
		else {
			redirect('auth/login', 'refresh');
		}
	
	}
	
	/**
	 * Отдаёт на загрузку текстовый файл со списком кошельков победителей указанного тиража
	 * @param string $drawcode Код тиража
	 */
	public function winners($drawcode) {
		
		$draw = $this->draw->getDraw($drawcode);
		
		if(isset($draw['winners']) and is_array($draw['winners'])) {
			
			$this->load->helper('download');
			force_download("winners-{$drawcode}.txt", implode("\r\n", $draw['winners']));
		}
		
	}
	
	/**
	 * Отдаёт на загрузку текстовый файл со списком кошельков кэшбеков указанного тиража
	 * @param string $drawcode Код тиража
	 */
	public function cashbacks($drawcode) {
		
		$cashbackWallets = $this->draw->getCashbackWallets($drawcode);
		
		if(is_array($cashbackWallets)) {
			
			$this->load->helper('download');
			force_download("cashbacks-{$drawcode}.txt", implode("\r\n", $cashbackWallets));
		}
	}

}