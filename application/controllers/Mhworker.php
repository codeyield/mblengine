<?php
declare(ticks = 1);
defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * Консольный воркер для действий с запущенными тиражами в реальном времени
 * 
 * php index.php mhworker
 * php /var/www/lotto/data/www/vps20103.xxvps.net/index.php mhworker
 */

const SCRIPTNAME = 'Mhworker';
const LOOPDELAY  = 6;					// Интервал проверки транзакций, секунд (int)


class Mhworker extends CI_Controller {
	
	/**
	 * Консольный воркер, обрабатывающий новые входящие транзакции тиражей
	 */
	public function index() {
		
		set_time_limit(0);
		ini_set('memory_limit', '512M');
		
		if(!is_cli()) {
			echo "This script may be running in console mode only\n";
			exit(1);
		}
		
		// Обработчик завершения скрипта для консольного режима
		function sig_handler($signo) {
			
			switch ($signo) {
				case SIGINT:	$msg = 'Terminated by user';			break;
				case SIGTERM:	$msg = 'Process killed';				break;
				case SIGHUP:	$msg = 'Restart signal was received';	break;
			}
			
			log_message('error', SCRIPTNAME . ': ' . $msg);
			@mail(EMAIL_ALERT, SCRIPTNAME . ' ALERT', $msg);
			// $this->db->close();
			
			exit(1);
		}

		// Устанавливаем обработчики сигналов, если скрипт запущен под NIX*
		if(strtoupper(substr(PHP_OS, 0, 3)) !== 'WIN') {
			pcntl_signal(SIGINT,  'sig_handler');	// Ctrl+C
			pcntl_signal(SIGTERM, 'sig_handler');	// killall myscript / kill <PID>
			pcntl_signal(SIGHUP,  'sig_handler');	// обрыв связи
		}
		
		$this->load->model(['draw', 'failer', 'minter', 'trx']);
		$this->load->model(['text', 'playout', 'tgposter', 'chat', 'checkout']);
		$this->load->model(['cashback', 'cbdata']);
		
		echo SCRIPTNAME . ' is running...';
		
		// Счетчик запросов к БД
		$queryCounter = 0;
		
		
		// Вечный цикл проверки
		while(true) {
		
			// Периодически переподключает соединение с БД для избежания исчерпания памяти движком БД CI
			if(++$queryCounter >= QUERIES_TO_RECONNECT) {
				$this->db->reconnect();
				$queryCounter = 0;
			}
			
			$this->checkout->make(SCRIPTNAME);
			
			sleep(LOOPDELAY);
		}
	}
	
}