<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require(APPPATH . 'libraries/RestController.php');
require(APPPATH . 'libraries/Format.php');

use chriskacerguis\RestServer\RestController;

const DRAWCODE = 'MBL1';


class Api extends RestController {

	function __construct() {
		parent::__construct();
	}

	/**
	 * URL вызова:
	 * https://domain/api/yesterday_buyers/
	 */
	public function yesterday_buyers_get() {
		
		$this->load->model('trx');
		
		$data = $this->trx->getYesterdayBuyers(DRAWCODE);
		
		if(!empty($data)) {
			
			$this->response($data, 200);
		}
		else {
			$this->response([
				'response' => false,
				'error'    => true,
				'message'  => 'Can`t get data on yesterday buyers'
			], 404);
		}
	
	}

	/**
	 * URL вызова:
	 * https://domain/api/tickets_sold/
	 */
	public function tickets_sold_get() {
		
		$this->load->model('trx');
		
		$tickets_sold = $this->trx->countBetsForDraw(DRAWCODE);
		
		if(!empty($tickets_sold)) {
			
			$this->response([
				'response' => $tickets_sold,
			], 200);
		}
		else {
			$this->response([
				'response' => false,
				'error'    => true,
				'message'  => 'Can`t get data on tickets sold'
			], 404);
		}
	
	}

	/**
	 * URL вызова:
	 * http://domain/api/user_bought/wallet/Mx2329a1c20ef096695a25a1d39b969bdeb2bd4847
	 */
	public function user_bought_get() {
		
		$this->load->model('trx');
		
		$wallet = $this->get('wallet');
		
		if(preg_match("/Mx[0-9a-f]{40}/i", $wallet)) {
			
			$user_bought = $this->trx->countBetsForWallet(DRAWCODE, $wallet);
			
			if(!empty($user_bought)) {
				
				$this->response([
					'response' => $user_bought,
				], 200);
			}
			else {
				$this->response([
					'response' => false,
					'error'    => true,
					'message'  => 'Can`t get data on user bought'
				], 404);
			}
			
		}
		else {
			$this->response([
				'response' => false,
				'error'    => true,
				'message'  => 'Invalid format of the wallet'
			], 404);
		}
	}

/*
	// http://domain/api/users/ will return the list of all users
    // http://domain/api/users/id/1 will only return information about the user with id = 1

	public function users_get() {
		
		// Users from a data store e.g. database
		$users = [
			['id' => 0, 'name' => 'John', 'email' => 'john@example.com'],
			['id' => 1, 'name' => 'Jim', 'email' => 'jim@example.com'],
		];

		$id = $this->get( 'id' );

		if ( $id === null ){
			// Check if the users data store contains users
			if ( $users ) {
				// Set the response and exit
				$this->response( $users, 200 );
			}
			else {
				// Set the response and exit
				$this->response( [
					'status' => false,
					'message' => 'No users were found'
				], 404 );
			}
		}
		else {
			if ( array_key_exists( $id, $users ) ) {
				$this->response( $users[$id], 200 );
			}
			else {
				$this->response( [
					'status' => false,
					'message' => 'No such user found'
				], 404 );
			}
		}
	}
*/

}