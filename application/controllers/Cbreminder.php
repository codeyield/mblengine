<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * Консольный крон скрипт для кидания напоминаний о кэшбеке в чат, 
 *  если в течение CASHBACK_REMIND_AFTER минут не было постов кэшбека
 * Крон можно задавать с интервалом 5-10 мин с выключением на ночь
 * 
 * php index.php cbreminder
 * php /var/www/apps/data/www/lotto.thinkapps.ru/index.php cbreminder
 */

const CASHBACK_APPROX_PAYLOAD_FEE = 0.1;
const CASHBACK_MAX_TXS_VIEW   = 10;			// Показывать не более последних транз
const CASHBACK_REMIND_AFTER   = 38;			// Публиковать пост с напоминанием через, мин
const CASHBACK_TPLID_RUN      = 'run_tpl';	// Шаблон стартового поста

const CASHBACK_LOTTERY_WALLET = 'Mxd1267678bc5a37e7ab5ff6375ba5ebd391de2f36';
const CASHBACK_REMINDER_CHAT  = '@millionsbiplottery';


class Cbreminder extends CI_Controller {
	
	public function index() {
		
		if(!is_cli()) {
			echo "This script may be running in console mode only\n";
			exit(1);
		}
		
		$this->load->model(['failer', 'minter', 'tgposter', 'cbdata']);

		// Ф-ция сборки строки со списком выплатных транз для подстановки в шаблон
		$getTxsList = function($txs) {
			$k = 1; $text = '';
			foreach(array_filter($txs) as $tx) {
				$text .= '[' .$k++. ' ' .EMOJI_USDFLY. '](' .MSCAN_TXPREFIX.$tx. ') ';
			}
			return $text;
		};
		
		
		/**********************************************************
		 * Проходим по списку розыгрышей кэшбеков в БД
		 **********************************************************/
		foreach($this->cbdata->getRunnedRaffles() as $r) {
			
			// Еще не прошел мин. лимит времени для напоминания
			if((time() - strtotime($r['tstamp'])) < (CASHBACK_REMIND_AFTER * 60)) {
				continue;
			}
				
			// Проверяем, что достаточно баланса на выплатном кошельке
			$balance = $this->minter->getBalance($r['wallet'], $r['coin']);

			$maybePays = (int) ($balance / ($r['pay'] + CASHBACK_APPROX_PAYLOAD_FEE));

			// Баланса недостаточно для выплат
			if($maybePays < 1) {
				continue;
			}

			// Вероятностный шанс выигрыша в виде N:1
			$chanceCount = (int) (100 / $r['chance']);
			
			// Укорачиваем список транз до лимита, выкидывая самые старые транзы в НАЧАЛЕ массива
			$r['txs'] = count($r['txs']) > CASHBACK_MAX_TXS_VIEW 
				? array_slice($r['txs'], -CASHBACK_MAX_TXS_VIEW, 999)
				: $r['txs'];
			
			// Собираем подстановочный шаблон для постов
			$replacements = [
				'{{coin}}'			=> $r['coin'],
				'{{amount}}'		=> $r['amount'],
				'{{pay}}'			=> $r['pay'],
				'{{chance}}'		=> $r['chance'],
				'{{chancecount}}'	=> $chanceCount,
				'{{tries}}'			=> $r['tries'],
				'{{wins}}'			=> (!empty($r['wins']) ? $r['wins'] : 0),
				'{{maybepays}}'		=> $maybePays,
				'{{txslist}}'		=> (!empty($getTxsList($r['txs'])) ? $getTxsList($r['txs']) : '_еще не было_'),
				'{{realchance}}'	=> sprintf("%02.1f", $r['wins'] / $r['tries'] * 100),
				'{{deeplink}}'		=> $this->minter->deepLink(CASHBACK_LOTTERY_WALLET, $r['amount'], $r['coin']),
			];
			
			$text = trim(strtr(
				$this->cbdata->getTemplate($r['tplcode'], CASHBACK_TPLID_RUN), 
				$replacements
			));
			
			$postId = $this->tgposter->publish(CASHBACK_REMINDER_CHAT, $text);
			
			if($postId > 0) {
				
				$this->tgposter->delete(CASHBACK_REMINDER_CHAT, $r['postid']);
				$this->cbdata->updPostId($r['name'], $postId);
			}
			else {
				log_message('error', "{$scriptname}: Не удалось опубликовать инфопост кэшбека {$r['name']}");
			}
			
		}

	}
	
}