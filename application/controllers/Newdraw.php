<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Newdraw extends CI_Controller {

// Залогинен ли юзер
private $is_loggedin = false;


    public function __construct() {
        parent::__construct();
		
		$this->load->library(['ion_auth', 'form_validation']);
		$this->load->helper(['url', 'language']);
		
		if ( !($this->is_loggedin = $this->ion_auth->logged_in()) ) {
			redirect('auth/login', 'refresh');
		}
		
		/* 
		 * Доступы юзера:
		 * @param bool   $this->permits->is_granted   Права юзера подтверждены
		 * @param bool   $this->permits->is_demo      Демо-режим (кнопки неактивны)
		 * @param array  $this->permits->chats        Список доступных юзеру телеграм чатов/каналов
		 * @param array  $this->permits->tplcodes     Список имен шаблонов для постов для выбранного чата/канала
		 * @param string $this->permits->chatselected Текущий выбранный юзером чат/канал
		 * @param string $this->permits->tplselected  Текущее выбранное юзером имя шаблона для постов
		 * @param string $this->permits->useremail    Авторизационный email юзера
		 */
		$this->load->model('permits');
		$this->permits->load();
		
		$this->load->model(['draw', 'failer', 'minter', 'trx']);
	}

	/**
	 * Отображение формы для запуска нового тиража
	 */
	public function index() {
        
		if (MAINTENANCE) {
			$this->load->view('maintenance.html');
		}
		elseif ($this->is_loggedin) {

			// Загружаем шаблонизатор
			$this->load->library('twig', $this->config->item('twigconfig'));
			$this->twig->addGlobal('sitetitle', $this->config->item('sitetitle'));
			$this->twig->addGlobal('environment', ENVIRONMENT);
			$this->twig->addGlobal('base_url', base_url());
			$this->twig->addGlobal('uri_string', uri_string());

			if ($this->permits->is_granted) {

				$this->twig->display('newdraw', [
					'csrf'		=> ['name' => $this->security->get_csrf_token_name(), 'hash' => $this->security->get_csrf_hash()],
					'is_demo'	=> $this->permits->is_demo,
					'draw'		=> $this->config->item('drawparams'),
					'modes'		=> $this->config->item('drawmodes'),
					'chats'		=> $this->permits->chats,
					'tplcodes'	=> $this->permits->tplcodes,
					'lastparams'	=> $this->draw->getLastParams($this->permits->chatselected),
					'nextdrawcode'	=> $this->draw->nextDrawcode($this->permits->chatselected),
					'selected'	=> [
						'chat'		=> $this->permits->chatselected,
						'tplcode'	=> $this->permits->tplselected,
					],
				]);

			}
			else {
				$this->twig->display('nopermits', []);
			}
        
		}
		else {
			redirect('auth/login', 'refresh');
		}
	
	}

	/**
	 * Ajax контроллер обработки формы и добавления нового тиража
	 */
	public function create() {
        
        if (MAINTENANCE or !$this->input->is_ajax_request() or !$this->is_loggedin or !$this->permits->is_granted) 
            die();

		$this->load->is_loaded('form_validation') OR $this->load->library('form_validation');
		$f = $this->config->item('drawparams');

		// Настраиваем правила проверки формы
		$this->form_validation->set_rules([
			[
				'field' => 'drawcode',
				'rules' => "required|trim|min_length[{$f['code_minlen']}]|max_length[{$f['code_maxlen']}]|alpha_dash|is_unique[".DBTBL_DRAWS.".drawcode]",
			], [
				'field' => 'mode',
				'rules' => 'required|trim|in_list[' .implode(',', array_column($this->config->item('drawmodes'), 'type')). ']',
			], [
				'field' => 'wallet',
				'rules' => 'required|trim|regex_match[/^Mx[A-f0-9]{40}/]',
			], [
				'field' => 'coin',
				'rules' => 'required|trim|regex_match[/^[A-Z0-9]{3,10}/]',
			], [
				'field' => 'tplcode',
				'rules' => 'required|trim|in_list[' .implode(',', $this->permits->tplcodes). ']',
			], [
				'field' => 'bet',
				'rules' => "required|integer",
			], [
				'field' => 'maxbets',
				'rules' => "required|integer",
			], [
				'field' => 'prize',
				'rules' => "required|integer",
			], [
				'field' => 'maxprizes',
				'rules' => "required|integer",
			], [
				'field' => 'cashback',
				'rules' => "integer",
			], [
				'field' => 'title',
				'rules' => "trim|min_length[{$f['title_minlen']}]|max_length[{$f['title_maxlen']}]",
			],
		]);

		// Проверка отправленной формы
		if($this->form_validation->run()) {

			$formdata     = $this->input->post();

			// Создаем тираж
			$result = $this->draw->create($this->permits->chatselected, $formdata);
			
            $resp = $result ? ['response' => true] : ['response' => false];
        }
        else {
            $resp = ['response' => false];
		}
        
        // Отправляем ответ в ajax-скрипт
        echo json_encode($resp);
    
	}

    /**
	 * Ajax-контроллер проверки занятости кода тиража
	 * Юзается ajax-механизм проверки поля от Bootstrap Validator
	 */
	public function check() {
        
        if (MAINTENANCE or !$this->input->is_ajax_request() or !$this->is_loggedin or !$this->permits->is_granted) 
            die();
        
        $drawcode = $this->input->get('drawcode');
        
		// Код тиража свободен, т.е. не найден в БД
		if(isset($drawcode) and !$this->draw->isDrawcodeExists($drawcode)) {
        
			echo "OK";
        }
        // Неверные входные данные или код тиража занят (найден в БД)
        else {
            header("HTTP/1.1 404 Not Found");
        }
        
    }

}