<?php
defined('BASEPATH') OR exit('No direct script access allowed');

const DB_FILE = '/var/www/lotto/data/www/millionbip/index/db/dailylottery.db';

define('YESTERDAY', date('Y-m-d', strtotime('-1 day')));
define('WHERECOND', " WHERE day = '" .YESTERDAY. "'");


class Todaywinners extends CI_Controller {

	/**
	 * Возвращает данные сегодняшего розышрыша ревардов из базы скрипта dailylottery.php
	 */
	public function index() {

		try {
			$dbl = new SQLite3(DB_FILE);

			$result = $dbl->query("SELECT * FROM draws" . WHERECOND);
			$data = $result->fetchArray(SQLITE3_ASSOC);	// Return false if no data
		}
		catch(Exception $e) {
			log_message('error', 'Todaywinners: Can`t get data from DB');
		}
		
		header('Content-Type: application/json');
		echo json_encode($data);
	}
	
}
