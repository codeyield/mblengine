<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * GET загрузчик транзакций запущенных тиражей
 */

const SCRIPTNAME = 'Pull2';

class Pull2 extends CI_Controller {
	
    public function __construct() {
        parent::__construct();
		
		$this->load->model(['draw', 'failer', 'minter', 'trx']);
		$this->load->model(['text', 'playout', 'tgposter', 'chat', 'checkout', 'mhdata']);
		$this->load->library('curl');
	}
	
	/* 
	 * Загружает и парсит транзы блокчейна Минтер по кошелькам запущенных тиражей
	 */
	public function index() {
		
		define('ECHO_ON', !$this->input->is_ajax_request());
		
		// Таймаут побольше, т.к. за ≈7 сек. ноды не успевают отвечать
		$this->curl->timeout = PARSER_CURLTIMEOUT;
		
		// Получаем список API URL всех имеющихся нод и создаём массив со счетчиками ошибок нод
		$nodes = $this->config->item('minter_nodes');
		$nodeFails = array_combine(array_keys($nodes), array_fill(0, count($nodes), 0));
		
		// Получаем список кошельков запущенных тиражей
		$wallets = $this->mhdata->getCheckoutWallets();
		
		// Нет запущенных тиражей
		if(ECHO_ON and (count($wallets) == 0)) {
			echo "No runned draws now<br>";
			exit;
		}

		
		$trxs = [];
		$totalFails = 0;

		/* ===========================================================================
		 * Обходим кошельки и вытаскиваем данные из блокчейна по каждому
		 */
		foreach($wallets as $wallet) {

			// Получаем № крайней спарсенной страницы блокчейна по кошельку
			$page = $this->_getBlockchainPage($wallet);

			// Парсим нужное число страниц страниц транзакций по кошельку
			do {
				// Выбираем URL случайной ноды
				$nodeId = array_rand($nodes);
				
				/* ===========================================================================
				 * Получаем данные из блокчейна по API. Пример запроса:
				 * https://api.minter.one/transactions?query=%22tags.tx.to%3D%27ff88a0a2c080f7318b870512c3f0d382ac512fa4%27+AND+tags.tx.type%3D%2701%27%22&page=1
				 */
				$query   = urlencode("\"tags.tx.to='". substr($wallet, 2). "' AND tags.tx.type='01'\"") . "&page={$page}";
				
				$apiurl  = $nodes[$nodeId] . PARSER_QUERY_PREFIX . $query;
				$rawdata = $this->curl->get($apiurl);
				
				if($rawdata == false) {
					log_message('error', SCRIPTNAME . ": Page downloading fail by URL:\r\n" . $apiurl);
					log_message('error', SCRIPTNAME . ": CURL error #" . $this->curl->error['code']);
					log_message('error', SCRIPTNAME . ": CURL message: " . $this->curl->error['msg']);
					
					$nodeFails[$nodeId]++;
					$totalFails++;
					
					// Выкидываем ноду из списка, если по ней достигнуто макс. число неудачных попыток получения данных
					if($nodeFails[$nodeId] >= PARSER_MAXFAILS_4NODE) {
						log_message('error', SCRIPTNAME . ": {$nodeFails[$nodeId]} failed tries for node '{$nodeId}'");
						unset($nodes[$nodeId]);		// Считаем ссылку на эту ноду временно нерабочей и выкидываем
					}
					
					continue;				// Перезапускаем цикл заново на той же странице
				}
				
				$trxsPage = json_decode($rawdata);
				
				// Для безопасной проверки в while, если json вернёт ошибку
				$countTrxs = 999;
				
				// Невалидный JSON
				if(empty($trxsPage)) {
					log_message('error', SCRIPTNAME . ": Json decoding fail by URL:\r\n" . $apiurl);
					$totalFails++;
					continue;
				}
				// Вернулась ошибка
				elseif(isset($trxsPage->error)) {
					log_message('error', SCRIPTNAME . ": Json error:\r\n" . print_r($trxsPage, 1));
					$totalFails++;
					continue;
				}
				// Распарсили, все ОК
				else {
					// Сохраняем № текущей спарсенной страницы блокчейна по кошельку
					// Еще неизвестно, станет ли она крайней, т.к. это зависит от проверки в конце while()
					$this->_setBlockchainPage($wallet, $page);
					
					// Транзакций на странице
					$countTrxs = count($trxsPage->result);
					
					$trxs = array_merge($trxs, (array) $trxsPage->result);

					// Выводим статистику парсинга в браузер
					if(ECHO_ON) {
						echo "Parsing wallet: {$wallet}<br>";
						echo "Page number: {$page}<br>";
						echo "Transactions got: " . count($trxsPage->result) ."<br><br>";
					}

					$page++;
				}

			} while(    (count($nodes) > 0) 					// Ссылки на ноды еще есть в списке (не выкинуты как нерабочие)
					and ($countTrxs >= 100) 					// Еще НЕ последняя страница c данными по кошельку
					and ($totalFails < PARSER_MAXFAILS_TOTAL)	// Максимум ошибок еще не достигнут
				   );
		
		} // end foreach($wallets as $wallet)

		
		/* ===========================================================================
		 * Добавлем транзакции в БД
		 */
		$addedTrxs = 0;
		
		foreach($trxs as $trx) {

			// Перечень действий для обнаруженных подходящих транзакций
			switch ($trx->type) {

				case TXTYPE_SEND:

					if($this->mhdata->isWalletFound($trx->data->to, $wallets)) {
						$addedTrxs += $this->mhdata->addTrxTypeSend($trx);
					}
					break;

				case TXTYPE_DELEGATE:

					if($this->mhdata->isWalletFound($trx->data->pub_key, $wallets)) {
						// $addedTrxs += $this->mhdata->addTrxTypeDelegate($trx);
					}
					break;
			}
		}

		/* ===========================================================================
		 * Запускаем обработчик транзакций
		 */
		$this->checkout->make(SCRIPTNAME);
		
		
		/* ===========================================================================
		 * Выводим статистику в браузер
		 */
		echo ECHO_ON ? "Transactions added: {$addedTrxs}<br>" : '';

		// Отчет о неудачных запросах к нодам
		if(ECHO_ON and ($totalFails > 0)) {
			echo "-----------------------------------------<br>";
			foreach($nodeFails as $nodeId => $tries) {
				echo $tries > 0 ? "WARNING! Node '{$nodeId}' failed attempts of: {$tries}<br>" : '';
			}
			echo "Total failed attempts of: {$totalFails}<br>";
		}

    }
	
	
	/**
	 * Возвращает № крайней спарсенной страницы блокчейна по кошельку
	 * @private
	 * @param  string $wallet Кошелек тиража
	 * @return integer № страницы для парсинга
	 */
	private function _getBlockchainPage($wallet) {
		
		$result = $this->db
			->select('page')
			->get_where(DBTBL_BCPAGES, ['wallet' => $wallet]);
        
		$row = (array) $result->row();
		
		if(($this->db->affected_rows() > 0) and isset($row['page'])) {
			
			return $row['page'];
		}
		
		// Запись для кошелька еще не создана, сохраняем № страницы как первый
		$this->_setBlockchainPage($wallet, 1);
		
		return 1;
	}

	/**
	 * Сохраняет № крайней спарсенной страницы блокчейна по кошельку
	 * @private
	 * @param  string  $wallet Кошелек тиража
	 * @param  integer $page   № текущей спарсенной страницы
	 * @return boolean Успех операции
	 */
	private function _setBlockchainPage($wallet, $page) {

		$query = $this->db
			->insert_string(DBTBL_BCPAGES, ['wallet' => $wallet, 'page' => $page]) 
			. " ON DUPLICATE KEY UPDATE `page` = {$page}";
		
		return $this->db->query($query);
	}
	
}