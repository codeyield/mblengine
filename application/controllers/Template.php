<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Template extends CI_Controller {

// Залогинен ли юзер
private $is_loggedin = false;


    public function __construct() {
        parent::__construct();
		
		$this->load->library(['ion_auth', 'form_validation']);
		$this->load->helper(['url', 'language']);
		
		if ( !($this->is_loggedin = $this->ion_auth->logged_in()) ) {
			redirect('auth/login', 'refresh');
		}
		
		/* 
		 * Доступы юзера:
		 * @param bool   $this->permits->is_granted   Права юзера подтверждены
		 * @param bool   $this->permits->is_demo      Демо-режим (кнопки неактивны)
		 * @param array  $this->permits->chats        Список доступных юзеру телеграм чатов/каналов
		 * @param array  $this->permits->tplcodes     Список имен шаблонов для постов для выбранного чата/канала
		 * @param string $this->permits->chatselected Текущий выбранный юзером чат/канал
		 * @param string $this->permits->tplselected  Текущее выбранное юзером имя шаблона для постов
		 * @param string $this->permits->useremail    Авторизационный email юзера
		 */
		$this->load->model('permits');
		$this->permits->load();
		
		$this->load->model(['draw', 'failer', 'minter', 'trx', 'text']);
	}
	
	/**
	 * Отображение формы с шаблонами постов
	 */
	public function index() {
        
		if (MAINTENANCE) {
			$this->load->view('maintenance.html');
		}
		elseif ($this->is_loggedin) {

			// Загружаем шаблонизатор
			$this->load->library('twig', $this->config->item('twigconfig'));
			$this->twig->addGlobal('sitetitle', $this->config->item('sitetitle'));
			$this->twig->addGlobal('environment', ENVIRONMENT);
			$this->twig->addGlobal('base_url', base_url());
			$this->twig->addGlobal('uri_string', uri_string());

			if ($this->permits->is_granted) {
				
				// Страница формы с шаблонами постов
				$this->twig->display('template', $tmp = [
					'csrf'		=> ['name' => $this->security->get_csrf_token_name(), 'hash' => $this->security->get_csrf_hash()],
					'is_demo'	=> $this->permits->is_demo,
					'text'		=> $this->config->item('textparams'),
					'data'		=> $this->text->getAll($this->permits->tplselected),
					'chats'		=> $this->permits->chats,
					'tplcodes'	=> $this->permits->tplcodes,
					'selected'	=> [
						'chat'		=> $this->permits->chatselected,
						'tplcode'	=> $this->permits->tplselected,
					],
				]);

			}
			else {
				$this->twig->display('nopermits', []);
			}
        
		}
		else {
			redirect('auth/login', 'refresh');
		}
	
	}

	/**
	 * Ajax контроллер обновления текстовых шаблонов
	 */
	public function update() {
        
        if (MAINTENANCE or !$this->input->is_ajax_request() or !$this->is_loggedin or !$this->permits->is_granted) 
            die();

		$this->load->is_loaded('form_validation') OR $this->load->library('form_validation');
		$f = $this->config->item('textparams');
		
		// Настраиваем правила проверки формы
		$this->form_validation->set_rules([
			['field' => 'runmnl',			'rules' => "trim|min_length[{$f['minlen']}]|max_length[{$f['maxlen']}]"], 
			['field' => 'runmnl_isimg',		'rules' => "integer"], 
			['field' => 'runmnl_imgurl',	'rules' => "valid_url|min_length[{$f['url_minlen']}]|max_length[{$f['url_maxlen']}]"], 
			
			['field' => 'runmnl2',			'rules' => "trim|min_length[{$f['minlen']}]|max_length[{$f['maxlen']}]"], 
			['field' => 'runmnl2_isimg',	'rules' => "integer"], 
			['field' => 'runmnl2_imgurl',	'rules' => "valid_url|min_length[{$f['url_minlen']}]|max_length[{$f['url_maxlen']}]"], 
			
			['field' => 'playmnl',			'rules' => "trim|min_length[{$f['minlen']}]|max_length[{$f['maxlen']}]"], 
			['field' => 'playmnl_isimg',	'rules' => "integer"], 
			['field' => 'playmnl_imgurl',	'rules' => "valid_url|min_length[{$f['url_minlen']}]|max_length[{$f['url_maxlen']}]"], 
			
			['field' => 'playmnl2',			'rules' => "trim|min_length[{$f['minlen']}]|max_length[{$f['maxlen']}]"], 
			['field' => 'playmnl2_isimg',	'rules' => "integer"], 
			['field' => 'playmnl2_imgurl',	'rules' => "valid_url|min_length[{$f['url_minlen']}]|max_length[{$f['url_maxlen']}]"], 
			
			['field' => 'info',				'rules' => "trim|min_length[{$f['minlen']}]|max_length[{$f['maxlen']}]"], 
			['field' => 'info_isimg',		'rules' => "integer"], 
			['field' => 'info_imgurl',		'rules' => "valid_url|min_length[{$f['url_minlen']}]|max_length[{$f['url_maxlen']}]"],
			
			['field' => 'info2',			'rules' => "trim|min_length[{$f['minlen']}]|max_length[{$f['maxlen']}]"], 
			['field' => 'info2_isimg',		'rules' => "integer"], 
			['field' => 'info2_imgurl',		'rules' => "valid_url|min_length[{$f['url_minlen']}]|max_length[{$f['url_maxlen']}]"],
		]);

		// Проверка отправленной формы
		if($this->form_validation->run()) {
			
			$modalmsg = $this->config->item('tplmessages');
			
			$formdata = $this->input->post();
			
			$this->text->update($this->permits->tplselected, $formdata, $this->ion_auth->user()->row()->email);
			
            $resp = ['response' => true, 'body' => $modalmsg['tplsaved']];
        }
        else {
			$resp = ['response' => false, 'body' => $modalmsg['tplfail']];
		}
        
        // Отправляем ответ в ajax-скрипт
        echo json_encode($resp);
    
	}

	/**
	 * Ajax контроллер Добавления нового имени для шаблонов
	 */
	public function addnew() {
        
        if (MAINTENANCE or !$this->input->is_ajax_request() or !$this->is_loggedin or !$this->permits->is_granted) 
            die();

		$this->load->is_loaded('form_validation') OR $this->load->library('form_validation');
		$f = $this->config->item('textparams');
		
		// Настраиваем правила проверки формы
		$this->form_validation->set_rules([
			[
				'field' => 'newtplcode',
				'rules' => "required|trim|min_length[{$f['tpl_minlen']}]|max_length[{$f['tpl_maxlen']}]|alpha_dash|is_unique[".DBTBL_TEMPLATES.".tplcode]",
			],
		]);

		// Проверка отправленной формы
		if($this->form_validation->run()) {
			
			$modalmsg = $this->config->item('tplmessages');

			$newtplcode = $this->input->post('newtplcode');
			
			$this->permits->addNewTplcode($newtplcode);
			$this->text->addNewTemplate($newtplcode, $this->ion_auth->user()->row()->email);
			
            $resp = ['response' => true, 'body' => $modalmsg['newtpladded']];
        }
        else {
			$resp = ['response' => false, 'body' => $modalmsg['newtplfail']];
		}
        
        // Отправляем ответ в ajax-скрипт
        echo json_encode($resp);
    
	}

    /**
	 * Ajax-контроллер проверки занятости имени шаблона
	 * Юзается ajax-механизм проверки поля от Bootstrap Validator
	 */
	public function check() {
        
        if (MAINTENANCE or !$this->input->is_ajax_request() or !$this->is_loggedin or !$this->permits->is_granted) 
            die();
        
        $newtplcode = $this->input->get('newtplcode');
        
		// Код тиража свободен, т.е. не найден в БД
		if(isset($newtplcode) and !$this->text->isTplcodeExists($newtplcode)) {
        
			echo "OK";
        }
        // Неверные входные данные или код тиража занят (найден в БД)
        else {
            header("HTTP/1.1 404 Not Found");
        }
        
    }

}
