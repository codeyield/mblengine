<?php
defined('BASEPATH') OR exit('No direct script access allowed');

const CASHBACK_APPROX_PAYLOAD_FEE = 0.1;

const CASHBACK_TPLID_RUN   = 'run_tpl';			// Шаблон стартового поста
const CASHBACK_TPLID_WIN   = 'win_tpl';			// Шаблон поста в выигрышем
const CASHBACK_TPLID_SORRY = 'sorry_tpl';		// Шаблон поста БЕЗ выигрыша

const CASHBACK_MAX_TXS_VIEW = 10;		// Показывать не более последних транз
const CASHBACK_MAX_TXS_SAVE = 100;		// Сохранять не более последних транз

const CASHBACK_BUTTON = "\xF0\x9F\x91\x8F Поздравляем победителя!";
const CASHBACK_PAYLOAD = 'Congrats with cashback @millionsbiplottery';


class Cashback extends CI_Model {

	
	/*************************************************************************************************
	 * Разыгрывает с указанной вероятностью и мгновенно выплачивает кэшбеки за покупку билетов лотереи
	 */
	public function make($scriptname = '', $t, $d) {
		
		
		/**********************************************************
		 * Проходим по списку розыгрышей кэшбеков в БД
		 **********************************************************/
		foreach($this->cbdata->getRunnedRaffles() as $r) {
			
			// Проверка на совпадение отправленной монеты и суммы для розыгрыша кэшбека
			if(($t['coin'] == $r['coin']) and (round($t['amountf'], 5) == $r['amount'])) {
				
				// Проверяем, что достаточно баланса на выплатном кошельке
				$balance = $this->minter->getBalance($r['wallet'], $r['coin']);
				
				$maybePays = (int) ($balance / ($r['pay'] + CASHBACK_APPROX_PAYLOAD_FEE));
				
				// Баланса недостаточно для выплат
				if($maybePays < 1) {
					continue;
				}
				
				// Инкрементируем в БД честный счетчик срабатываний рандома
				$this->cbdata->updIncrementTry($r['name']);

				// Вероятностный шанс выигрыша в виде N:1
				$chanceCount = (int) (100 / $r['chance']);


				// Получаем статданные текущего розыгрыша
				$s = $this->cbdata->getStat($r['name']);
				
				// Ф-ция сборки строки со списком выплатных транз для подстановки в шаблон
				$getTxsList = function($txs) {
					$k = 1; $text = '';
					foreach(array_filter($txs) as $tx) {
						$text .= '[' .$k++. ' ' .EMOJI_USDFLY. '](' .MSCAN_TXPREFIX.$tx. ') ';
					}
					return $text;
				};
				
				// Укорачиваем до лимита, выкидывая самые старые транзы в НАЧАЛЕ массива
				$s['txs'] = count($s['txs']) > CASHBACK_MAX_TXS_VIEW 
					? array_slice($s['txs'], -CASHBACK_MAX_TXS_VIEW, 999)
					: $s['txs'];
				
				// Собираем подстановочный шаблон для постов
				$replacements = [
					'{{coin}}'			=> $r['coin'],
					'{{amount}}'		=> $r['amount'],
					'{{pay}}'			=> $r['pay'],
					'{{chance}}'		=> $r['chance'],
					'{{chancecount}}'	=> $chanceCount,
					'{{wallet}}'		=> $t['wallet'],
					'{{tries}}'			=> $s['tries'],
					'{{wins}}'			=> (!empty($s['wins']) ? $s['wins'] : 0),
					'{{maybepays}}'		=> $maybePays,
					'{{txslist}}'		=> (!empty($getTxsList($s['txs'])) ? $getTxsList($s['txs']) : '_еще не было_'),
					'{{realchance}}'	=> sprintf("%02.1f", $s['wins'] / $s['tries'] * 100),
					'{{deeplink}}'		=> $this->minter->deepLink($d['wallet'], $r['amount'], $r['coin']),
				];
				
				
				/**********************************************************
				 * Выкидываем рандом с заданной вероятностью и разыгрываем
				 **********************************************************/
				if(rand(1, $chanceCount) == 1) {

//					// START DEBUG! ////////////////////////////////////////////////////////////////////////////////////////////////////////
//					$t['wallet'] = 'Mxf69399d35524a8fb7153d82bcb22a4332cdaa991';
//					$txpay = 'Mt1234567890';
//					$d['chat'] = '@crypt0winners';
//					// END DEBUG! ////////////////////////////////////////////////////////////////////////////////////////////////////////

					/**********************************************************
					 * Выплачиваем приз победителю
					 **********************************************************/
					$txpay = $this->minter->sendCoin($r['mnemonic'], $t['wallet'], $r['amount'], $r['coin'], CASHBACK_PAYLOAD);


					// Сохраняем данные выигрыша в БД
					if(!empty($txpay) and $this->cbdata->updAddWinner($r['name'], $t['wallet'], $txpay)) {

						// Укорачиваем массив транз на одну самую старую вначале
						count($s['txs']) == CASHBACK_MAX_TXS_VIEW ? array_shift($s['txs']) : null;
						
						// добавим в подстановочные данные новый выигрыш
						$replacements['{{wins}}']++;
						$replacements['{{txslist}}'] = $getTxsList(array_merge($s['txs'], [$txpay]));

						$text = trim(strtr(
							$this->cbdata->getTemplate($r['tplcode'], CASHBACK_TPLID_WIN), 
							$replacements
						));

						// Собираем inline кнопку с выплатной транзой
						$keyboard = [
							[['text' => CASHBACK_BUTTON, 'url' => MSCAN_TXPREFIX . $txpay]]
						];

						$postId = $this->tgposter->publish($d['chat'], $text, null, $keyboard);

						if($postId > 0) {

							$this->tgposter->delete($d['chat'], $r['postid']);
							$this->cbdata->updPostId($r['name'], $postId);
						}
						else {
							log_message('error', "{$scriptname}: Не удалось опубликовать пост выигрыша кэшбека {$r['name']}");
						}
						
					}
					
				}

				/**********************************************************
				 * Без выигрыша - публикуем утешительный пост
				 **********************************************************/
				else {

					$text = trim(strtr(
						$this->cbdata->getTemplate($r['tplcode'], CASHBACK_TPLID_SORRY), 
						$replacements
					));
					
					$postId = $this->tgposter->publish($d['chat'], $text);
					
					if($postId > 0) {
						
						$this->tgposter->delete($d['chat'], $r['postid']);
						$this->cbdata->updPostId($r['name'], $postId);
					}
					else {
						log_message('error', "{$scriptname}: Не удалось опубликовать пост без выигрыша кэшбека {$r['name']}");
					}

				}

			}
			
		}
		
		return true;
	}
	
}