<?php
defined('BASEPATH') OR exit('No direct script access allowed');

const BOTDBTBL_USERS = 'user';
const BOTDBTBL_PAYS  = 'buy';


class Botdata extends CI_Model {

private static $botDb;

	
	public function __construct() {
		parent::__construct();
		
		self::$botDb = $this->load->database('botdb', TRUE);
	}

	/**
	 * Возвращает данные таблицы юзеров бота
	 * @return array Массив с полями данных
	 */
	public function getUsers() {
		
		$result = self::$botDb
            ->select('registered,tg_id,username,lang,wallet,invited_by,ref_data,msg_id,to_be_payed')
			->order_by('to_be_payed', 'DESC')
            ->get(BOTDBTBL_USERS);
		
		if(!empty($result)) {
			
			$users = $result->result_array();
			
			$userIds = array_combine(array_column($users, 'tg_id'), array_column($users, 'username'));
			
			foreach($users as &$item) {
				$item['ref_data'] = json_decode($item['ref_data'], true);
				$item['invited']  = isset($userIds[$item['invited_by']]) ? $userIds[$item['invited_by']] : null;
			}
			
			return $users;
		}
		
		return false;
	}
	
	
}