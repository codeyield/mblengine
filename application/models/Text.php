<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Text extends CI_Model {

private $parseMode = TGPARSEMODE_MARKDOWN;
	
	/**
	 * Устанавливает режим парсинга разметки телеграм для постов
	 * @param string $mode TGPARSEMODE_HTML|TGPARSEMODE_MARKDOWN
	 */
	public function setParseMode($mode) {
		
		$this->parseMode = ($mode == TGPARSEMODE_HTML) ? TGPARSEMODE_HTML : TGPARSEMODE_MARKDOWN;
	}

    /**
     * Возвращает массив с текстами шаблонов, флажками наличия изображений и ссылками на изображения
     * @param  string $tplcode Имя комплекта шаблонов
     * @return array|false     Массив данных или false при неуспехе
     */
    public function getAll($tplcode) {
        
		$result = $this->db
            ->select('*')
			->where('tplcode', $tplcode)
            ->get(DBTBL_TEMPLATES);
        
		if ($this->db->affected_rows() > 0) {
			
			return (array) $result->row();
		}
		else {
			return false;
		}

	}

	/**
	 * Сохраняет комплект шаблонов в БД
	 * @param  string  $tplcode             Имя комплекта шаблонов
	 * @param  array   $data                Данные из формы
	 * @param  string  [$whoupdated = null] Email юзера, выполнившего обновление
	 * @return boolean  Успех или неуспех
	 */
	public function update($tplcode, $data, $whoupdated = null) {
		
		// Нужны только поля *_imgurl, т.к. при отключении соотв. поля в форме посредством disabled 
		// во входных данных приходит null, в чего результате это же поле затирается в БД при REPLACE
		$currentdata = $this->getAll($tplcode);
		
        // Переприсвоение обязательно для безопасности и исключения поддельных данных в форме
		$setdata = [
			'tplcode'	=> $tplcode,
			'whoupdated'=> $whoupdated,
			
			'runmnl'	=> (isset($data['runmnl'])	? 		$data['runmnl']		: null),
			'runmnl2'	=> (isset($data['runmnl2'])	? 		$data['runmnl2']	: null),
			'playmnl'	=> (isset($data['playmnl'])	? 		$data['playmnl']	: null),
			'playmnl2'	=> (isset($data['playmnl2'])? 		$data['playmnl2']	: null),
			'info'		=> (isset($data['info'])	? 		$data['info']		: null),
			'info2'		=> (isset($data['info2'])	? 		$data['info2']		: null),
			
			'runmnl_isimg'	=> (isset($data['runmnl_isimg'])	? (bool) ($data['runmnl_isimg']   > 0)	: null),
			'runmnl2_isimg'	=> (isset($data['runmnl2_isimg'])	? (bool) ($data['runmnl2_isimg']  > 0)	: null),
			'playmnl_isimg'	=> (isset($data['playmnl_isimg'])	? (bool) ($data['playmnl_isimg']  > 0)	: null),
			'playmnl2_isimg'=> (isset($data['playmnl2_isimg'])	? (bool) ($data['playmnl2_isimg'] > 0)	: null),
			'info_isimg'	=> (isset($data['info_isimg'])		? (bool) ($data['info_isimg']     > 0)	: null),
			'info2_isimg'	=> (isset($data['info2_isimg'])		? (bool) ($data['info2_isimg']    > 0)	: null),
		];
		
		// Чистим от пустых значений
		$setdata = array_filter($setdata);

		// Эти поля всегда переопределяем явно, даже если они пришли пустые
		$setdata['runmnl_imgurl']	= isset($data['runmnl_imgurl'])		? $data['runmnl_imgurl']	: $currentdata['runmnl_imgurl'];
		$setdata['runmnl2_imgurl']	= isset($data['runmnl2_imgurl'])	? $data['runmnl2_imgurl']	: $currentdata['runmnl2_imgurl'];
		$setdata['playmnl_imgurl']	= isset($data['playmnl_imgurl'])	? $data['playmnl_imgurl']	: $currentdata['playmnl_imgurl'];
		$setdata['playmnl2_imgurl']	= isset($data['playmnl2_imgurl'])	? $data['playmnl2_imgurl']	: $currentdata['playmnl2_imgurl'];
		$setdata['info_imgurl']		= isset($data['info_imgurl'])		? $data['info_imgurl']		: $currentdata['info_imgurl'];
		$setdata['info2_imgurl']	= isset($data['info2_imgurl'])		? $data['info2_imgurl']		: $currentdata['info2_imgurl'];
		
		$this->db
			->set($setdata)
			->where('tplcode', $tplcode)
			->replace(DBTBL_TEMPLATES);

		return (bool) ($this->db->affected_rows() > 0);
	}
	
	/**
	 * Возвращает готовый текст поста для ПУБЛИКАЦИИ, отрендеренный по выбранному шаблону
	 * @param  string       $draw      Массив тиража
	 * @param  string       $textcode  Код текстового поля используемых шаблонов
	 * @return string|false Текст поста или false при неудаче
	 */
	public function render($draw, $textcode) {
		
		// Загружаем полные данные тиража, если не хватает каких-то строго обязательных полей
		if(!isset($draw['drawcode'], $draw['wallet'], $draw['coin'], $draw['bet'], $draw['maxbets'], $draw['prize'], $draw['maxprizes'], $draw['tplcode'])) {
			
			$draw = $this->draw->getDraw($draw['drawcode']);
		}
		
		// Получаем текст шаблона для рендеринга
		$result = $this->db
			->select($textcode)
			->where('tplcode', $draw['tplcode'])
			->get(DBTBL_TEMPLATES);

		$row = $result->row();

		if (isset($row->{$textcode})) {

			// Собираем массив подстановки диплинков с переменными значениями
			$dlReplacements = [];
			
			preg_match_all($dlTpl = "/{{deeplink(\d{1,9})}}/u", $row->{$textcode}, $matches);
			
			foreach($matches[0] as $deeplink) {
				if(preg_match($dlTpl, $deeplink, $amount)) {
					$dlReplacements[$deeplink] = $this->minter->deepLink($draw['wallet'], $amount[1], $draw['coin']);
				}
			}

			// Генерируем список демо-кошельков или берем кошельки победителей из данных тиража (если тираж разыгран)
			$winstring = (isset($draw['winners']) and is_array($draw['winners']))
				? implode("\n", $draw['winners']) 
				: implode("\n", array_fill(0, $draw['maxprizes'], 'Mx1111222233334444555566667777888899990000'));

			// Выполняем замены в шаблоне (с проверкой всех необязательных полей)
			$text = trim(strtr($row->{$textcode}, [
				// Уже прошедшие валидацию поля
				'{{drawcode}}'	=> $draw['drawcode'],
				'{{wallet}}'	=> $draw['wallet'],
				'{{coin}}'		=> $draw['coin'],
				'{{bet}}'		=> $draw['bet'],
				'{{maxbets}}'	=> $draw['maxbets'],
				'{{prize}}'		=> $draw['prize'],
				'{{maxprizes}}'	=> $draw['maxprizes'],
				// Опциональные поля
				'{{title}}'		=> (isset($draw['title'])		? $draw['title']		: ''),
				'{{cashback}}'	=> (isset($draw['cashback'])	? $draw['cashback']		: ''),
				'{{nowbets}}'	=> (isset($draw['nowbets'])		? $draw['nowbets']		: ''),
				'{{nowincome}}'	=> (isset($draw['nowincome'])	? $draw['nowincome']	: ''),
				'{{start}}'		=> (isset($draw['start'])		? $draw['start']		: ''),
				'{{stop}}'		=> (isset($draw['stop'])		? $draw['stop']			: ''),
				// Осталось купить билетов (если nowbets для расчета определен)
				'{{leftbets}}'  => (isset($draw['nowbets'])		? $draw['maxbets'] - $draw['nowbets'] : ''),
				'{{wintotal}}'  => ($draw['prize'] * $draw['maxprizes']),	// Общая сумма розыгрыша
				'{{winners}}'   => $winstring,				// Список выигравших (или демо) кошельков
			] + $dlReplacements));

			return $text == '' ? false : $text;
		}
		
		return false;
	}
	
	/**
	 * Возвращает готовый текст поста для ПРЕВЬЮ, отрендеренный по выбранному шаблону
	 * @param  string       $draw     Массив тиража
	 * @param  string       $textcode Код текстового поля в БД, который нужно извлечь
	 * @return string|false Текст поста или false при неудаче
	 */
	public function renderPreview($draw, $textcode) {
		
		$content = $this->render($draw, $textcode);
		
		$text = is_string($content)
			? str_replace("\n", "<br>", $content) 
			: false;

		return $text;
	}
	
	/**
	 * Добавляет новый комплект шаблонов
	 * @param  string  $newtplcode          Имя комплекта шаблонов
	 * @param  string  [$whoupdated = null] [$whoupdated = null] Email юзера, выполнившего добавление
	 * @return boolean Булев результат запроса
	 */
	public function addNewTemplate($newtplcode, $whoupdated = null) {
		
		return $this->db
			->set([
				'tplcode' => $newtplcode, 
				'whoupdated' => $whoupdated,
			])
			->insert(DBTBL_TEMPLATES);
	}
	
    /**
     * Проверяет существование имени шаблона в БД
     * @param  string $tplcode Имя комплекта шаблонов
     * @return boolean Булев результат запроса
     */
    public function isTplcodeExists($tplcode) {
        
        $this->db
            ->select('tplcode')
			->where('tplcode', $tplcode)
            ->get(DBTBL_TEMPLATES);
        
        return ($this->db->affected_rows() > 0);
    }

}