<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Permits extends CI_Model {

/* 
 * Доступы юзера:
 * @param bool   $is_granted   Права юзера подтверждены
 * @param bool   $is_demo      Демо-режим (кнопки неактивны)
 * @param array  $chats        Список доступных юзеру телеграм чатов/каналов
 * @param array  $tplcodes     Список имен шаблонов для постов для выбранного чата/канала
 * @param string $chatselected Выбранный юзером чат/канал
 * @param string $tplselected  Выбранное юзером имя шаблона для постов
 * @param string $useremail    Авторизационный email юзера
 */
public $is_granted   = false;
public $is_demo      = true;
public $chats        = [];
public $tplcodes     = [];
public $chatselected = false;
public $tplselected  = false;
public $useremail    = false;


	public function __construct() {
		parent::__construct();
	}

    /**
     * Записывает пермиты юзера и данные шаблонов в паблик свойства класса
     * @return boolean Удалось или нет
     */
    public function load($useremail = null) {
		
		$this->useremail = empty($useremail) 
			? $this->ion_auth->user()->row()->email 
			: $useremail;
        
		// Получаем данные юзера
		$result = $this->db
            ->select('chats,is_demo')
			->where('email', $this->useremail)
            ->get(DBTBL_PERMITS);
        
		$permits = $result->row();
		
		// Сохраняем в паблик свойство режим доступа юзера
		$this->is_demo = isset($permits->is_demo) ? (bool) $permits->is_demo : true;
		
		
		// Список чатов юзера не пустой - выполняем основные проверки
		if (!empty($permits->chats)) {
			
			// Сохраняем в паблик свойство список доступных юзеру чатов
			$this->chats = explode(',', $permits->chats);

			$chatCookie = isset($_COOKIE[COOKIE_CHATSELECTED]) ? $_COOKIE[COOKIE_CHATSELECTED] : false;

			// Сохраняем в паблик выбранный чат
			$this->chatselected = in_array($chatCookie, $this->chats)
				? $chatCookie
				: (!empty($this->chats[0]) ? $this->chats[0] : false);
			
			
			// Получаем доступные шаблоны для выбранного чата
			$result = $this->db
				->select('tplcodes')
				->where('chat', $this->chatselected)
				->get(DBTBL_CHATS);
			
			$chat = $result->row();
			
			if(!empty($chat->tplcodes)) {
				
				// Сохраняем в паблик свойство список доступных шаблонов чата
				$this->tplcodes = explode(',', $chat->tplcodes);
				
				$tplCookie = isset($_COOKIE[COOKIE_TPLSELECTED]) ? $_COOKIE[COOKIE_TPLSELECTED] : false;
				
				// Сохраняем в паблик выбранный шаблон
				$this->tplselected = in_array($tplCookie, $this->tplcodes)
					? $tplCookie
					: (!empty($this->tplcodes[0]) ? $this->tplcodes[0] : false);
			}
			
			
			// Права юзера подтверждены, даже если данные чата не удалось получить
			$this->is_granted = true;
			return true;
		}
		
		return false;
    }
	
	/**
	 * Добавляет новое имя комплекта шаблонов для указанного чата/канала
	 * @param  string  $newtplcode Имя комплекта шаблонов
	 * @return boolean Булев результат запроса
	 */
	public function addNewTplcode($newtplcode) {
		
		if((count($this->tplcodes) == 0) or ($this->chatselected === false)) {
			$this->get();
		}
		
		$this->tplcodes = array_unique(array_merge($this->tplcodes, [$newtplcode]));
		
		$result = $this->db
			->set('tplcodes', implode(',', $this->tplcodes))
			->where('chat', $this->chatselected)
            ->update(DBTBL_CHATS);
		
		return $result;
		
	}
	
}