<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * Следующие зависимые модели должны быть загружены в вызывающем контроллере:
 *  minter, trx
 */

class Draw extends CI_Model {
	
	public function __construct() {
		parent::__construct();
	}

    /**
     * Создает новый тираж
     * @param  string $chat Имя телеграм чата/канала
     * @param  array  $data POST-массив с данными формы, прошедший валидацию
     * @return array|false  Массив тиража при успехе или false
     */
    public function create($chat, $data) {

        // Переприсвоение обязательно для безопасности и исключения поддельных данных в форме
		$setdata = [
			'chat'		=> $chat,
			'status'	=> (isset($data['status'])		? 		$data['status']		: DRAWSTATUS_WAIT),
			'start'		=> (isset($data['start'])		? 		$data['start']		: date(YMDHIS)),
			'drawcode'	=> (isset($data['drawcode'])	? 		$data['drawcode']	: null),
			'mode'		=> (isset($data['mode'])		? 		$data['mode']		: null),
			'wallet'	=> (isset($data['wallet'])		? 		$data['wallet']		: null),
			'coin'		=> (isset($data['coin'])		? 		$data['coin']		: null),
			'tplcode'	=> (isset($data['tplcode'])		? 		$data['tplcode']	: null),
			'title'		=> (isset($data['title'])		?		$data['title']		: null),
			'bet'		=> (isset($data['bet'])			? (int) $data['bet']		: null),
			'maxbets'	=> (isset($data['maxbets'])		? (int) $data['maxbets']	: null),
			'prize'		=> (isset($data['prize'])		? (int) $data['prize']		: null),
			'maxprizes'	=> (isset($data['maxprizes'])	? (int) $data['maxprizes']	: null),
			'cashback'	=> (isset($data['cashback'])	? (int) $data['cashback']	: null),
		];
		
		// Чистим от пустых значений
		$setdata = array_filter($setdata);
		
		// Финальная проверка, что все обязательные данные пришли
		if(isset($setdata['drawcode'], $setdata['wallet'], $setdata['coin'], $setdata['tplcode'], 
				 $setdata['bet'], $setdata['maxbets'], $setdata['prize'], $setdata['maxprizes'])) {
			
        	$this->db->set($setdata)->insert(DBTBL_DRAWS);
        	
			return ($this->db->affected_rows() > 0) ? $setdata : false;
		}
		
		return false;
    }

	/**
	 * Прерывает тираж
     * @param  string $drawcode Код тиража
     * @return boolean Успех или нет
	 */
	public function break($drawcode) {
		
		$result = $this->db
			->set('status', DRAWSTATUS_BREAK)
			->set('stop', 'NOW()', false)
			->where('drawcode', $drawcode)
            ->update(DBTBL_DRAWS);
		
		return $result;
	}
	
	/**
	 * Удаляет тираж
     * @param  string $drawcode Код тиража
     * @return boolean Успех или нет
	 */
	public function delete($drawcode) {
		
		$result = $this->db
			->where('drawcode', $drawcode)
            ->delete(DBTBL_DRAWS);
        
		return !($result === false);	// При успехе запроса в $result будет не TRUE, а объект CI_DB_query_builder!
	}

	/**
	 * Возвращает данные выбранного тиража
	 * @param  string $drawcode Код тиража
	 * @return array|bool Ассоциативный массив с данными тиража или false при неудаче
	 */
	public function getDraw($drawcode) {
		
		$result = $this->db
            ->select('*')
			->where('drawcode', $drawcode)
            ->get(DBTBL_DRAWS);
        
		if ($this->db->affected_rows() > 0) {
			
			$row = (array) $result->row();
			
			$row['varprizes'] = $this->_multivalsDecode($row['varprizes']);
			$row['winners']   = $this->_multivalsDecode($row['winners']);
			
			// Считаем фактическое кол-во купленных билетов и выручку
			$row['nowbets']   = $this->trx->countBetsForDraw($drawcode);
			$row['nowincome'] = $row['nowbets'] * $row['bet'];
			
			return $row;
		}
		
		return false;
	}
	
    /**
     * Получает список "активных" тиражей для панели управления: Ожидающих, запущенных и недавно завершенных
     * @param  string $chat Имя чата/канала
     * @return array  Массив строк или пустой массив
     */
    public function getActiveDraws($chat) {
		
		$result = $this->db
            ->select('*')
			->where('chat', $chat)
				->group_start()
					// Ожидающие и запущенные тиражи
					->where_in('status', [DRAWSTATUS_WAIT, DRAWSTATUS_RUN])
					->or_group_start()
						// Или любые завершенные тиражи в течение CLOSED_DRAW_POSTVIEW часов с момента завершения
						->where_in('status', [DRAWSTATUS_DONE, DRAWSTATUS_BREAK])
						->where("`stop` >= DATE_SUB(NOW(), INTERVAL ".CLOSED_DRAW_POSTVIEW." HOUR)", null, FALSE)
					->group_end()
				->group_end()
			->order_by('start', 'DESC')
            ->get(DBTBL_DRAWS);

		return $this->_attachBets(
			$this->_multivalsDecodeArray($result->result_array())
		);
    }
	
    /**
     * Получает список завершенных тиражей для панели управления
     * @param  string $chat Имя чата/канала
     * @return array  Массив строк или пустой массив
     */
    public function getStatisticalDraws($chat) {
        
		$result = $this->db
            ->select('*')
			->where('chat', $chat)
			->where_in('status', [DRAWSTATUS_RUN, DRAWSTATUS_DONE, DRAWSTATUS_BREAK])
			->order_by('stop', 'DESC')
            ->get(DBTBL_DRAWS);
        
		return $this->_attachBets(
			$this->_multivalsDecodeArray($result->result_array())
		);
    }
	
	/**
     * Получает список проверяемых тиражей для загрузчиков транзакций Pull и Mhworker
     * @return array  Массив строк или пустой массив
	 */
	public function getCheckoutDraws() {
		
		$result = $this->db
            ->select('*')
			->where('status', DRAWSTATUS_RUN)
				// Завершенные тиражи в течение CLOSED_DRAW_POSTCHECK часов с момента завершения
				->or_group_start()
					->where_in('status', [DRAWSTATUS_DONE])
					->where("`stop` >= DATE_SUB(NOW(), INTERVAL ".CLOSED_DRAW_POSTCHECK." HOUR)", null, FALSE)
				->group_end()
            ->get(DBTBL_DRAWS);
		
		return (is_object($result) and ($result !== false))
			? $this->_multivalsDecodeArray($result->result_array())
			: [];
	}
	
	/**
	 * Возвращает массив кошельков для кэшбека. Выигравшие кошельки убирает из списка строго по одному, 
	 * так что кол-во кошельков в 2х списках (победители + кэшбек) будет равно числу купленных билетов.
	 * @param  string $drawcode Код тиража
	 * @return array Массив кошельков для кэшбека
	 */
	public function getCashbackWallets($drawcode) {
		
		$winners = $this->getDraw($drawcode)['winners'];
		
		$wallets = $this->trx->getDrawWallets($drawcode);
		
		$cashbacks = $wallets;
		
		foreach($wallets as $i => $wallet) {
			
			if(count($winners) > 0) {
				
				foreach($winners as $j => $winner) {
					
					if($wallet == $winner) {
						unset($cashbacks[$i], $winners[$j]);
						break;
					}
				}
			}
		}
		
		return $cashbacks;
	}
	
	/**
	 * Запускает тираж
     * @param  string  $drawcode Код тиража
     * @return boolean Успех или нет
	 */
	public function setRun($drawcode) {
		
		$result = $this->db
			->set('status', DRAWSTATUS_RUN)
			->set('start', 'NOW()', false)
			->where('drawcode', $drawcode)
            ->update(DBTBL_DRAWS);
		
		return $result;
	}

	/**
	 * Сохраняет данные разыгранного тиража
	 * @param  string  $drawcode Код тиража
	 * @param  array   $winners  Массив со списком кошельков победителей
     * @return boolean Успех или нет
	 */
	public function setPlayout($drawcode, $winners = null) {
		
		$result = $this->db
			->set('status', DRAWSTATUS_DONE)
			->set('stop', 'NOW()', false)
			->set('winners', $this->_multivalsEncode($winners))
			->where('drawcode', $drawcode)
            ->update(DBTBL_DRAWS);
		
		return $result;
	}
	
	/**
	 * Сохраняет выплатные транзакции тиража
	 * @param  string  $drawcode Код тиража
	 * @param  array   $data     Массив изменяемых полей
     * @return boolean Успех или нет
	 */
	public function updTrxs($drawcode, $data) {
		
		$setdata = [
			DRAWTX_PAY		=> (isset($data[DRAWTX_PAY])		? $data[DRAWTX_PAY]		 : null),
			DRAWTX_CASHBACK	=> (isset($data[DRAWTX_CASHBACK])	? $data[DRAWTX_CASHBACK] : null),
			DRAWTX_PROFIT	=> (isset($data[DRAWTX_PROFIT])		? $data[DRAWTX_PROFIT]	 : null),
		];
		
		$setdata = array_filter($setdata);
		
		if(count($setdata) > 0) {
			
			$result = $this->db
				->set($setdata)
				->where('drawcode', $drawcode)
				->update(DBTBL_DRAWS);
			
			return $result;
		}
		
		return false;
	}
	
	/**
	 * Сохраняет ID указанных постов для тиража
	 * @param  string  $drawcode Код тиража
	 * @param  array   $data     Массив изменяемых полей
     * @return boolean Успех или нет
	 */
	public function setPostIds($drawcode, $data) {
		
		$setdata = [
			'runpostid'		=> (isset($data['runpostid'])	? $data['runpostid']	: null),
			'infopostid'	=> (isset($data['infopostid'])	? $data['infopostid']	: null),
			'endpostid'		=> (isset($data['endpostid'])	? $data['endpostid']	: null),
			
			'runpostid2'	=> (isset($data['runpostid2'])	? $data['runpostid2']	: null),
			'infopostid2'	=> (isset($data['infopostid2'])	? $data['infopostid2']	: null),
			'endpostid2'	=> (isset($data['endpostid2'])	? $data['endpostid2']	: null),
		];
		
		// НЕ выкидываем равные нулю значения!
		$setdata = array_filter($setdata, function($var) { return isset($var); });
		
		if(count($setdata) > 0) {
			
			$result = $this->db
				->set($setdata)
				->where('drawcode', $drawcode)
				->update(DBTBL_DRAWS);
			
			return $result;
		}
		
		return false;
	}
	
	/**
	 * Возвращает заголовок крайней завершившейся лотереи в этом чате (для подстановки в Newdraw вместо куки)
	 * @param  string        $chat Чат/канал тиража
	 * @return array|boolean Массив или false
	 */
	public function getLastParams($chat) {
		
		$result = $this->db
            ->select('mode,coin,wallet,tplcode,title')
			->where('chat', $chat)
			->order_by('stop', 'DESC')
			->limit(1)
            ->get(DBTBL_DRAWS);
        
		return (($result !== false) and is_object($result))
			? (array) $result->row()
			: false;
	}
	
    /**
     * Проверяет существование кода тиража в БД
     * @param  string $drawcode Код тиража
     * @return boolean Успех или нет
     */
    public function isDrawcodeExists($drawcode) {
        
        $this->db
            ->select('drawcode')
			->where('drawcode', $drawcode)
            ->get(DBTBL_DRAWS);
        
        return ($this->db->affected_rows() > 0);
    }
	
	
	/********************** Методы для авто-перезапуска тиражей ************************/
	
	/**
	 * Автоматически запускает новый тираж, используя код другого тиража в качестве пресета
	 * @param  string  $chat             Чат/канал, в котором будет запущен тираж
	 * @param  string  $drawcodeAsPreset Код тиража в качестве пресета
	 * @return boolean Получилось или нет
	 */
	public function autorun($chat, $drawcodeAsPreset) {
		
		// Получаем данные тиража, который используется в качестве пресета для нового
		$result = $this->db
            ->select('*')
			->where('drawcode', $drawcodeAsPreset)
            ->get(DBTBL_DRAWS);
        
		if(($result !== false) and ($this->db->affected_rows() > 0)) {
			
			// Данные тиража-пресета
			$pdraw = $result->row();
			
			// Формируем данные нового тиража
			$data = [
				//'chat'		=> $chat,
				'status'	=> DRAWSTATUS_RUN,
				'start'		=> date(YMDHIS),
				'drawcode'	=> $this->nextDrawcode($chat),
				'mode'		=> $pdraw->mode,
				'wallet'	=> $pdraw->wallet,
				'coin'		=> $pdraw->coin,
				'tplcode'	=> $pdraw->tplcode,
				'title'		=> $pdraw->title,
				'bet'		=> $pdraw->bet,
				'maxbets'	=> $pdraw->maxbets,
				'prize'		=> $pdraw->prize,
				'maxprizes'	=> $pdraw->maxprizes,
				'cashback'	=> $pdraw->cashback,
			];
			
			// Создаем новый тираж - запуск уже определён в массиве с данными
			return $this->create($chat, $data);
		}
		
		return false;
	}
	
	/**
	 * Генерирует код следующего тиража вида ABC100 или ABC1000 с приращением индекса
	 * @param  string $chat Канал/чат для которого создается код
	 * @return string|false Код тиража или false при неудаче
	 */
	public function nextDrawcode($chat) {

		$result = $this->db
            ->select('drawcode')
			->where('chat', $chat)
            ->get(DBTBL_DRAWS);
        
		if ($this->db->affected_rows() > 0) {
			
			$drawcodes = array_column($result->result_array(), 'drawcode');
			
			$letters = [];
			$maxvalue = 0;
			
			foreach($drawcodes as $drawcode) {
				
				preg_match("/^([A-Z]{2,4})([0-9]{2,4})$/i", $drawcode, $match);
				
				$letters[] = isset($match[1]) ? $match[1] : '';
				$maxvalue  = isset($match[2]) ? max((int) $match[2], $maxvalue) : $maxvalue;
			}
			
			rsort($letters);
			
			$qtydigits = ($maxvalue + 1) > 999 ? 4 : 3;
			
			$newcode = $letters[0] . sprintf("%0{$qtydigits}d", $maxvalue + 1);
			
			// Этот новый код точно уникальный?
			return $this->isDrawcodeExists($newcode) ? false : $newcode;
		}
		
		return false;
	}
	
	/**
	 * Проверяет наличие запущенных тиражей
	 * @param  string $chat Проверяемый канал/чат
	 * @return boolean Есть запущенные тиражи или нет
	 */
	public function isRunningSomeDraws($chat) {
		
		$result = $this->db
            ->select('drawcode')
			->where('chat', $chat)
			->where('status', DRAWSTATUS_RUN)
            ->get(DBTBL_DRAWS);
		
		return ($this->db->affected_rows() > 0);
	}
	
    /**
     * Проверяет валидность списка кодов тиражей для указанного канала
     * @param  string $drawcodes Строка списков кодов тиражей через запятую
     * @return boolean Успех или нет
     */
    public function isDrawcodesValid($drawcodes) {
		
		if(empty($this->permits->chatselected)) {
			return false;
		}
        
        $result = $this->db
            ->select('drawcode')
			->where('chat', $this->permits->chatselected)
            ->get(DBTBL_DRAWS);
		
		// Разворачиваем строку с кодами в массив и фильтруем от пустых значений
		$drawcodes = array_filter(explode(',', $drawcodes));
        
        return $this->db->affected_rows() > 0
			// Если КАЖДОЕ из проверяемых значений имеется в списке кодов из БД, то array_diff вернёт ПУСТОЙ массив
			? (count(array_diff($drawcodes, array_column($result->result_array(), 'drawcode'))) == 0)
			: false;
    }
	
	
	/*************************** Private методы ***************************/
	
	/**
	 * Прикрепляет поля nowbets, nowincome к каждому тиражу в массиве массивов тиражей
	 * @private
	 * @param  array $draws Массив массивов тиражей
	 * @return array Массив массивов тиражей
	 */
	private function _attachBets($draws) {
		
		foreach($draws as $key => $draw) {
			
			$draws[$key]['nowbets'] = $this->trx->countBetsForDraw($draw['drawcode']);
			$draws[$key]['nowincome'] = $draws[$key]['nowbets'] * $draw['bet'];
		}
		
		return $draws;
	}
	
	/**
	 * Энкодер текстовых полей данных varprizes и winners в массиве тиражей
	 * @private
	 * @param  array $data Входной массив тиражей
	 * @return array Выходной массив тиражей
	 */
	private function _multivalsDecodeArray($data) {
		
		foreach($data as $key => $item) {
			
			$varprizes = $this->_multivalsDecode($item['varprizes']);
			$winners   = $this->_multivalsDecode($item['winners']);

			$data[$key]['varprizes'] = is_null($varprizes) ? [] : $varprizes;
			$data[$key]['winners']   = is_null($winners)   ? [] : $winners;
		}
		
		return $data;
	}
	
	/**
	 * Энкодер текстовых полей данных varprizes и winners: Если ключи требуются, то кодируем в json, иначе склеиваем в строку с разделителем запятой
	 * @param  array       $data               Массив данных для упаковки
	 * @param  boolean     [$needkeys = false] Нужно ли сохранять массив вместе с ключами?
	 * @return string|null Строка для сохранения в БД или null при ошибке
	 */
	private function _multivalsEncode($data, $needkeys = false) {
		
		if (isset($data) and is_array($data) and (count($data) > 0)) {
			
			if($needkeys) {
				$jsonResult = json_encode($data);
			}

			return ($needkeys and ($jsonResult !== false))
				? $jsonResult
				: implode(',', $data);
		}
		else {
			return null;
		}
	}
	
	/**
	 * Декодер текстовых полей данных varprizes и winners, которые могут быть сохранены как в json, так и в виде строки с разделителем запятой
	 * @param  string $data Строка данных для декодирования
	 * @return array|null   Массив элементов или null, если пусто
	 */
	private function _multivalsDecode($data) {
		
		$result = json_decode($data, true);
		
		// Json декодировать не получилось, но во входной строке что-то есть
		if (is_null($result) and is_string($data) and ($data !== '')) {
			
			$result = explode(',', $data);
			
			return is_array($result) ? $result : null;
		}
		else {
			return null;
		}
	}

}