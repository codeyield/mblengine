<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Trx extends CI_Model {

private $queryCounter = 0;				// Счетчик запросов к БД
	
	
	/**
	 * Единая ф-ция расчета недостающих в транзакции полей, исходя из стоимости одного билета тиража
	 * @param  integer $trxvalue Исходная сумма транзакции
	 * @param  integer $bet      Стоимость одного билета тиража
	 * @return array   Массив рассчититанных значений
	 */
	public function calcFieldValues($trxvalue, $bet) {
		
		$amountf  = round($trxvalue / PIPS, PRECISION);				// Сумма покупки как float
		$bets     = (int) ($amountf / $bet);						// Кол-во целых купленных билетов
		$oddbackf = round($amountf - ($bets * $bet), PRECISION);	// Сумма сдачи, если была переплата
		
		return ['amountf' => $amountf, 'bets' => $bets, 'oddbackf' => $oddbackf];
	}
	
	/**
	 * Устанавливает новый статус и обновляет недостающие данные транзакции
	 * @param  string $trx  ID транзакции
	 * @param  array  $data Массив полей для обновления
	 * @return boolean Обновили или нет
	 */
	public function setTrxAsChecked($trx, $data) {
		$this->_reconnect();
		
		// Перепроверяем поля для защиты данных
		$setdata = [
			'status'	=> (isset($data['status'])		? $data['status']	: null),
			'drawcode'	=> (isset($data['drawcode'])	? $data['drawcode']	: null),
			'amountf'	=> (isset($data['amountf'])		? $data['amountf']	: null),
			'bets'		=> (isset($data['bets'])		? $data['bets']		: null),
			'oddbackf'	=> (isset($data['oddbackf'])	? $data['oddbackf']	: null),
			'txreject'	=> (isset($data['txreject'])	? $data['txreject']	: null),	// false значение тоже удалится в array_filter()
		];

		$result = $this->db
			->set(array_filter($setdata))
			->where('trx', $trx)
			->update(DBTBL_TRXS);
		
		return $result;
	}
	
	/**
	 * Возвращает список транзакций, ожидающих обработки
	 * @return array Массив массивов транзакций
	 */
	public function getCheckoutTrxs() {
		$this->_reconnect();
		
		$result = $this->db
            ->select('*')
			->where('`status` IS NULL', null, false)
			->order_by('block', 'ASC')
            ->get(DBTBL_TRXS);
		
		return (is_object($result) and ($result !== false))
			? $result->result_array()
			: [];
	}
	
	/**
	 * Считает сумму билетов по транзакциям тиража
	 * @param  string  $drawcode Код тиража
	 * @return integer Сумма билетов тиража
	 */
	public function countBetsForDraw($drawcode) {
		$this->_reconnect();
		
		$result = $this->db
            ->select_sum('bets')
			->where('drawcode', $drawcode)
			->where('bets > 0', null, false)	// Чтобы не считать левые монеты, т.к. для них bets = 0
			->where_in('status', [TXSTATUS_EVEN, TXSTATUS_OVERPAY, TXSTATUS_EXCESS])
			->get(DBTBL_TRXS);
		
		$data = $result->result();
		
		return isset($data[0]->bets) ? (int) $data[0]->bets : 0;
	}
	
	/**
	 * Считает сумму билетов по указанному кошельку
	 * @param  string  $drawcode Код тиража
	 * @param  string  $wallet   Кошелек покупателя
	 * @return integer Сумма билетов тиража
	 */
	public function countBetsForWallet($drawcode, $wallet) {
		$this->_reconnect();
		
		$result = $this->db
            ->select_sum('bets')
			->where('drawcode', $drawcode)
			->where('wallet', $wallet)
			->where('bets > 0', null, false)	// Чтобы не считать левые монеты, т.к. для них bets = 0
			->where_in('status', [TXSTATUS_EVEN, TXSTATUS_OVERPAY, TXSTATUS_EXCESS])
			->get(DBTBL_TRXS);
		
		$data = $result->result();
		
		return isset($data[0]->bets) ? (int) $data[0]->bets : 0;
	}

	/**
	 * Возвращает данные транзакций оплат для тиража
	 * @private
	 * @param  string  $drawcode Код тиража
	 * @return array   Массив с транзакциями для указанного тиража
	 */
	public function getDrawStat($drawcode) {
		
		$result = $this->db
            ->select('blocktime,block,trx,wallet,amountf,coin,bets,payload')
			->where('drawcode', $drawcode)
			->where('bets > 0', null, false)	// Чтобы не считать левые монеты, т.к. для них bets = 0
			->where_in('status', [TXSTATUS_EVEN, TXSTATUS_OVERPAY, TXSTATUS_EXCESS])
			->order_by('block', 'DESC')
            ->get(DBTBL_TRXS);
		
		return (is_object($result) and ($result !== false))
			? $result->result_array()
			: [];
	}

	/**
	 * Возвращает список кошельков участников тиража (покупателей билетов)
	 * @param  string $drawcode Код тиража
	 * @return array  Массив со списком кошельков
	 */
	public function getDrawWallets($drawcode) {
		$this->_reconnect();
		
		$result = $this->db
            ->select('wallet,bets')
			->where('drawcode', $drawcode)
			->where('bets > 0', null, false)
			->where_in('status', [TXSTATUS_EVEN, TXSTATUS_OVERPAY, TXSTATUS_EXCESS])
            ->get(DBTBL_TRXS);
		
		$trxs = $result->result_array();
		
		$wallets = [];
		
		// Размножает кошельки пропорционально числу купленных билетов
		if(!empty($trxs) and is_array($trxs)) {
			
			foreach($trxs as $trx) {
				$wallets = array_merge($wallets, array_fill(0, $trx['bets'], $trx['wallet']));
			}
		}
		
		return $wallets;
	}

	/**
	 * Возвращает список кошельков покупателей билетов тиража за вчерашний день
	 * @return array Массив массивов транзакций
	 */
	public function getYesterdayBuyers($drawcode) {
		
		$result = $this->db
			->select('blocktime,block,wallet,bets')		// amountd,trx
			->where('drawcode', $drawcode)
			->where('bets > 0', null, false)
			->where_in('status', [TXSTATUS_EVEN, TXSTATUS_OVERPAY, TXSTATUS_EXCESS])
			->where('DATE(blocktime) = DATE(NOW() - INTERVAL 1 DAY)', null, false)
			->order_by('block', 'DESC')
			->get(DBTBL_TRXS);
		
		return (is_object($result) and ($result !== false))
			? $result->result_array()
			: [];
	}
	
	/**
	 * Переподключает соединение с БД после указанного числа запросов в константе конфига
	 * Требуется для избежания исчерпания памяти движком БД CI и ошибок "Allowed memory size of NNN bytes exhausted" на консольных скриптах
	 * @private
	 */
	private function _reconnect() {
		
		if(++$this->queryCounter >= QUERIES_TO_RECONNECT) {
			$this->db->reconnect();
			$this->queryCounter = 0;
		}
		
		return null;
	}
	
}