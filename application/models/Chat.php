<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Chat extends CI_Model {
	
	public function __construct() {
		parent::__construct();
	}

	/**
	 * Возвращает список всех чатов
	 * @return array Линейный массив со списком чатов
	 */
//	public function getChats() {
//		
//		$result = $this->db
//			->select('chat')
//			->get(DBTBL_CHATS);
//			
//		return (($result !== false) and is_object($result))
//			? array_column($result->result_array(), 'chat')
//			: [];
//	}

	/**
	 * Возвращает данные чата, которые юзаются для авто-запуска тиражей
	 * @param  string      Телеграм чат/канал
	 * @return array|false Массив: Флаг автозапуска новых тиражей + Список кодов тиражей, параметры которых юзаются для авто-запуска
	 */
	public function getAutorunData($chat) {
		
		$result = $this->db
			->select('is_autorun,apresets')
			->where('chat', $chat)
			->get(DBTBL_CHATS);
			
		if(($result !== false) and is_object($result)) {
			
			$data = $result->row();
			
			return [
				'is_autorun' => !empty($data->is_autorun),
				'apresets'   => (empty($data->apresets) ? [] : explode(',', $data->apresets)),
			];
		}
		
		return false;
	}
	
	/**
	 * Сохраняет данные чата, которые юзаются для авто-запуска тиражей
	 * @param  string  $chat Телеграм чат/канал
	 * @param  array   $data POST-массив с данными формы, прошедший валидацию
	 * @return boolean Успех или нет
	 */
	public function setAutorunData($chat, $data) {
		
		$apresets   = isset($data['apresets']) ? $data['apresets'] : '';
		$is_autorun = (isset($data['is_autorun']) and ($apresets != '')) ? true : false;	// Выключаем автозапуск, если строка пресетов пустая

		$result = $this->db
			->set(['apresets' => $apresets, 'is_autorun' => $is_autorun])
			->where('chat', $chat)
			->update(DBTBL_CHATS);

		return $result;
	}

}