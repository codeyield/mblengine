<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Класс подсчёта отказов для произвольного массива индексов и рандомного выбора варианта с меньшим числом отказов
 *
 * @author     Dmitry P. <https://github.com/codeyield/>
 * @license    -
 * @copyright  2020 codeyield
 * @link       -
 */

class Failer extends CI_Model {

public $fails = [];
	
	/**
	 * Создает внутренний массив счетчиков ошибок для произвольного списка ключей
	 * @param array $keys Скалярный массив с ключами
	 */
	public function init($keys) {
		
		$this->fails = array_combine($keys, array_fill(0, count($keys), 0));
		return;
	}
	
	/**
	 * Обнуляет внутренний массив счетчиков ошибок
	 */
	public function reset() {
		
		$this->fails = array_map(function($n) { return 0; }, $this->fails);
		return;
	}

	/**
	 * Инкрементирует счетчик ошибок для указанного ключа
	 * @param  string|int $key Ключ для которого инкрементируется ошибка
	 * @return string|int Кол-во ошибок для ключа
	 */
	public function countErr($key) {
		
		return isset($this->fails[$key]) ? ++$this->fails[$key] : null;
	}
	
	
	/**
	 * Возвращает случайный ключ внутреннего массива со счетчиком ошибок.
	 * Вероятность выпадения ключа обратно пропорциональна числу накопленных ошибок и в сумме составляет ≈100% для всех ключей.
	 * @return string|null Ключ внутреннего массива или null, если массив пустой
	 */
	function getOptKey() {
		
		$distrib = array_map(function($n) {
			return (1 / ($n + 0.01));
		}, $this->fails);
		
		$sum = array_sum($distrib);
		
		array_walk($distrib, function(&$n, $key, $sum) {
			$n = $n / $sum * 100;
		}, $sum);
		
		$duplicates = [];
		foreach ($distrib as $key => $percent) {
			$duplicates = array_merge($duplicates, array_fill(0, (int) round($percent, 0), $key));
		}
		
		return !empty($duplicates) ? $duplicates[array_rand($duplicates)] : null;
	}
	
}