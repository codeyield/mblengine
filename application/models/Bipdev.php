<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Класс получения курса BIP с кэшированием в БД
 *
 * @author     Dmitry P. <https://github.com/codeyield/>
 * @license    -
 * @copyright  2020 codeyield
 * @link       -
 */

// Ссылка Bip.dev API для загрузки курса + Интервал обновления курса BIP, мин.
defined('BIPDEV_APIURL_GETPRICE') or define('BIPDEV_APIURL_GETPRICE', 'https://api.bip.dev/api/price');
defined('BIPDEV_UPDATE_INTERVAL') or define('BIPDEV_UPDATE_INTERVAL', 180);


class Bipdev extends CI_Model {

	protected $usdprice = false;
	protected $btcprice = false;
	protected $lastupd  = false;
	
	// Массив рез-тов запросов estimateCoinSell() на отметку времени:
	// Тикер монеты [ключ] => Отметка времени крайнего запроса time() [значение]
	protected $coinprices = [];
	
	
	/**
	 * [[Description]]
	 * @param [[Type]] $amount [[Description]]
	 */
	public function coinToUsdRate($amount) {
		
		
	}
	
	/**
	 * [[Description]]
	 * @return [[Type]] [[Description]]
	 */
	public function usdRate() {
		
		if(empty($this->usdprice)) {
			$this->_get();
		}
		
		return empty($this->usdprice) ? false : ($this->usdprice / 10000);
	}
	
	
	/**
	 * [[Description]]
	 * @private
	 * @return [[Type]] [[Description]]
	 */
	private function _get() {
		
		// В свойствах класса данных нет или интервал уже истёк
		if(empty($this->usdprice) or empty($this->btcprice) or empty($this->lastupd) or ((time() - $this->lastupd) > (60 * BIPDEV_UPDATE_INTERVAL))) {
			
			// Получаем данные из БД
			$result = $this->db
				->select('usdprice,btcprice,lastupd')
				->where('nomkey', 'bipprice')
				->get(DBTBL_BIPDEV);
			
			$data = $result->row();
			
			if(isset($data->usdprice, $data->btcprice, $data->lastupd)) {
				$this->usdprice = $data->usdprice;
				$this->btcprice = $data->btcprice;
				$this->lastupd  = strtotime($data->lastupd);
			}
			
			// В БД данных нет или интервал уже истёк
			if(empty($this->usdprice) or empty($this->btcprice) or empty($this->lastupd) or ((time() - $this->lastupd) > (60 * BIPDEV_UPDATE_INTERVAL))) {
				
				// Получаем курс по API
				$result = file_get_contents(BIPDEV_APIURL_GETPRICE);
				$data   = json_decode($result);
				
				if(isset($data->data->sell_price, $data->data->btc_price)) {
					$this->usdprice = $data->data->sell_price;
					$this->btcprice = $data->data->btc_price;
					$this->lastupd  = time();
					
					// Сохраняем в БД
					$this->db
						->set([
							'usdprice' => $data->data->sell_price, 
							'btcprice' => $data->data->btc_price,
						])
						->where('nomkey', 'bipprice')
						->update(DBTBL_BIPDEV);
				}
			}
		}
		
		return;
	}
	
}