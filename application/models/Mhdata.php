<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mhdata extends CI_Model {

private $queryCounter = 0;				// Счетчик запросов к БД
	
	
	/**
	 * Возвращает массив кошельков проверяемых тиражей
	 * @return array Массив кошельков
	 */
	public function getCheckoutWallets() {
		$this->_reconnect();
		
		$result = $this->db
            ->select('wallet')
			->where('status', DRAWSTATUS_RUN)
				// Завершенные тиражи в течение CLOSED_DRAW_POSTCHECK часов с момента завершения
				->or_group_start()
					->where_in('status', [DRAWSTATUS_DONE, DRAWSTATUS_BREAK])
					->where("`stop` >= DATE_SUB(NOW(), INTERVAL ".CLOSED_DRAW_POSTCHECK." HOUR)", NULL, FALSE)
				->group_end()
            ->get(DBTBL_DRAWS);
		
		$data = $result->result_array();
        
		return (!empty($data) and is_array($data))
			? array_unique(array_column($data, 'wallet'))
			: [];
	}
	
	/**
	 * Определяет наличие указанного кошелька (паблик ключа валидатора) в массиве кошельков (ключей)
	 * @param  string  $wallet  Искомый кошелек (паблик ключ)
	 * @param  array   $wallets Массив кошельков (ключей)
	 * @return boolean Найден или нет
	 */
	public function isWalletFound($wallet, $wallets) {
		
		return in_array(strtolower($wallet), array_map('strtolower', $wallets));
	}
	
	/**
	 * Сохраняет подходящую Send транзакцию в БД
	 * @param  object  $trx Объект с транзакцией
	 * @return boolean Получилось или нет
	 */
	public function addTrxTypeSend($trx, $height = null, $time = null) {

		$amountf = isset($trx->data->value) ? round($trx->data->value / PIPS, PRECISION) : null;
		
		// В query запросе хэш транзы капсом и без 'Mt', в API запросе с 'Mt' и строчными знаками
		$trxhash = isset($trx->hash) ? (substr($trx->hash, 0, 2) == 'Mt' ? substr($trx->hash, 2) : strtolower($trx->hash)) : null;
		
		// В query запросе поле height есть внутри транзы, в API запросе поле height здесь $response->result->height
		$height = isset($trx->height) ? $trx->height : (isset($height) ? $height : null);
		
		// Конвертируем в валидную дату строку вида 2020-03-25T19:36:15.759074145Z
		$blocktime = isset($time) ? strtotime(substr($time, 0, 19)) : false;
		
		$setdata = [
			'trx'		=> $trxhash,
			'block'		=> (isset($height)			? $height						: null),
			'blocktime'	=> (!empty($blocktime)		? date(YMDHIS, $blocktime)		: null),
			'txtype'	=> (isset($trx->type)		? $trx->type					: null),
			'coin'		=> (isset($trx->data->coin)	? $trx->data->coin				: null),
			'amountf'	=> (float) $amountf,
			'amountd'	=> (isset($trx->data->value)? (string) $trx->data->value	: null),	// DECIMAL(36)
			'payload'	=> (isset($trx->payload)	? base64_decode($trx->payload)	: null),
			'wallet'	=> (isset($trx->from)		? $trx->from					: null),
			'walletto'	=> (isset($trx->data->to)	? $trx->data->to				: null),
		];
		
		if(isset($trxhash, $height)) {
			
			$query = $this->db->insert_string(DBTBL_TRXS, array_filter($setdata));
			$result = $this->db->query(str_replace('INSERT INTO', 'INSERT IGNORE INTO', $query));

			return $result ? $this->db->affected_rows() : false;
		}
		else {
			log_message('error', "mhdata->addTrxTypeSend: Invalid tx data Mt{$trxhash} at block {$height}\r\n" . print_r($setdata, 1));
			return false;
		}
	}
	
	/**
	 * Сохраняет подходящую Delegate транзакцию в БД
	 * @param  object  $trx Объект с транзакцией
	 * @return boolean Получилось или нет
	 */
	public function addTrxTypeDelegate($trx, $height = null, $time = null) {
		
		// В query запросе поле height есть внутри транзы, в API запросе поле height здесь $response->result->height
		$height = isset($trx->height) ? $trx->height : (isset($height) ? $height : null);
		
		log_message('error', "Delegate Tx at block {$height}\r\n" . print_r($trx, 1));
		
		return;
	}
	
	/**
	 * Возвращает сохранённый № блока
	 * @return integer № блока
	 */
	public function getBlockNumber() {
		$this->_reconnect();
		
		$result = $this->db
			->select('block')
			->where('nomkey', 'lastblock')
			->get(DBTBL_MHDATA);
        
		$data = $result->row();
		
		return isset($data->block) ? (int) $data->block : 0;
	}

	/**
	 * Сохраняет № блока
	 * @param  integer $block № блока
	 * @return boolean Успех операции
	 */
	public function putBlockNumber($block) {
		$this->_reconnect();
		
		$result = $this->db
			->set('block', $block)
			->where('nomkey', 'lastblock')
            ->update(DBTBL_MHDATA);
		
		return $result;
	}

	/**
	 * Переподключает соединение с БД после указанного числа запросов в константе конфига
	 * Требуется для избежания исчерпания памяти движком БД CI и ошибок "Allowed memory size of NNN bytes exhausted" на консольных скриптах
	 * @private
	 */
	private function _reconnect() {
		
		if(++$this->queryCounter >= QUERIES_TO_RECONNECT) {
			$this->db->reconnect();
			$this->queryCounter = 0;
		}
		
		return null;
	}

}