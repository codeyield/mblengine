<?php
defined('BASEPATH') OR exit('No direct script access allowed');

const DBTBL_CBRAFFLES   = 'cb_raffles';
const DBTBL_CBTEMPLATES = 'cb_templates';

const FIELD_DELIMITER = ',';


class Cbdata extends CI_Model {
	
	/**
	 * Возвращает список запущенных розыгрышей кэшбеков
	 * @return array Массив массивов запущенных розыгрышей
	 */
	public function getRunnedRaffles() {
		
		$result = $this->db
            ->select('*')
			->where('is_run', true)
            ->get(DBTBL_CBRAFFLES);
        
		if(!empty($result->result_array())) {
		
			$raffles = $result->result_array();
			
			foreach($raffles as &$r) {
				$r = $this->_unpackTxs($r);
			}
			
			return $raffles;
		} 
		else {
			return [];
		}
	}
	
	/**
	 * Возвращает указанный кэшбек розыгрыш
	 * @param  string $raffleName Имя розыгрыша
	 * @return array Массив с розыгрышем
	 */
	public function getRaffle($raffleName) {
		
		$result = $this->db
            ->select('*')
			->where('name', $raffleName)
            ->get(DBTBL_CBRAFFLES);
		
		return ($this->db->affected_rows() > 0) ? (array) $result->row() : false;
	}
	
	/**
	 * Обновляет id опубликованного поста
	 * @param  string  $raffleName Имя розыгрыша
	 * @param  integer $postid     id опубликованного поста
	 * @return boolean Успех или нет
	 */
	public function updPostId($raffleName, $postid) {
		
		$result = $this->db
			->set('postid', $postid)
			->where('name', $raffleName)
            ->update(DBTBL_CBRAFFLES);
		
		return $result;
	}
	
	/**
	 * Возвращает данные указанного розыгрыша
	 * @param  string $raffleName Имя розыгрыша
	 * @return array Массив с данными розыгрыша
	 */
	public function getStat($raffleName) {
		
		$result = $this->db
			->select('tries,wins,txs')
			->where('name', $raffleName)
			->get(DBTBL_CBRAFFLES);
		
		return ($this->db->affected_rows() > 0) 
			? $this->_unpackTxs((array) $result->row())
			: false;
	}
	
	/**
	 * Инкрементирует счетчик срабатываний рандома
	 * @param  string $raffleName Имя розыгрыша
	 * @return boolean Успех или нет
	 */
	public function updIncrementTry($raffleName) {
		
		$result = $this->db
			->set('tries', 'tries+1', false)
			->where('name', $raffleName)
            ->update(DBTBL_CBRAFFLES);
		
		return $result;
	}
	
	/**
	 * Добавялет нового победителя в статистику розыгрыша
	 * @param  string $raffleName Имя розыгрыша
	 * @param  string $wallet     Кошелек победителя
	 * @param  string $tx         Транза выплаты победителю
	 * @return boolean Успех или нет
	 */
	public function updAddWinner($raffleName, $wallet, $tx) {
		
		if($s = $this->getStat($raffleName)) {		// Именно присваивание!
				
			// Укорачиваем до лимита, выкидывая самые старые транзы в НАЧАЛЕ массива
			$s['txs'] = count($s['txs']) > CASHBACK_MAX_TXS_SAVE 
				? array_slice($s['txs'], -CASHBACK_MAX_TXS_SAVE, 999)
				: $s['txs'];
			
			$result = $this->db
				->set([
					'wins' => $s['wins'] + 1, 
					'txs'  => implode(FIELD_DELIMITER, array_filter(array_merge($s['txs'], [$tx]))),
				])
				->where('name', $raffleName)
				->update(DBTBL_CBRAFFLES);
				
			return $result;
		}
		
		return false;
	}
	
	/**
	 * Возвращет выбранный шаблон для поста
	 * @param  string $tplcode        Код комплекта шаблонов
	 * @param  string [$tplid = null] Код (поле БД) с выбранным шаблоном
	 * @return string|boolean Текст шаблона или false
	 */
	public function getTemplate($tplcode, $tplid = null) {
		
		$result = $this->db
			->select('*')
			->where('tplcode', $tplcode)
			->get(DBTBL_CBTEMPLATES);
		
		if(($this->db->affected_rows() > 0) and !empty($tplid)) {
			
			$data = (array) $result->row();
			
			return isset($data[$tplid]) ? $data[$tplid] : false;
		}
		
		return false;
	}
	
	/**
	 * Разворачивает список транз в массив в массиве розыгрыша кэшбека
	 * @private
	 * @param  array $txs Массив розыгрыша кэшбека
	 * @return array Измененный массив розыгрыша кэшбека
	 */
	protected function _unpackTxs($r) {
		
		$r['txs'] = (isset($r['txs']) and is_string($r['txs'])) ? explode(FIELD_DELIMITER, $r['txs']) : [];
		
		return $r;
	}
	
}