<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Checkout extends CI_Model {
	
	
	/**
	 * Поочередно обрабатывает необработанные транзакции в БД
	 * Публикация постов д.б. отключена для _парсера_ Pull, чтобы при пакетной выгрузке транз избежать кучи постов одномоментно
	 * Автовыплаты можно включить для парсера позднее, если сохраняется поле txreject и исключены дубли
	 * 
	 * @param  string   [$scriptname = '']     Имя скрипта вызывающего контроллера
	 * @param  string   [$onlyDrawcode = null] Код тиража, если проверяем только этот тираж (опция)
	 * @return integer  Кол-во опубликованных постов в телеграм
	 */
	public function make($scriptname = '', $onlyDrawcode = null) {
		
		$tgPostCount = 0;
		
		
		/**********************************************************
		 * ОБРАБОТКА ВСЕХ ТРАНЗАКЦИЙ (в порядке возрастания блоков)
		 **********************************************************/
		foreach($this->trx->getCheckoutTrxs() as $t) {
			
			$d = null;
			$infoPostId = $endPostId = 0;
			
			// Тиражи переполучаем перед каждой транзой, т.к. любая из них может изменить статус тиража
			// Определяем код тиража для текущей транзакции
			foreach($this->draw->getCheckoutDraws() as $item) {
				// Тираж полуавтомат определяем по пейлоаду и кошельку || Тираж полный автомат определяем по уникальному кошельку тиража
				if($item['wallet'] == $t['walletto']) {
					$d = $item;
					break;
				}
			}
			
			// Транзакция не идентифицирована ИЛИ этот не тот единственный тираж, котрый проверяем
			if(empty($d) or (isset($onlyDrawcode) and ($onlyDrawcode != $d['drawcode']))) {
				continue;
			}
			
			/*-------------------
			 * ТИРАЖ УЖЕ ЗАВЕРШЁН
			 */
			if(in_array($d['status'], [DRAWSTATUS_DONE, DRAWSTATUS_BREAK])) {

				// Обновляем статус и данные транзакции
				$this->trx->setTrxAsChecked($t['trx'], [
					'status'	=> TXSTATUS_LATE,
					'drawcode'	=> $d['drawcode'],
					'txreject'	=> false,
				]);
				
				continue;
			}
			
			/*---------------------------
			 * ПЕРЕВЕДЕНА НЕВЕРНАЯ МОНЕТА
			 */
			if($d['coin'] != $t['coin']) {

				// Обновляем статус и данные транзакции
				$this->trx->setTrxAsChecked($t['trx'], [
					'status'	=> TXSTATUS_BADCOIN,
					'drawcode'	=> $d['drawcode'],
					'txreject'	=> false,
				]);
				
				continue;
			}
			
			
			// Рассчитываем недостающие данные транзакции для тиража
			$txdata = $this->trx->calcFieldValues($t['amountd'], $d['bet']);
			
			// Кол-во билетов в обработанных транзакциях тиража + купленные сейчас
			$d['nowbets'] = $this->trx->countBetsForDraw($d['drawcode']) + $txdata['bets'];
			
			// Считаем итоговую выручку для инфопоста
			$d['nowincome'] = $d['nowbets'] * $d['bet'];
			
			/*------------------------------------------------------------------------------
			 * ЕЩЕ НЕ ВСЕ БИЛЕТЫ ТИРАЖА КУПЛЕНЫ - ПУБЛИКУЕМ УВЕДОМЛЕНИЯ О ПОКУПКЕ БИЛЕТА(ОВ)
			 */
			if($d['nowbets'] < $d['maxbets']) {
				// Переплаты не возвращаем, но если потребуется, то вставить сюда
				
				// Обновляем статус и данные транзакции - ДО рендера и публикации поста!
				$this->trx->setTrxAsChecked($t['trx'], [
					
					'status'	=> ($txdata['oddbackf'] > 0 ? TXSTATUS_OVERPAY : TXSTATUS_EVEN),
					'drawcode'	=> $d['drawcode'],
					'amountf'	=> $txdata['amountf'],
					'bets'		=> $txdata['bets'],
					'oddbackf'	=> $txdata['oddbackf'],
				]);
				
				if(TGPOSTING_ON) {
					
					// Публикуем инфо пост #1 по выбранному шаблону
					$infoPostId = $this->tgposter->publish($d['chat'], $this->text->render($d, TEXTCODE_INFO));

					if($infoPostId > 0) {
						$tgPostCount++;
						
						// Удаляем стартовый пост (однократно) и предыдущий инфопост каждый раз
						$this->tgposter->delete($d['chat'], $d['runpostid']);
						$this->tgposter->delete($d['chat'], $d['infopostid']);
						
						$this->draw->setPostIds($d['drawcode'], [POSTID_RUN => 0, POSTID_INFO => $infoPostId]);
					}
					else {
						log_message('error', "{$scriptname}: Не удалось обновить инфопост #1 {$d['drawcode']} после оплаты {$t['trx']}");
					}
					
					// Публикуем инфо пост #2 по выбранному шаблону
					if(!empty($d['chat2'])) {
						
						$infoPostId2 = $this->tgposter->publish($d['chat2'], $this->text->render($d, TEXTCODE_INFO2));

						if($infoPostId2 > 0) {
							$tgPostCount++;

							// Удаляем стартовый пост (однократно) и предыдущий инфопост каждый раз
							$this->tgposter->delete($d['chat2'], $d['runpostid2']);
							$this->tgposter->delete($d['chat2'], $d['infopostid2']);

							$this->draw->setPostIds($d['drawcode'], [POSTID_RUN2 => 0, POSTID_INFO2 => $infoPostId2]);
						}
						else {
							log_message('error', "{$scriptname}: Не удалось обновить инфопост #2 {$d['drawcode']} после оплаты {$t['trx']}");
						}
					}
				
				}
			}
			
			/*---------------------------------------------------------------------
			 * ВСЕ БИЛЕТЫ ТИРАЖА КУПЛЕНЫ РОВНО ИЛИ СВЕРХ ЛИМИТА - МОЖНО РАЗЫГРЫВАТЬ
			 */
			else {
				// Переплаты не возвращаем, но если потребуется, то вставить сюда
				
				// Обновляем статус и данные транзакции - ДО розыгрыша и рендера поста!
				$this->trx->setTrxAsChecked($t['trx'], [
					
					'status'	=> ($d['nowbets'] > $d['maxbets'] ? TXSTATUS_EXCESS : TXSTATUS_EVEN),
					'drawcode'	=> $d['drawcode'],
					'amountf'	=> $txdata['amountf'],
					'bets'		=> $txdata['bets'],
					'oddbackf'	=> $txdata['oddbackf'],
				]);
				
				/*---------------------------------------------------------------------------
				 * ЗАВЕРШЕНИЕ ТИРАЖА БЕЗ РАНДОМА И ПУБЛИКАЦИЯ ПОСТА
				 */
				
				if($this->draw->setPlayout($d['drawcode'])) {

					if(TGPOSTING_ON) {
						
						// Формируем текст по соотв. шаблону (последующий ручной розыгрыш) и публикуем пост #1
						$endPostId = $this->tgposter->publish($d['chat'], $this->text->render($d, TEXTCODE_PLAYMANUAL));
						
						if($endPostId > 0) {
							$tgPostCount++;
							
							// Удаляем последний инфопост тиража
							$this->tgposter->delete($d['chat'], $d['infopostid']);
							$this->draw->setPostIds($d['drawcode'], [POSTID_INFO => 0, POSTID_END => $endPostId]);
						}
						
						// Формируем текст по соотв. шаблону (последующий ручной розыгрыш) и публикуем пост #2
						if(!empty($d['chat2'])) {
							
							$endPostId2 = $this->tgposter->publish($d['chat2'], $this->text->render($d, TEXTCODE_PLAYMANUAL2));

							if($endPostId2 > 0) {
								$tgPostCount++;

								// Удаляем последний инфопост тиража
								$this->tgposter->delete($d['chat2'], $d['infopostid2']);
								$this->draw->setPostIds($d['drawcode'], [POSTID_INFO2 => 0, POSTID_END2 => $endPostId2]);
							}
						}
					}
				}
				else {
					log_message('error', "{$scriptname}: Не удалось разыграть тираж {$d['drawcode']}");
				}
				
			}	// Конец действий с тиражом
			
			
			/*---------------------------------------------------------------------
			 * РОЗЫГРЫШ КЭШБЕКОВ
			 */
			$this->cashback->make($scriptname, $t, $d);
			
			
			// Страховка от чрезмерного постинга в телеграм. Необработанные транзы обработаются в след. вызов Checkout (≈5 сек)
			if($tgPostCount >= TG_MAX_POSTS_ONCE) {
				break;
			}
			
		}	// end foreach($this->trx->getCheckoutTrxs() as $t)
		
		
		return $tgPostCount;
	}
	
}