<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * Следующие зависимые модели должны быть загружены в вызывающем контроллере:
 *  failer
 */

use Minter\MinterAPI;
use Minter\SDK\MinterTx;
use Minter\SDK\MinterWallet;
use Minter\SDK\MinterDeepLink;
use Minter\SDK\MinterCoins\MinterSendCoinTx;
use Minter\SDK\MinterCoins\MinterMultiSendTx;

use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\ConnectException;

// Не юзается реально, нужен только для подсчета комиссии транзакции
const NOTUSED_WALLET = 'Mxe27b53f26c6e83f20b0bff5e035fa5520c870aa2';


class Minter extends CI_Model {

private $nodes = [];	// Массив с параметрами доступа к нодам
private $apis  = [];	// Массив готовых API объектов для доступных нод (mscan, funfasy)
	
	
	public function __construct() {
		parent::__construct();
		
		// Кросс-вызов в порядке исключения для избежания фатальной ошибки
		// Однако модель failer ДОЛЖНА загружаться в вызывающем контроллере!
		$this->load->is_loaded('failer') or $this->load->model('failer');
		
		// Получаем список API нод и инициализируем счетчики ошибок
		$this->nodes = $this->config->item('minter_nodes_api');
		$this->failer->init(array_keys($this->nodes));
		
		// Формируем массив MinterAPI объектов для каждой ноды
		foreach($this->nodes as $nodekey => $node) {
			$this->apis[$nodekey] = new MinterAPI(new \GuzzleHttp\Client($node));
		}
	}
	
	/**
	 * Создаёт новый Minter кошелек
	 * @return array ['seed' => ..., 'address' => ..., 'mnemonic' => ..., 'public_key' => ..., 'private_key' => ...]
	 */
	public function createWallet() {

		return MinterWallet::create();
	}
	
	/**
	 * Преобразует сумму платежа в значение, аналогичное float (типа 1.5 BIP), для передачи в параметры транзакции
	 * Порогом является PRECISION знаков в числе, при превышении которых число НЕ будет преобразовано!
	 * @private
	 * @param  mixed $val Сумма платежа
	 * @return mixed Преобразованное значение
	 */
	public function _amountToShort($val) {
		
		return $val > (PIPS / (10 ** PRECISION)) ? (function_exists('bcdiv') ? bcdiv($val, PIPS, 18) : $val / PIPS) : $val;
	}
	
	/**
	 * Преобразует сумму платежа в длинное целое или строку
	 * @private
	 * @param  mixed $val Сумма платежа
	 * @return mixed Преобразованное значение
	 */
	public function _amountToLong($val) {
		
		return ($val < PIPS) ? (function_exists('bcmul') ? bcmul($val, PIPS) : $val * PIPS) : $val;
	}
	
	/**
	 * Возвращает баланс кошелька для всех монет или указанной
	 * @param  string $wallet Кошелек
	 * @return mixed  Массив с балансами всех монет | Баланс указанной монеты | false
	 */
	public function getBalance($wallet, $coin = null) {
		
		// Пробуем выполнить API запрос за разрешенное число попыток
		$maxtries = 0;
		do {
			// Выбираем ключ и готовый API объект наиболее стабильной ноды
			$nodekey = $this->failer->getOptKey();
			$api = $this->apis[$nodekey];
			
			try {
				$response = $api->getBalance($wallet);
				
				// Возвращаем баланс, если получилось
				if(isset($response->result->balance)) {
					
					if(!empty($coin) and isset($response->result->balance->{$coin})) {
						
						$val = $response->result->balance->{$coin};
						return function_exists('bcdiv') ? bcdiv($val, PIPS, 18) : $val / PIPS;
					}
					else {
						// Массив с балансами всех монет
						return array_map(function($val) {
							return function_exists('bcdiv') ? bcdiv($val, PIPS, 18) : $val / PIPS;
						}, (array) $response->result->balance);
					}
				}
				else {
					log_message('error', __METHOD__ . ": getBalance failure\n" . print_r($response, 1));
					$this->failer->countErr($nodekey);
					continue;		// Сделаем следующую попытку
				}
				
			} catch(RequestException | ConnectException $e) {
				log_message('error', __METHOD__ . ': ' . $e->getMessage());
				$this->failer->countErr($nodekey);	// Инкрементируем счетчик ошибок ноды
			}
		
		} while (++$maxtries <= MINTERPAY_MAXTRIES);
		
		return false;
	}
	
	/**
	 * Отправляет сумму в монете на указанный кошелек с комиссией за счет ОТПРАВИТЕЛЯ
	 * @param  string       $mnemonic Сид фраза
	 * @param  string       $walletTo Кошелек получателя
	 * @param  mixed        $amount   Сумма перевода
	 * @param  string       $coin     Тикер монеты
	 * @param  string       $payload  Примечание к платежу
	 * @return string|false ID отправленной транзы или false при неудаче
	 */
	public function sendCoin($mnemonic, $walletTo, $amount, $coin, $payload = '') {
		
		if(!PAYMENT_ON) return false;
		
		// Получаем приватный ключ для подписи транзакции и адрес кошелька отправителя
		list($privateKey, $walletFrom) = $this->_getPrivateKeyAndWallet($mnemonic);
		
		// Мера предосторожности от зацикливаний и прочих недоразумений
		if($walletFrom == $walletTo) {
			return false;
		}
		
		// Пробуем выполнить API запрос за разрешенное число попыток
		$maxtries = 0;
		do {
			// Выбираем ключ и готовый API объект наиболее стабильной ноды
			$nodekey = $this->failer->getOptKey();
			$api = $this->apis[$nodekey];
			
			try {
				// Собираем и подписываем транзакцию
				$tx = new MinterTx([
					'nonce'    => $api->getNonce($walletFrom),
					'chainId'  => MinterTx::MAINNET_CHAIN_ID,
					'gasPrice' => 1,
					'gasCoin'  => $coin,
					'type'     => MinterSendCoinTx::TYPE,
					'data'     => [
						'coin'  => $coin,
						'to'    => $walletTo,
						'value' => $amount
					],
					'payload'       => $payload,
					'serviceData'   => '',
					'signatureType' => MinterTx::SIGNATURE_SINGLE_TYPE,
				]);
				
				$tx = $tx->sign($privateKey);
				
				$response = $api->send($tx);
				
				// Возвращаем хэш транзакции, если получилось
				if(isset($response->result->code, $response->result->hash) and ($response->result->code == 0)) {
					
					return 'Mt'.strtolower($response->result->hash);
				}
				else {
					log_message('error', __METHOD__ . ": Tx failure\n" . print_r($response, 1));
					$this->failer->countErr($nodekey);
					continue;		// Сделаем следующую попытку подписать и отправить транзакцию
				}
				
			} catch(RequestException | ConnectException $e) {
				log_message('error', __METHOD__ . ': ' . $e->getMessage());
				$this->failer->countErr($nodekey);	// Инкрементируем счетчик ошибок ноды
			}
		
		} while (++$maxtries <= MINTERPAY_MAXTRIES);
		
		return false;
	}
	
	/**
	 * Отправляет сумму в монете на указанный кошелек с комиссией за счет ПОЛУЧАТЕЛЯ
	 * @param  string       $mnemonic Сид фраза
	 * @param  string       $walletTo Кошелек получателя
	 * @param  mixed        $amount   Сумма перевода
	 * @param  string       $coin     Тикер монеты
	 * @param  string       $payload  Примечание к платежу
	 * @return string|false ID отправленной транзы или false при неудаче
	 */
	public function sendCoinTakeFee($mnemonic, $walletTo, $amount, $coin, $payload = '') {
		
		if(!PAYMENT_ON) return false;
		
		// Получаем приватный ключ для подписи транзакции и адрес кошелька отправителя
		list($privateKey, $walletFrom) = $this->_getPrivateKeyAndWallet($mnemonic);
		
		// Мера предосторожности от зацикливаний и прочих недоразумений
		if($walletFrom == $walletTo) {
			return false;
		}
		
		$amount = $this->_amountToLong($amount);
		
		// Рассчитываем комиссию транзакции с первоначальным пейлоад
		$fee = $this->calcPayloadFee($coin, $payload);
		
		$amountToPay = function_exists('bcsub') ? bcsub($amount, $fee) : $amount - $fee;
		
		// Комса слишком велика, чтобы отправить эту мелочь - рассчитываем еще раз с пустым пейлоад
		if($amountToPay < 0) {
			
			$fee = $this->calcPayloadFee($coin, $payload = '');
			
			$amountToPay = function_exists('bcsub') ? bcsub($amount, $fee) : $amount - $fee;
			
			// Сумма перевода меньше комсы за этот перевод
			if($amountToPay < 0) {
				
				log_message('error', __METHOD__ . ": {$amount} ({$amountToPay}) {$coin} is too small to pay back {$walletFrom}->{$walletTo}");
				return false;
			}
		}
		
		// Пробуем выполнить API запрос за разрешенное число попыток
		$maxtries = 0;
		do {
			// Выбираем ключ и готовый API объект наиболее стабильной ноды
			$nodekey = $this->failer->getOptKey();
			$api = $this->apis[$nodekey];
			
			try {
				// Собираем и подписываем транзакцию
				$tx = new MinterTx([
					'nonce'    => $api->getNonce($walletFrom),
					'chainId'  => MinterTx::MAINNET_CHAIN_ID,
					'gasPrice' => 1,
					'gasCoin'  => $coin,
					'type'     => MinterSendCoinTx::TYPE,
					'data'     => [
						'coin'  => $coin,
						'to'    => $walletTo,
						'value' => $this->_amountToShort($amountToPay)
					],
					'payload'       => $payload,
					'serviceData'   => '',
					'signatureType' => MinterTx::SIGNATURE_SINGLE_TYPE,
				]);
				
				$tx = $tx->sign($privateKey);
				
				$response = $api->send($tx);
				
				// Возвращаем хэш транзакции, если получилось
				if(isset($response->result->code, $response->result->hash) and ($response->result->code == 0)) {
					
					return 'Mt'.strtolower($response->result->hash);
				}
				else {
					log_message('error', __METHOD__ . ": Tx failure\n" . print_r($response, 1));
					$this->failer->countErr($nodekey);
					continue;		// Сделаем следующую попытку подписать и отправить транзакцию
				}
				
			} catch(RequestException | ConnectException $e) {
				log_message('error', __METHOD__ . ': ' . $e->getMessage());
				$this->failer->countErr($nodekey);	// Инкрементируем счетчик ошибок ноды
			}
		
		} while (++$maxtries <= MINTERPAY_MAXTRIES);
		
		return false;
	}
	
	/**
	 * Рассчитывает стоимость комиссии за перевод в указанной монете при заданном тексте в пейлоад
	 * @param  string $coin           Монета для оплаты комиссии перевода
	 * @param  string [$payload = ''] Примечание к платежу
	 * @return string Величина комиссии за перевод в монете $coin (long представление)
	 */
	public function calcPayloadFee($coin, $payload = '') {
		
		// Пробуем выполнить API запрос за разрешенное число попыток
		$maxtries = 0;
		do {
			// Выбираем ключ и готовый API объект наиболее стабильной ноды
			$nodekey = $this->failer->getOptKey();
			$api = $this->apis[$nodekey];
			
			try {
				// Собираем и подписываем транзакцию
				$tx = new MinterTx([
					'nonce'    => 1,				// Левые данные, юзается только для оценки
					'chainId'  => MinterTx::MAINNET_CHAIN_ID,
					'gasPrice' => 1,
					'gasCoin'  => $coin,
					'type'     => MinterSendCoinTx::TYPE,
					'data' => [
						'coin'  => $coin,
						'to'    => NOTUSED_WALLET,	// Левые данные, юзается только для оценки
						'value' => 1				// Левые данные, юзается только для оценки
					],
					'payload'       => $payload,
					'serviceData'   => '',
					'signatureType' => MinterTx::SIGNATURE_SINGLE_TYPE,
				]);

				$tx = $tx->sign('123456');			// Левые данные, юзается только для оценки

				$response = $api->estimateTxCommission($tx);

				// Возвращаем размер комиссии при успехе
				return isset($response->result->commission) ? $response->result->commission : false;

			} catch(RequestException | ConnectException $e) {
				log_message('error', __METHOD__ . ': ' . $e->getMessage());
				$this->failer->countErr($nodekey);	// Инкрементируем счетчик ошибок ноды
			}
		
		} while (++$maxtries <= MINTERPAY_MAXTRIES);
		
		return false;
	}
	
	/**
	 * Отправляет мультисенд транзакцию с одинаковой суммой монет на список кошельков
	 * @param  string       $mnemonic Сид фраза
	 * @param  array        $wallets  Массив кошельков получателей
	 * @param  mixed        $amount   Сумма перевода на каждый кошелек
	 * @param  string       $coin     Тикер монеты
	 * @param  string       $payload  Примечание к платежу
	 * @return string|false ID отправленной транзы или false при неудаче
	 */
	public function multisendCoins($mnemonic, $wallets, $amount, $coin, $payload = '') {
		
		if(!PAYMENT_ON) return false;
		
		// Получаем приватный ключ для подписи транзакции и адрес кошелька отправителя
		list($privateKey, $walletFrom) = $this->_getPrivateKeyAndWallet($mnemonic);
		
		// Пересобираем список кошельков в формат для транзакции
		$wallets4trx = [];
		foreach($wallets as $wallet) {
			$wallets4trx[] = [
				'coin'  => $coin,
				'to'    => $wallet,
				'value' => $this->_amountToShort($amount)
			];
		}
		
		// Пробуем выполнить API запрос за разрешенное число попыток
		$maxtries = 0;
		do {
			// Выбираем ключ и готовый API объект наиболее стабильной ноды
			$nodekey = $this->failer->getOptKey();
			$api = $this->apis[$nodekey];
			
			try {
				// Собираем и подписываем мультисенд транзакцию
				$tx = new MinterTx([
					'nonce'    => $api->getNonce($walletFrom),
					'chainId'  => MinterTx::MAINNET_CHAIN_ID,
					'gasPrice' => 1,
					'gasCoin'  => $coin,
					'type'     => MinterMultiSendTx::TYPE,
					'data'     => [
						'list' => $wallets4trx
					],
					'payload'       => $payload,
					// ОБЯЗАТЕЛЬНЫЕ поля для мультисенда, не убирать!
					'serviceData'   => '',
					'signatureType' => MinterTx::SIGNATURE_SINGLE_TYPE,
				]);
				
				$tx = $tx->sign($privateKey);
				
				$response = $api->send($tx);
				
				// Возвращаем хэш транзакции, если получилось
				if(isset($response->result->code, $response->result->hash) and ($response->result->code == 0)) {
					
					return 'Mt'.strtolower($response->result->hash);
				}
				else {
					log_message('error', __METHOD__ . ": Tx failure\n" . print_r($response, 1));
					$this->failer->countErr($nodekey);
					continue;		// Сделаем следующую попытку подписать и отправить транзакцию
				}
				
			} catch(RequestException | ConnectException $e) {
				log_message('error', __METHOD__ . ': ' . $e->getMessage());
				$this->failer->countErr($nodekey);	// Инкрементируем счетчик ошибок ноды
			}
		
		} while (++$maxtries <= MINTERPAY_MAXTRIES);
		
		return false;
	}
	
	/**
	 * Отправляет всю сумму остатка указанной монеты на кошельке
	 * @param  string         $mnemonic Сид фраза
	 * @param  string         $walletTo  Кошелек получателя
	 * @param  string         $coin     Тикер монеты
	 * @param  string         $payload  Примечание к платежу
	 * @return string|boolean ID отправленной транзы или false при неудаче
	 */
	public function sendCoinAll($mnemonic, $walletTo, $coin, $payload = '') {
		
		if(!PAYMENT_ON) return false;
		
		// Получаем приватный ключ для подписи транзакции и адрес кошелька отправителя
		list($privateKey, $walletFrom) = $this->_getPrivateKeyAndWallet($mnemonic);
		
		// Пробуем выполнить API запрос за разрешенное число попыток
		$maxtries = 0;
		do {
			// Выбираем ключ и готовый API объект наиболее стабильной ноды
			$nodekey = $this->failer->getOptKey();
			$api = $this->apis[$nodekey];
			
			try {
				$response = $api->getBalance($walletFrom);
				
				// Есть баланс в заданной монете, пробуем его отправить, если он положительный
				if(isset($response->result->balance->{$coin})) {
					
					return ($response->result->balance->{$coin} > 0)
						? $this->sendCoinTakeFee($mnemonic, $walletTo, $response->result->balance->{$coin}, $coin, $payload)
						: false;
				}
				
			} catch(RequestException | ConnectException $e) {
				log_message('error', __METHOD__ . ': ' . $e->getMessage());
				$this->failer->countErr($nodekey);	// Инкрементируем счетчик ошибок ноды
			}
		
		} while (++$maxtries <= MINTERPAY_MAXTRIES);
		
		return false;
	}
	
	/**
	 * Собирает диплинк с указанными параметрами
	 * @param  string     $wallet         Кошелек
	 * @param  string|int $value          Сумма к оплате
	 * @param  string     [$coin = 'BIP'] Монета оплаты
	 * @return string     Строка с диплинком
	 */
	public function deepLink($wallet, $value, $coin = 'BIP') {
		
		try {
			$txData = new MinterSendCoinTx([
				'coin'  => $coin,
				'to'    => $wallet,
				'value' => $value
			]);
			
			$link = new MinterDeepLink($txData);
			
		} catch(Exception | RequestException $e) {
			log_message('error', __METHOD__ . ': ' . $e->getMessage());
			return false;
		}

		return $link->encode();
	}
	
	/**
	 * Возвращает приватный ключ для подписи транзакции и адрес кошелька отправителя
	 * @private
	 * @param  string $mnemonic Сид-фраза
	 * @return array  Приватный ключ + кошелек для этой сид фразы
	 */
	private function _getPrivateKeyAndWallet($mnemonic) {
		
		$privateKey = MinterWallet::seedToPrivateKey( MinterWallet::mnemonicToSeed($mnemonic) );
		$walletFrom = MinterWallet::getAddressFromPublicKey( MinterWallet::privateToPublic($privateKey) );
		
		return [$privateKey, $walletFrom];
	}
	
}