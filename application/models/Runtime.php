<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Runtime extends CI_Model {

protected $points = [];

const DEFPOINT = 'default';
	
	/**
	 * Устанавливает время начала отсчёта для указанного параметра
	 * @param string [$key = self::DEFPOINT] Имя параметра
	 */
	public function start($key = self::DEFPOINT) {
		$this->points[$key] = microtime(true);
	}

	/**
	 * Возвращает число секунд, прошедшее с момента установки указанного параметра
	 * @param string [$key = self::DEFPOINT] Имя параметра
	 * @return float
	 */
	public function passed($key = self::DEFPOINT) {
		return (microtime(true) - $this->points[$key]);
	}

	/**
	 * Проверяет, истекло ли указанное число секунд для указанного параметра
	 * @param float|integer $timelimit       Замеряемое время в секундах
	 * @param string [$key = self::DEFPOINT] Имя параметра
	 * @return boolean
	 */
	public function isExpiry($key = self::DEFPOINT, $timelimit) {
		return (bool) ((microtime(true) - $this->points[$key]) > (float) $timelimit);
	}

	/**
	 * Проверяет, истекло ли указанное число секунд для указанного параметра, и перезапускает отчет, если да
	 * @param float|integer $timelimit       Замеряемое время в секундах
	 * @param string [$key = self::DEFPOINT] Имя параметра
	 * @return boolean
	 */
	public function isExpiryRestart($key = self::DEFPOINT, $timelimit) {
		$result = $this->isExpiry($key, $timelimit);
		if($result) {
			$this->start($key);
		}
		return $result;
	}

	/**
	 * Возвращает остаток времени до истечения заданного числа секунд или 0, если уже истекло
	 * @param float|integer $timelimit       Замеряемое время в секундах
	 * @param string [$key = self::DEFPOINT] Имя параметра
	 * @return float Время в секундах
	 */
	public function getTimeLeft($key = self::DEFPOINT, $timelimit) {
		return max(0, $this->points[$key] + $timelimit - microtime(true));
	}
	
	/**
	 * Возвращает форматированное число секунд, прошедшее с момента установки указанного параметра
	 * @param string [$key = self::DEFPOINT] Имя параметра
	 * @return string Форматированная строка '0.00'
	 */
	public function passedStr($key = self::DEFPOINT) {
		return sprintf("%01.2f", $this->passed($key));
	}
	
}