<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tgposter extends CI_Model {
	
	public function __construct() {
		parent::__construct();
		
        $this->load->library('telegram_botapi', ['token' => TELEGRAM_BOT_TOKEN]);
		
		// Только для хостинга НЕ в России!
		if(ENVIRONMENT !== 'production') {
			
			$proxydata = $this->config->item('telegram_proxy');
			
			if(isset($proxydata['type'], $proxydata['proxy'])) {
				$this->telegram_botapi->setProxy($proxydata);
			}
		}
	}

	/**
	 * Публикует пост в Телеграм чат/канал
	 * @param  string       $chatId             Чат/канал в виде @username
	 * @param  string       $text               Текст поста для публикации
	 * @param  string       [$parseMode = null] TGPARSEMODE_MARKDOWN|TGPARSEMODE_HTML
	 * @param  object       [$keyboard  = null] Объект с кнопками клавиатуры
	 * @return integer|bool ID опубликованного поста или false
	 */
	public function publish($chatId, $text, $parseMode = null, $keyboard = null) {
		
		if(!empty($chatId) and !empty($text)) {

			$parseMode = (is_null($parseMode) or ($parseMode === false)) ? $this->config->item('tg_parse_mode') : $parseMode;

			$this->telegram_botapi->sendMessage($chatId, $text, $parseMode, true, null, $keyboard);

			$messageId = $this->telegram_botapi->getLastMessageId();

			return ($messageId > 0) ? $messageId : false;
		}
			
		return false;
	}

	/**
	 * Удаляет пост из Телеграм чата/канала
	 * @param  string  $chatId    Чат/канал в виде @username
	 * @param  integer $messageId ID ранее опубликованного поста
	 * @return bool    Успех или нет
	 */
	public function delete($chatId, $messageId) {
        
		if(!empty($chatId) and !empty($messageId) and ($messageId > 0)) {
			
			return $this->telegram_botapi->deleteMessage($chatId, $messageId);
		}
		
		return false;
	}

}