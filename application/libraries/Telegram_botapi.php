<?php
/**
 * Telegram API Lib for CI
 *
 * @author     Dmitry P. <https://github.com/codeyield/>
 * @license    -
 * @copyright  2020 codeyield
 * @link       -
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Telegram_botapi {


public $botToken      = null;
public $lastResult    = null;
public $lastMessageId = null;
    
// Main bot object
private $bot = null;

// Approx (!) Reg Exp to matching a bot toket
const BOTTOKEN_REGEXP = '/^\d{8,12}:[a-z0-9_\-]{32,38}$/i';

	
    /**
     * Configuring library when loading
     * @private
     * @param array $params = ['token'  => '123456789:ABCDEFGHIJKLMNOPQRSTUVWXYZ123456789', 
     *                         'type'   => CURLPROXY_HTTP|CURLPROXY_SOCKS4|CURLPROXY_SOCKS5, 
     *                         'proxy'  => 'ipv4:port', 
     *                         'usrpwd' => 'login:pass'
     *                        ]
     */
    public function __construct($params = []) {
        
        if (isset($params['token']) and is_string($params['token']) and preg_match(self::BOTTOKEN_REGEXP, $params['token'])) {
            
            $this->botToken = $params['token'];
            
            $this->init($this->botToken);
            
            if(isset($params['type'], $params['proxy'])) {
                $this->setProxy($params);
            }
        }
        
    }
    
    /**
     * Initialize Telegram Bot API
     * @param string [$token = null] Bot token received from @Botfather
     *  Bot token is an option if you configured it when loading the library
     *  
     * @throws \TelegramBot\Api\Exception
     */
    public function init($token = null) {
		
        if (isset($token) and is_string($token) and preg_match(self::BOTTOKEN_REGEXP, $token)) {
            
            $this->botToken = $token;
        }
        
        try {
            $this->bot = new \TelegramBot\Api\BotApi($this->botToken);
            
        } catch (\TelegramBot\Api\Exception $e) {
            $e->getMessage();
        }
        
    }
    
    /**
     * Setting proxy for Telegram
     * @param  array $params = ['type' => CURLPROXY_HTTP|CURLPROXY_SOCKS4|CURLPROXY_SOCKS5, 'proxy' => 'ipv4:port', 'usrpwd' => 'login:pass']
     *  - For 'type' see https://www.php.net/manual/en/curl.constants.php
     *  - 'usrpwd' is optional
     *  
     * @return boolean Is success
     */
    public function setProxy($params) {
        
        if (isset($this->botToken, $params['type'], $params['proxy'])) {
            
            $this->bot->setCurlOption(CURLOPT_HTTPPROXYTUNNEL, true);
            $this->bot->setCurlOption(CURLOPT_PROXYTYPE, $params['type']);
            $this->bot->setCurlOption(CURLOPT_PROXY, $params['proxy']);
            
            if (isset($params['usrpwd'])) {
                
                $this->bot->setCurlOption(CURLOPT_PROXYUSERPWD, $params['usrpwd']);
            }
            
            return true;
        }
        else {
            return false;
        }
    }

    /**
     * Send text messages. On success, the sent \TelegramBot\Api\Types\Message is returned.
     *
     * @param int|string $chatId
     * @param string $text
     * @param string|null $parseMode
     * @param bool $disablePreview
     * @param int|null $replyToMessageId
     * @param array [['text' => 'Text example', 'url' => 'https://minterscan.net'], ...]
     * @param bool $disableNotify
     *
     * @return \TelegramBot\Api\Types\Message
     * @throws \TelegramBot\Api\InvalidArgumentException
     * @throws \TelegramBot\Api\Exception
     */
    public function sendMessage(
        $chatId,
        $text,
        $parseMode = null,
        $disablePreview = false,
        $replyToMessageId = null,
        $keyboard = null,
        $disableNotify = false
    ) {
		
		// Check for the presence of '@' at the beginning of the line and mandatory add if not
		$chatId = substr($chatId, 0, 1) == '@' ? $chatId : '@'.$chatId;

		// Пересобираем массив кнопок (если он не пустой) в объект InlineKeyboardMarkup
		$replyMarkup = (isset($keyboard) and is_array($keyboard) and (count($keyboard) > 0))
			? new \TelegramBot\Api\Types\Inline\InlineKeyboardMarkup($keyboard)
			: null;
        
        try {
            $this->lastResult = $this->bot->sendMessage($chatId, $text, $parseMode, $disablePreview, $replyToMessageId, $replyMarkup, $disableNotify);
			
			$this->lastMessageId = (int) $this->lastResult->getMessageId();
            
        } catch (\TelegramBot\Api\Exception $e) {
            $e->getMessage();
        }
    
        return $this->lastResult;
    }
    
    
    /**
     * Returns the last message ID after it sent
     * @return integer Last message ID
     */
    public function getLastMessageId() {
        
		return $this->lastMessageId;
    }

    
   /**
     * Use this method to delete a message, including service messages, with the following limitations:
     *  - A message can only be deleted if it was sent less than 48 hours ago.
     *  - Bots can delete outgoing messages in groups and supergroups.
     *  - Bots granted can_post_messages permissions can delete outgoing messages in channels.
     *  - If the bot is an administrator of a group, it can delete any message there.
     *  - If the bot has can_delete_messages permission in a supergroup or a channel, it can delete any message there.
     *
     * @param int|string $chatId
     * @param int $messageId
     * @return bool
     */
    public function deleteMessage($chatId, $messageId) {
		
		// Check for the presence of '@' at the beginning of the line and mandatory add if not
		$chatId = substr($chatId, 0, 1) == '@' ? $chatId : '@'.$chatId;
        
        try {
            $this->lastResult = $this->bot->deleteMessage($chatId, $messageId);
            
        } catch (\TelegramBot\Api\Exception $e) {
            $e->getMessage();
        }
    	
        return $this->lastResult;
    }
    
}
