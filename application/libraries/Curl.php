<?php
/**
 * Telegram API Lib for CI
 *
 * @author     Dmitry P. <https://github.com/codeyield/>
 * @license    -
 * @copyright  2020 codeyield
 * @link       -
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Curl {

// Паблик свойства класса для установки опций CURL
public $cookieFile		= false;
// public $SSL			= false;
public $header			= false;
public $userAgent		= false;
public $referer			= false;
public $followLocation	= false;
public $sslVerifyPeer	= false;
public $timeout			= 10;

// Возвращаемый массив ошибок CURL
public $error				= false;

// Список возможных User Agents
protected $userAgents	= array(
	'mozilla' => "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1)",
	'mobile'  => "Opera/9.80 (J2ME/MIDP; Opera Mini/5.0.3521/886; U; en) Presto/2.4.15",
);
	
	/**
	 * Отправляет POST-запрос
	 * @param  string  $url         Целевой URL
	 * @param  array   [$data = []] Массив POST-параметров вида Ключ => Значение
	 * @return boolean Успех/неуспех
	 */
	public function post($url, $data = []) {
		
		return $this->doCURL($this->setOpt() + [
			CURLOPT_URL				=> $url,
			CURLOPT_POST			=> true,
			CURLOPT_POSTFIELDS      => (is_array($data) ? $data : []),
		]);
	}

	/**
	 * Отправляет GET-запрос
	 * @param  string $url           Целевой URL
	 * @param  array [$data = false] Массив GET-параметров вида Ключ => Значение
	 * @return boolean Успех/неуспех
	 */
	public function get($url, $data = false) {
		
		return $this->doCURL($this->setOpt() + [
			CURLOPT_URL				=> $url . (is_array($data) ? '?'.http_build_query($data) : '')
		]);
	}

	// Protected методы /////////////////////////////////////////////////////////////////////////////
	
	/**
	 * Устанавливает CURL-опции из присвоенных public-параметров
	 * @private
	 * @return array Массив опций для curl_setopt
	 */
	protected function setOpt() {
	
		$opts = [
			CURLOPT_HEADER			=> $this->header,
			CURLOPT_USERAGENT		=> ($this->userAgent ? $this->userAgent : $this->userAgents['mozilla']),
			CURLOPT_TIMEOUT			=> $this->timeout,
			CURLOPT_SSL_VERIFYPEER	=> $this->sslVerifyPeer,		// Нужно для https, false если нет сертификата
			// CURLOPT_FOLLOWLOCATION	=> $this->followLocation,
			CURLOPT_RETURNTRANSFER	=> true,			            // Возвращает страницу вместо ее вывода в браузер
		] +
		(!$this->referer    ? [] : [
			CURLOPT_REFERER		    => $this->referer,
		]) +
		(!$this->cookieFile ? [] : [
			CURLOPT_COOKIEJAR       => $this->cookieFile,
			CURLOPT_COOKIEFILE      => $this->cookieFile,
		]);
		
		return $opts;
	} 
	
	/**
	 * Выполняет CURL-запрос
	 * @private
	 * @param  array   $opts Массив опций для curl_setopt
	 * @return boolean Успех/неуспех
	 */
	protected function doCURL($opts) {
		
		$this->error = false;				// Сброс предыдущих ошибок
		$ch = curl_init();
		
		if (curl_setopt_array($ch, $opts) === false) {
			$this->error['code']    = -1;	
			$this->error['msg']     = "Can't set parameter(s) in 'curl_setopt_array'";
			$this->error['options'] = print_r($opts, true);
			$resp = false; 
		}
		elseif (($resp = curl_exec($ch)) === false) {
			$this->error['code']    = curl_errno($ch);	
			$this->error['msg']     = curl_error($ch);
		}
		
		curl_close($ch);
		return $resp;
	}

}