$(document).ready(function () {

	var bet = 0;
	var maxbets = 0;
	var prize = 0;
	var maxprizes = 0;
	var cashback = 0;
	
	/* Проверка изменения расчетных полей в форме */
    $('#id-bet').on('change', function(){
		bet = $(this).val();
        calculate();
    });
    $('#id-maxbets').on('change', function(){
		maxbets = $(this).val();
        calculate();
    });
    $('#id-prize').on('change', function(){
		prize = $(this).val();
        calculate();
    });
    $('#id-maxprizes').on('change', function(){
		maxprizes = $(this).val();
        calculate();
    });
    $('#id-cashback').on('change', function(){
		cashback = $(this).val();
        calculate();
    });
	
	/* Расчет доходности тиража */
	function calculate() {
	
		var income = bet * maxbets;
		var payout = prize * maxprizes;
		var payback = cashback * (maxbets - maxprizes);
		var profit = income - payout - payback;
		var percent = '';
		
		if(income > 0) 
			$('#calc-income').html(income);
		else 
			$('#calc-income').html('?');
		
		if(payout > 0) 
			$('#calc-payout').html(payout);
		else 
			$('#calc-payout').html('?');
		
		if(payback > 0) 
			$('#calc-cashback').html(' - ' + payback);
		else 
			$('#calc-cashback').html('');

		
		if(profit > 0) {
			percent = ' (' + Math.floor(profit / income * 100) + '%)';
		}

		if((income > 0) && (payout > 0)) {
			$('#calc-result').html(profit + percent);
//			$('#calc-block').removeClass('sr-only');
		}
		else {
			$('#calc-result').html('?');
//			$('#calc-block').addClass('sr-only');
		}
		
		if((income > 0) && (payout > 0) && (profit > 0)) {
			$('#calc-result').addClass('text-success');
			$('#calc-result').removeClass('text-danger');
		}
		else if((income > 0) && (payout > 0) && (profit <= 0)) {
			$('#calc-result').addClass('text-danger');
			$('#calc-result').removeClass('text-success');
		}
		else {
			$('#calc-result').removeClass('text-success');
			$('#calc-result').removeClass('text-danger');
		}
		
	};
	
    /* Отправляет форму для создания нового тиража */
    $('#form-newdraw').submit(function() {

		var form = $(this);
        $.ajax({
            type: 'POST',
            url: '/newdraw/create',
            dataType: 'json',
            data: form.serialize(),

            beforeSend: function() {
				var sure = confirm("Создать тираж, вы уверены?");
				if (sure) {
					form.find('button[type="submit"]').prop('disabled', true);
					return true;
				}
				else {
					return false;
				}
			},
            success: function(resp) {
                if (resp['response']) {
					$('#modaldraw-success').modal('show');
                } else {
					$('#modaldraw-error').modal('show');
                }
            },
            error: function(resp) {
				alert('Ошибка Ajax: Сообщите разработчику приложения!');
            },
            complete: function() {
                setTimeout(function() { 
                    form.find('button[type="submit"]').prop('disabled', false);
                }, 5000);
            }
        });
        return false;
	});

	/* Перезагружает страницу после закрытия любого модального окна */
	$('.modal').on('hidden.bs.modal', function () {
		location.reload();
	});

});