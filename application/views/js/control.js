$body = $("body");

$(document).ready(function () {
	
    /* Получает текст от ajax-контроллера и показывает модаль с превью поста */
    $('.event-preview').submit(function() {
		
		var form = $(this);
        $.ajax({
            type: 'POST',
            url: '/control/action',
            dataType: 'json',
            data: form.serialize(),

            beforeSend: function() {
				form.find('button[type="submit"]').prop('disabled', true);
			},
            success: function(resp) {
				
                if(resp['response']) {
					
					if(resp['body']) {
						$('#modalctrl-preview-body').html(resp['body']);
					}
					// Если текст не передан, то выводим текст модали по умолчанию
					$('#modalctrl-preview').modal('show');

				} else {
					
					if(resp['body']) {
						$('#modalctrl-error-body').html(resp['body']);
					}
					// Если текст не передан, то выводим текст модали по умолчанию
					$('#modalctrl-error').modal('show');
                }
            },
            error: function(resp) {
				alert('Ошибка Ajax: Сообщите разработчику приложения!');
            },
            complete: function() {
                setTimeout(function() { 
                    form.find('button[type="submit"]').prop('disabled', false);
                }, 2000);
            }
        });
        return false;
	});
	
    /* Запрашивает подтверждение и отправляет команду для действий с тиражом */
    $('.event-action').submit(function() {
		
		var form = $(this);
        $.ajax({
            type: 'POST',
            url: '/control/action',
            dataType: 'json',
            data: form.serialize(),

            beforeSend: function() {
				var sure = confirm("Вы уверены?");
				if (sure) {
					form.find('button[type="submit"]').prop('disabled', true);
					return true;
				}
				else {
					return false;
				}
			},
            success: function(resp) {
                if(resp['response']) {
					if(resp['body']) {
						$('#modalctrl-okay-body').html(resp['body']);
					}
					// Если текст не передан, то выводим текст модали по умолчанию
					$('#modalctrl-okay').modal('show');

				} else {
					if(resp['body']) {
						$('#modalctrl-error-body').html(resp['body']);
					}
					// Если текст не передан, то выводим текст модали по умолчанию
					$('#modalctrl-error').modal('show');
                }
            },
            error: function(resp) {
				alert('Ошибка Ajax: Сообщите разработчику приложения!');
            },
            complete: function() {
                setTimeout(function() { 
                    form.find('button[type="submit"]').prop('disabled', false);
                }, 2000);
            }
        });
        return false;
	});
	
    /* Разрешает/запрещает кнопку "Дернуть блокчейн" при отметке соответствующего чекбокса */
    $('#checkbox-pull').on('change', function(){
        
        if ($('#checkbox-pull').is(':checked')) 
            $('#button-pull').prop('disabled', false);
        else 
            $('#button-pull').prop('disabled', true);
        
        return true;
    });

    /* Вызывает URL "Дёрнуть блокчейн" и показывает гифку ожидания во время процесса */
    $('#form-pull').submit(function() {
		
		$('#button-pull').prop('disabled', true);
		$('.modal-wait').show();
		
		$.get('/pull2', function() {
			$('#button-pull').prop('disabled', false);
			$('.modal-wait').hide();
			location.reload();
		});
		
		return false;
	});

	/* Перезагружает страницу после закрытия любого модального окна */
	$('.modal').on('hidden.bs.modal', function() {
		location.reload();
	});

});