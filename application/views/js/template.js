$(document).ready(function () {
/*
 * WARNING! По необъяснимой причине НЕЛЬЗЯ называть этот файл templates.js (с 's' на конце),
 * иначе встроенный js-минифиер Brackets глючит и в файле templates.min.js (только в нём)
 * перестает обрабатываться событие $('#form-templates').submit() (только оно)
 */

    /* Показывает/скрывает поле ввода URL при отметке соответствующего чекбокса */
    $('.event-isimg').on('change', function(){
        
		var fieldid = '#id-imgurl-' + this.id.substring('id-isimg-'.length);;
        
        if ($('#' + this.id).is(':checked')) 
            $(fieldid).prop('disabled', false);
        else 
            $(fieldid).prop('disabled', true);
        
        return true;
    });
	
    /* Отправляет форму с текстами шаблона постов */
    $('#form-tpls').submit(function() {
		
		var form = $(this);
        $.ajax({
            type: 'POST',
            url: '/template/update',
            dataType: 'json',
            data: form.serialize(),

            beforeSend: function() {
				form.find('button[type="submit"]').prop('disabled', true);
			},
            success: function(resp) {
                if(resp['response']) {
					
					if(resp['body']) {
						$('#modaltpl-okay-body').html(resp['body']);
					}
					// Если текст не передан, то выводим текст модали по умолчанию
					$('#modaltpl-okay').modal('show');

				} else {
					
					if(resp['body']) {
						$('#modaltpl-error-body').html(resp['body']);
					}
					// Если текст не передан, то выводим текст модали по умолчанию
					$('#modaltpl-error').modal('show');
                }
            },
            error: function(resp) {
				alert('Ошибка Ajax: Сообщите разработчику приложения!');
            },
            complete: function() {
                setTimeout(function() { 
                    form.find('button[type="submit"]').prop('disabled', false);
                }, 2000);
            }
        });
        return false;
	});
	
    /* Создает новый комплект шаблонов для постов */
    $('#form-newtplcode').submit(function() {
		
		var form = $(this);
        $.ajax({
            type: 'POST',
            url: '/template/addnew',
            dataType: 'json',
            data: form.serialize(),

            beforeSend: function() {
				var sure = confirm("Вы уверены?");
				if(sure) {
					form.find('button[type="submit"]').prop('disabled', true);
					return true;
				}
				else {
					return false;
				}
			},
            success: function(resp) {
                if(resp['response']) {
					
					if(resp['body']) {
						$('#modaltpl-okay-body').html(resp['body']);
					}
					// Если текст не передан, то выводим текст модали по умолчанию
					$('#modaltpl-okay').modal('show');

				} else {
					
					if(resp['body']) {
						$('#modaltpl-error-body').html(resp['body']);
					}
					// Если текст не передан, то выводим текст модали по умолчанию
					$('#modaltpl-error').modal('show');
                }
            },
            error: function(resp) {
				alert('Ошибка Ajax: Сообщите разработчику приложения!');
            },
            complete: function() {
                setTimeout(function() { 
                    form.find('button[type="submit"]').prop('disabled', false);
                }, 2000);
            }
        });
        return false;
	});

	/* Перезагружает страницу после закрытия любого модального окна */
	$('.modal').on('hidden.bs.modal', function() {
		location.reload();
	});

});