$(document).ready(function () {

    /* 
     * Переключение юзером чат/канала с записью в куку и перезагрузкой страницы
     */
    $('#chat-selector').on('change', function(){

        var chat = "";
        $("#chat-selector option:selected").each(function() {
            chat = $(this).text();
        });
		
        var exp = (new Date(new Date().getTime() + (86400e3 * 365))).toUTCString();
        
        document.cookie = "chatselected=" + chat + "; path=/; expires=" + exp;

        location.reload();

        return true;
    });

    /* 
     * Переключение юзером комплекта шаблонов с записью в куку и перезагрузкой страницы
     */
    $('#tplcode-selector').on('change', function(){

        var tplcode = "";
        $("#tplcode-selector option:selected").each(function() {
            tplcode = $(this).text();
        });
        
        var exp = (new Date(new Date().getTime() + (86400e3 * 365))).toUTCString();
        
        document.cookie = "tplselected=" + tplcode + "; path=/; expires=" + exp;

        location.reload();

        return true;
    });
	
});