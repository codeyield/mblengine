# Структура БД

# Параметры розыгрышей кэшбеков
CREATE TABLE cb_raffles (
	`name`			CHAR(10)		NOT NULL PRIMARY KEY,			# Произвольный уникальный id
	`is_run`		TINYINT,										# Этот розыгрыш запущен
	`coin`			CHAR(10),										# Монета, которая д.б. оплачена для участия в розыгрыше
	`amount`		MEDIUMINT,										# Сумма, которая д.б. оплачена для участия в розыгрыше
	`pay`			MEDIUMINT,										# Размер выплаты
	`chance`		TINYINT,										# % вероятности выигрыша
	`postid`		MEDIUMINT		NOT NULL DEFAULT 0,				# ID опубликованного поста
	`tplcode`		VARCHAR(12),									# Имя комплекта шаблона для постов
	`tries`			MEDIUMINT		NOT NULL DEFAULT 0,				# Срабатываний скрипта и попыток рандома (если баланса для выплат достаточно)
	`wins`			MEDIUMINT,										# Выигрышей
	`txs`			VARCHAR(7000),									# Список выигравших транзакций (100 транз = 6700 байт)
	`wallet`		CHAR(42),										# Выплатной кошелек
	`mnemonic`		VARCHAR(200),									# Сид-фраза выплатного кошелька
	`tstamp`		TIMESTAMP		DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

INSERT INTO `cb_raffles` (`name`, `is_run`, `coin`, `amount`, `pay`, `chance`, `tplcode`) VALUES ('10BIP', TRUE, 'BIP', 10, 10, 10, 'default');

# Стата розыгрышей по дням
#CREATE TABLE cb_stats (
#	`id`			VARCHAR(32)		NOT NULL PRIMARY KEY,			# Дата + name	// + coin + amount + pay + chance (?)
#	`tries`			MEDIUMINT		NOT NULL DEFAULT 1,				# Срабатываний скрипта и попыток рандома (если баланса для выплат достаточно)
#	`wins`			MEDIUMINT,										# Выигрышей
#	`wallets`		VARCHAR(5000),									# Список выигравших кошельков (100 кошей = 4300 байт)
#	`txs`			VARCHAR(7000),									# Список выигравших транзакций (100 транз = 6700 байт)
#	`tstamp`		TIMESTAMP		DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
#) ENGINE=InnoDB CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

# Шаблоны постов для публикации
CREATE TABLE cb_templates (
	`tplcode`		VARCHAR(12)		NOT NULL PRIMARY KEY,			# Имя комплекта шаблона для постов
	`run_tpl`		VARCHAR(2000),									# Шаблон стартового поста
	`win_tpl`		VARCHAR(2000),									# Шаблон поста при выигрыше
	`sorry_tpl`		VARCHAR(2000),									# Шаблон поста без выигрыша
	`tstamp`		TIMESTAMP		DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
