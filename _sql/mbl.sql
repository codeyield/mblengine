# Структура БД

# Доступные юзеру чаты/каналы для публикации тиражей и шаблоны
CREATE TABLE le_permits (
	`email`			VARCHAR(50)		NOT NULL PRIMARY KEY,			# Email юзера
	`chats`			VARCHAR(250),									# Список доступных этому юзеру телеграм каналов, через запятую
	`is_demo`		BOOLEAN			NOT NULL DEFAULT FALSE			# Демо-режим для юзера = TRUE, нормальный режим = FALSE
) ENGINE=InnoDB CHARACTER SET utf8 COLLATE utf8_general_ci;


# Телеграм чаты/каналы для публикации тиражей
CREATE TABLE le_chats (
	`chat`			CHAR(32)		NOT NULL PRIMARY KEY,			# Имя чата/канала тиража
	`is_autorun`	BOOLEAN			NOT NULL DEFAULT FALSE,			# Перезапускать тиражи в канале автоматически
	`wallets`		VARCHAR(500),									# Список юзаемых кошельков для этого канала, через запятую
	`coins`			VARCHAR(500),									# Список юзаемых тикеров монет для этого канала, через запятую
	`tplcodes`		VARCHAR(500),									# Список юзаемых шаблонов постов для этого канала, через запятую
	`apresets`		VARCHAR(500)									# Список кодов тиражей, параметры которых юзаются для авто-запуска, через запятую
) ENGINE=InnoDB CHARACTER SET utf8 COLLATE utf8_general_ci;


# Тиражи лотереи
CREATE TABLE le_draws (
	`drawcode`		VARCHAR(50)		NOT NULL PRIMARY KEY,			# Код тиража
	`status`		ENUM('wait','run','done','break') NOT NULL DEFAULT 'wait',	# Тираж запущен/завершен/прерван
	`start`			DATETIME,										# Тираж запущен
	`stop`			DATETIME,										# Тираж завершен/прерван/остановлен
	`chat`			CHAR(32),										# Имя чата/канала #1
	`chat2`			CHAR(32),										# Имя чата/канала #2
	`mode`			ENUM('auto','manual','fullauto') NOT NULL DEFAULT 'manual',	# Способ розыгрыша тиража: автоматический/ручной/полный автомат
	`wallet`		CHAR(42),										# Кошелек тиража
	`coin`			CHAR(10),										# Тикер монеты тиража
	`txtype`		TINYINT			UNSIGNED NOT NULL DEFAULT 1,	# Тип транзакции
	`bet`			INT				NOT NULL DEFAULT 0,				# Цена лотерейного билета
	`maxbets`		INT				NOT NULL DEFAULT 0,				# Кол-во билетов, после покупки которых будет разыгран тираж
	`prize`			INT				NOT NULL DEFAULT 0,				# Размер приза каждому победителю
	`maxprizes`		INT				NOT NULL DEFAULT 0,				# Кол-во призов
	`cashback`		INT				NOT NULL DEFAULT 0,				# Размер кэшбека остальным, кто не выиграл
	`varprizes`		VARCHAR(1000),									# Запасное поле при розыгрыше разных сумм призов: json(['1' => VAL1, '2' => VAL2, ...])
	`winners`		VARCHAR(6000),									# Победители: json(['1' => WALLET1, '2' => WALLET2, ...])
	`runpostid`		INT				NOT NULL DEFAULT 0,				# ID опубликованного поста #1 с запуском тиража
	`infopostid`	INT				NOT NULL DEFAULT 0,				# ID опубликованного поста #1 со статой тиража
	`endpostid`		INT				NOT NULL DEFAULT 0,				# ID опубликованного поста #1 с завершением тиража
	`runpostid2`	INT				NOT NULL DEFAULT 0,				# ID опубликованного поста #2 с запуском тиража
	`infopostid2`	INT				NOT NULL DEFAULT 0,				# ID опубликованного поста #2 со статой тиража
	`endpostid2`	INT				NOT NULL DEFAULT 0,				# ID опубликованного поста #2 с завершением тиража
	`tplcode`		VARCHAR(50),									# Имя комплекта шаблона для постов
	`title`			VARCHAR(200)	CHARACTER SET utf8mb4,			# Необязательный заголовок лотереи
) ENGINE=InnoDB CHARACTER SET utf8 COLLATE utf8_general_ci;

ALTER TABLE `le_draws` ADD INDEX(`drawcode`);


# Шаблоны для постов
CREATE TABLE le_templates (
	`tplcode`			VARCHAR(50)		NOT NULL PRIMARY KEY,		# Имя комлекта шаблонов
#
	`runmnl`			VARCHAR(2500),								# Шаблон поста при запуске тиража
	`runmnl_isimg`		BOOLEAN			NOT NULL DEFAULT FALSE,		# Наличие изображения для этого поста
	`runmnl_imgurl` 	VARCHAR(100)	CHARACTER SET utf8,			# URL изображения для этого поста
	`runmnl2`			VARCHAR(2500),								# Шаблон второго поста [EN]
	`runmnl2_isimg`		BOOLEAN			NOT NULL DEFAULT FALSE,		# Наличие изображения для этого поста
	`runmnl2_imgurl` 	VARCHAR(100),								# URL изображения для этого поста
#	
	`playmnl`			VARCHAR(2500),								# Шаблон поста при завершении тиража
	`playmnl_isimg`		BOOLEAN			NOT NULL DEFAULT FALSE,		# Наличие изображения для этого поста
	`playmnl_imgurl`	VARCHAR(100)	CHARACTER SET utf8,			# URL изображения для этого поста
	`playmnl2`			VARCHAR(2500),								# Шаблон второго поста [EN] при завершении тиража
	`playmnl2_isimg`	BOOLEAN			NOT NULL DEFAULT FALSE,		# Наличие изображения для этого поста
	`playmnl2_imgurl`	VARCHAR(100),								# URL изображения для этого поста
#
	`info`				VARCHAR(2500),								# Шаблон поста вывода текущего состояния тиража
	`info_isimg`		BOOLEAN			NOT NULL DEFAULT FALSE,		# Наличие изображения для этого поста
	`info_imgurl` 		VARCHAR(100)	CHARACTER SET utf8,			# URL изображения для этого поста
	`info2`				VARCHAR(2500),								# Шаблон второго поста [EN] вывода текущего состояния тиража
	`info2_isimg`		BOOLEAN			NOT NULL DEFAULT FALSE,		# Наличие изображения для этого поста
	`info2_imgurl` 		VARCHAR(100),								# URL изображения для этого поста
#	
	`whoupdated` 		VARCHAR(50)		CHARACTER SET utf8,			# Email юзера, который внёс крайние изменения
	`tstamp`			TIMESTAMP		DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;	# utf8mb4 + utf8mb4_unicode_ci


# Выгруженные из блокчейна транзации по кошелькам тиражей
CREATE TABLE le_trxs (
	`trx`			CHAR(66)		NOT NULL PRIMARY KEY,			# Транза оплаты (64 знака, если без 'Mt')
	`status`		ENUM('even','overpay','excess','badcoin','late') DEFAULT NULL,  # NULL на проверку/Ровно/Переплата/Лишнее/Левая монета/Уже разыгран
	`drawcode`		VARCHAR(50),									# Код тиража
	`block`			INT				UNSIGNED,						# № блока транзакции
	`blocktime`		DATETIME,										# Дата/время блока транзакции
	`txtype`		TINYINT			UNSIGNED,						# Тип транзакции
	`coin`			CHAR(10),										# Тикер монеты платежа
	`amountd`		DECIMAL(36,0)	UNSIGNED,						# Сумма оплаты - полная, как пришла из транзакции
	`amountf`		DOUBLE(18,9)	UNSIGNED,						# Сумма оплаты - с точностью константы PRECISION
	`payload`		VARCHAR(1024)	COLLATE utf8mb4_unicode_ci,		# Фактический пейлоад в транзакции
	`bets`			INT,											# Куплено билетов
	`oddbackf`		DOUBLE(18,9)	UNSIGNED,						# Размер сдачи для возврата отправителю, если была переплата
	`wallet`		CHAR(42),										# Кошелек покупателя билета тиража
	`walletto`		CHAR(42),										# Кошелек лотереи
	`txreject`		CHAR(66),										# Транза возврата ошибочного платежа или переплаты
	`tstamp`		TIMESTAMP		DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB CHARACTER SET utf8 COLLATE utf8_general_ci;

ALTER TABLE `le_trxs` ADD INDEX(`drawcode`);
ALTER TABLE `le_trxs` ADD INDEX(`status`);
ALTER TABLE `le_trxs` ADD INDEX(`block`);


# Номера крайних спарсенных страниц для кошельков тиражей
CREATE TABLE le_bcpages (
	`wallet`		CHAR(42)		NOT NULL PRIMARY KEY,			# Кошелек тиража, который парсится
	`page`			MEDIUMINT		UNSIGNED NOT NULL DEFAULT 1,	# Крайняя загруженная страница блокчейна по кошельку
	`tstamp`		TIMESTAMP		DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB CHARACTER SET utf8 COLLATE utf8_general_ci;


# Крайний № блока, загруженный прослушивающим сервером
CREATE TABLE le_mhdata (
	`nomkey`		CHAR(10)		NOT NULL PRIMARY KEY,			# Номинальный ключ для однострочной таблицы
	`block`			INT				UNSIGNED NOT NULL DEFAULT 0,	# Крайний загруженный № блока блокчейна
	`tstamp`		TIMESTAMP		DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB CHARACTER SET utf8 COLLATE utf8_general_ci;
INSERT INTO `le_mhdata` (`nomkey`, `block`, `tstamp`) VALUES ('lastblock', '0', CURRENT_TIMESTAMP);


# Обновление курса с Bip.dev
CREATE TABLE le_bipdev (
	`nomkey`		CHAR(10)		NOT NULL PRIMARY KEY,			# Номинальный ключ для однострочной таблицы
	`usdprice`		INT				UNSIGNED NOT NULL DEFAULT 0,	# Курс USD
	`btcprice`		INT				UNSIGNED NOT NULL DEFAULT 0,	# Курс BTC
	`lastupd`		TIMESTAMP		DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
INSERT INTO `le_bipdev` (`nomkey`, `usdprice`, `btcprice`, `lastupd`) VALUES ('bipprice', 0, 0, CURRENT_TIMESTAMP);



##################################################################################
# Обновления таблиц с исходной версии движка Lottery Engine (архив, уже применено)

ALTER TABLE `le_draws` DROP `walletback`, DROP `txpay`, DROP `txcashback`, DROP `txprofit`, DROP `mnemonic`;
ALTER TABLE `le_draws` ADD `chat2` CHAR(32) NULL DEFAULT NULL AFTER `chat`;
ALTER TABLE `le_draws` 
	ADD `runpostid2` INT NOT NULL DEFAULT '0' AFTER `endpostid`, 
	ADD `infopostid2` INT NOT NULL DEFAULT '0' AFTER `runpostid2`, 
	ADD `endpostid2` INT NOT NULL DEFAULT '0' AFTER `infopostid2`;

ALTER TABLE `le_templates` DROP `runauto`, DROP `runauto_isimg`, DROP `runauto_imgurl`, DROP `playauto`, DROP `playauto_isimg`, DROP `playauto_imgurl`;
ALTER TABLE `le_templates` 
	ADD `runmnl2` VARCHAR(2500) NULL DEFAULT NULL AFTER `runmnl_imgurl`, 
	ADD `runmnl2_isimg` BOOLEAN NOT NULL DEFAULT FALSE AFTER `runmnl2`, 
	ADD `runmnl2_imgurl` VARCHAR(100) NULL DEFAULT NULL AFTER `runmnl2_isimg`;
ALTER TABLE `le_templates` 
	ADD `playmnl2` VARCHAR(2500) NULL DEFAULT NULL AFTER `playmnl_imgurl`, 
	ADD `playmnl2_isimg` BOOLEAN NOT NULL DEFAULT FALSE AFTER `playmnl2`, 
	ADD `playmnl2_imgurl` VARCHAR(100) NULL DEFAULT NULL AFTER `playmnl2_isimg`;
ALTER TABLE `le_templates` 
	ADD `info2` VARCHAR(2500) NULL DEFAULT NULL AFTER `info_imgurl`, 
	ADD `info2_isimg` BOOLEAN NOT NULL DEFAULT FALSE AFTER `info2`, 
	ADD `info2_imgurl` VARCHAR(100) NULL DEFAULT NULL AFTER `info2_isimg`;

ALTER TABLE `le_templates` CHANGE `runmnl_imgurl` `runmnl_imgurl` VARCHAR(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL;
ALTER TABLE `le_templates` CHANGE `playmnl_imgurl` `playmnl_imgurl` VARCHAR(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL;
ALTER TABLE `le_templates` CHANGE `info_imgurl` `info_imgurl` VARCHAR(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL;
ALTER TABLE `le_templates` CHANGE `whoupdated` `whoupdated` VARCHAR(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL;
